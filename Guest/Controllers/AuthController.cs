﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;

namespace Guest.Controllers
{
    public class AuthController : Controller
    {
        private readonly string _Partner = "99AF7431-AE45-4F47-9D17-4560C10255A8";

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult SignIn()
        {
            if (Request.Cookies["username"] != null &&
                Request.Cookies["password"] != null)
            {
                ViewBag.UserName = Request.Cookies["username"];
                ViewBag.Password = Request.Cookies["password"];
            }

            ViewBag.Message = "NotAuthen";
            return View("~/Views/Auth/SignIn.cshtml");
        }
        [HttpPost]
        public IActionResult SignIn(string Phone, string Password)
        {
            var zInfo = new Customer_Info(Phone, Password);
            if (zInfo.Code == "200")
            {
                ViewBag.Message = "IsAuthen";

                //Get Partner
                var zUser = zInfo.Customer;
                zUser.Partner = new Partner_Info(zUser.PartnerNumber).Partner;

                // Save session
                var key = "GuestLogged";
                var obj = JsonConvert.SerializeObject(zUser);
                HttpContext.Session.SetString(key, obj);

                //Update Logged
                zInfo.UpdateLogged();

                //set cookies
                Response.Cookies.Append("username", Phone,
                    new CookieOptions
                    {
                        Expires = DateTime.Now.AddDays(15),
                        IsEssential = true
                    });

                Response.Cookies.Append("password", Password,
                    new CookieOptions
                    {
                        Expires = DateTime.Now.AddDays(15),
                        IsEssential = true
                    });

                //
                string url = Url.Action("Index", "Home");
                return Redirect(url);
            }
            else
            {
                ViewBag.Message = "405";
            }

            return View("~/Views/Auth/SignIn.cshtml");
        }

        public IActionResult SignOut()
        {
            HttpContext.Session.Clear();
            return View("~/Views/Auth/SignIn.cshtml");
        }
        [HttpPost]
        public IActionResult Register(string Phone, string Password, string Name, string Address)
        {
            var zInfo = new Customer_Info();
            var zModel = new Customer_Model
            {
                CustomerKey = Guid.NewGuid().ToString(),
                FullName = Name.ExTrim(),
                Address = Address.ExTrim(),
                Phone = Phone.ExTrim(),
                PartnerNumber = _Partner,
                Password = TN_Utils.HashPass(Password),
                Referrer = UriHelper.GetEncodedUrl(Request),
                CreatedName = UriHelper.GetEncodedUrl(Request),
                ModifiedName = UriHelper.GetEncodedUrl(Request),
            };
            
            zInfo.Customer = zModel;
            zInfo.Create_ClientKey();

            if (zInfo.Code == "200" || 
                zInfo.Code == "201")
            {
                var zUser = zInfo.Customer;
                zUser.Partner = new Partner_Info(zUser.PartnerNumber).Partner;

                var key = "GuestLogged";
                var obj = JsonConvert.SerializeObject(zUser);
                HttpContext.Session.SetString(key, obj);

                return RedirectToAction("Index", "Home");
            }
            else
            {
                TempData["Error"] = zInfo.Message.GetFirstLine();
                return View("~/Views/Auth/SignIn.cshtml");
            }            
        }

        public JsonResult CheckPhone(string Phone)
        {
            ServerResult zServer = new ServerResult();
            var zInfo = new Customer_Info(Phone, _Partner, true);
            if (zInfo.Code == "200")
            {
                zServer.Data = JsonConvert.SerializeObject(zInfo.Customer);
                zServer.Success = false;
            }
            else
            {
                zServer.Success = true;
            }

            return Json(zServer);
        }
    }
}