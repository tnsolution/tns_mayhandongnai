﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;

namespace Guest.Controllers
{
    public class BaseController : Controller
    {
        public static Customer_Model GuestLog = new Customer_Model();
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            var session = HttpContext.Session;
            var key = "GuestLogged";
            var GuestSession = session.GetString(key);

            if (GuestSession == null ||
                GuestSession == string.Empty)
            {
                string url = Url.Action("SignIn", "Auth");
                filterContext.Result = new RedirectResult(url);
                return;
            }
            else
            {
                GuestLog = JsonConvert.DeserializeObject<Customer_Model>(GuestSession);
            }
        }
    }
}