﻿using Guest.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;

namespace Guest.Controllers
{
    public class HomeController : BaseController
    {
        public IActionResult Index()
        {
            string Logo = GuestLog.Partner.Logo_Large;
            if (Logo == string.Empty)
            {
                Logo = "~/assets/themes/img/unnamed.png";
            }

            ViewBag.Logo = Logo;
            return View();
        }

        public IActionResult EditTask(string TaskKey)
        {
            TaskKey = TaskKey ?? "";
            var zInfo = new Task_Info(TaskKey);
            zInfo.Task.CustomerName = GuestLog.FullName;
            zInfo.Task.CustomerAddress = GuestLog.Address;
            zInfo.Task.CustomerPhone = GuestLog.Phone;

            return View("~/Views/Home/Task/Edit.cshtml", zInfo.Task);
        }

        [HttpPost]
        public async Task<IActionResult> SaveTaskAsync(string Name, string Phone, string Address, string Content, IFormFile FileUpload)
        {
            ServerResult zResult = new ServerResult();

            var storePath = "";
            var webPath = "";
            if (FileUpload != null &&
                FileUpload.Length > 0)
            {
                storePath = Path.Combine(
                       Directory.GetCurrentDirectory(), "wwwroot/_FileUpload/",
                       FileUpload.FileName);

                if (System.IO.File.Exists(storePath))
                {
                    System.IO.File.Delete(storePath);
                }
                using (var stream = new FileStream(storePath, FileMode.Create))
                {
                    await FileUpload.CopyToAsync(stream);
                }

                webPath = "/_FileUpload/" + FileUpload.FileName;
            }

            var zInfo = new Task_Info();
            var zModel = new Task_Model
            {
                TaskID = Task_Data.AutoID("KH", GuestLog.PartnerNumber),
                StartDate = DateTime.Now,
                CategoryKey = 26,       //VIỆC MƠI
                CategoryName = "1. Việc mới",
                GroupKey = 20,          //VIEC NGOÀI CỬA HÀNG
                GroupName = "Việc ngoài cửa hàng",
                Style = "SM",
                Class = "OS",
                StatusKey = 1,
                StatusName = "Mới gửi y/c",
                TaskContent = Content,
                TaskFile = webPath,
                CustomerName = Name,
                CustomerPhone = Phone,
                CustomerAddress = Address,
                CustomerKey = GuestLog.CustomerKey,
                CreatedName = GuestLog.FullName,
                CreatedBy = GuestLog.CustomerKey,
                ModifiedName = GuestLog.FullName,
                ModifiedBy = GuestLog.CustomerKey,
                PartnerNumber = GuestLog.PartnerNumber,
            };

            zInfo.Task = zModel;
            zInfo.Create_ServerKey();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                Save_Log(zModel, string.Empty);
                zResult.Success = true;
                //zResult.Data = Url.Action("Tracking", "Home", new { Style = 0 });
                return RedirectToAction("Tracking", "Home", new { Style = 0 });
            }

            TempData["Error"] = zInfo.Message.GetFirstLine();
            zResult.Success = false;
            zResult.Data = "";

            //return Json(zResult);
            return View("~/Views/Home/Task/Edit.cshtml", zInfo.Task);
        }

        public IActionResult ViewTask(string TaskKey)
        {
            var zInfo = new Task_Info(TaskKey);
            var zModel = zInfo.Task;
            var Logs = Task_Data.Log(TaskKey, out string Message);
            ViewBag.History = Logs;
            return View("~/Views/Home/Task/Detail.cshtml", zModel);
        }

        public IActionResult Tracking(int Style = 0)
        {
            string Cate = "";
            switch (Style)
            {
                //chưa thực hiện
                default:
                case 1:
                    Cate = "21,26";
                    break;
                //đang làm
                case 2:
                    Cate = "22,23,27,28";
                    break;
                //đã hoàn thành
                case 3:
                    Cate = "24,29";
                    break;
                //đã trả máy
                case 4:
                    Cate = "25,30";
                    break;
            }

            var List = Task_Data.ListGuest(GuestLog.PartnerNumber, GuestLog.CustomerKey, Cate, out string Message);
            ViewBag.ListData = List;
            ViewBag.Count = Count_Task();
            return View("~/Views/Home/Task/List.cshtml");
        }

        private List<string> Count_Task()
        {
            var data = new List<string>();

            string Cate = "21,26";
            int Count = Task_Data.Count(GuestLog.PartnerNumber, Cate, GuestLog.CustomerKey);
            data.Add(Count.ToString());

            //chưa thực hiện
            Cate = "22,23,27,28";
            Count = Task_Data.Count(GuestLog.PartnerNumber, Cate, GuestLog.CustomerKey);
            data.Add(Count.ToString());

            //đang làm
            Cate = "24,29";
            Count = Task_Data.Count(GuestLog.PartnerNumber, Cate, GuestLog.CustomerKey);
            data.Add(Count.ToString());

            //đã hoàn thành
            Cate = "25,30";
            Count = Task_Data.Count(GuestLog.PartnerNumber, Cate, GuestLog.CustomerKey);
            data.Add(Count.ToString());

            return data;
        }

        public IActionResult Search(string FromDate, string ToDate)
        {
            DateTime zFromDate = DateTime.MinValue;
            if (FromDate != null && FromDate != string.Empty)
            {
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
            }
            DateTime zToDate = DateTime.MinValue;
            if (ToDate != null && ToDate != string.Empty)
            {
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            }

            TimeSpan zTimeSpan = zToDate - zFromDate;
            if (zTimeSpan.Days > 90)
            {
                TempData["Error"] = "Thời gian không đươc quá 90 ngày. Vui lòng chọn lại khoản thời gian phù hợp !.";
            }
            else
            {
                var List = Task_Data.ListGuest(GuestLog.PartnerNumber, GuestLog.CustomerKey, zFromDate, zToDate, out string Message);
                ViewBag.Message = "Thông tin giao dịch của bạn " + zFromDate.ToString("dd/MM") + " đến " + zToDate.ToString("dd/MM");
                ViewBag.ListData = List;
            }

            return View("~/Views/Home/Task/Search.cshtml");
        }

        //
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private void Save_Log(Task_Model Model, string Description)
        {
            var zLog = new Task_Log_Info();
            var zModel = new Task_Log_Model
            {
                TaskKey = Model.TaskKey,
                CategoryKey = Model.CategoryKey.ToString(),
                CategoryName = Model.CategoryName,
                StatusKey = Model.StatusKey.ToString(),
                StatusName = Model.StatusName,
                Description = Description
            };

            zLog.Task_Log = zModel;
            zLog.Create_ServerKey();
        }

        //
        public IActionResult Setting()
        {
            return View("~/Views/Home/Setting/Index.cshtml");
        }

        public IActionResult Personal()
        {
            string CustomerKey = GuestLog.CustomerKey;
            var zInfo = new Customer_Info(CustomerKey);

            return View("~/Views/Home/Setting/Personal.cshtml", zInfo.Customer);
        }

        public IActionResult UpdatePersonal(string Name, string Address)
        {
            string CustomerKey = GuestLog.CustomerKey;
            var zInfo = new Customer_Info(CustomerKey);

            zInfo.Customer.FullName = Name;
            zInfo.Customer.Address = Address;
            zInfo.Customer.ModifiedName = GuestLog.FullName;
            zInfo.Customer.ModifiedBy = GuestLog.CustomerKey;

            zInfo.Update();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                TempData["Message"] = "Đổi thông tin thành công !.";
                return RedirectToAction("Setting", "Home");
            }

            TempData["Error"] = zInfo.Message.GetFirstLine();
            return View("~/Views/Home/Setting/Personal.cshtml");
        }

        public IActionResult ChangePass()
        {
            return View("~/Views/Home/Setting/ChangePass.cshtml");
        }

        public IActionResult UpdatePass(string OldPass = "", string NewPass = "", string RePass = "")
        {
            bool zSuccess = false;

            if (OldPass != string.Empty &&
                NewPass != string.Empty &&
                RePass != string.Empty)
            {
                string CustomerKey = GuestLog.CustomerKey;
                var zInfo = new Customer_Info(GuestLog.Phone, OldPass);
                if (zInfo.Customer.Password == TN_Utils.HashPass(OldPass))
                {
                    if (NewPass == RePass)
                    {
                        zSuccess = true;
                        zInfo.ResetPass(CustomerKey, TN_Utils.HashPass(RePass));
                        ViewBag.Message = "Đã đổi mật khẩu thành công !.";
                    }
                    else
                    {
                        zSuccess = false;
                        ViewBag.Message = "Mật khẩu mới chưa giống nhau vui lòng nhập lại !.";
                    }
                }
                else
                {
                    zSuccess = false;
                    ViewBag.Message = "Mật khẩu cũ không đúng vui lòng nhập lại !.";
                }
            }

            if (!zSuccess)
            {
                return View("~/Views/Home/Setting/ChangePass.cshtml");
            }
            else
            {
                TempData["Message"] = "Đổi mật khẩu thành công !.";
                return View("~/Views/Home/Setting/Index.cshtml");
            }
        }
    }
}
