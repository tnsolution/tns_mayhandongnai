﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Guest
{
    public class Helper
    {
        public static string ConnectionString = "";
        public static List<TN_Item> ListOption()
        {
            var List = new List<TN_Item>();
            List.Add(new TN_Item
            {
                Value = "1",
                Text = "Khách vãng lai",
            });

            List.Add(new TN_Item
            {
                Value = "2",
                Text = "Máy mang về",
            });

            List.Add(new TN_Item
            {
                Value = "3",
                Text = "Chỉ định cửa hàng",
            });

            return List;
        }
        public static string ExecuteSQL(string SQL, out string Message)
        {
            string zResult = "";
            string zConnectionString = ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(SQL, zConnect);
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                Message = "200 OK";
            }
            catch (Exception Err)
            {
                Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
    }

    public class TN_Item
    {
        public string Value { get; set; } = "";
        public string Text { get; set; } = "";
    }
}
