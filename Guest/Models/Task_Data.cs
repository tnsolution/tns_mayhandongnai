﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Guest
{
    public class Task_Data
    {
        public static List<Task_Model> ListGuest(string PartnerNumber, string CustomerKey, string Category, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM CRM_Task A WHERE A.RecordStatus != 99 AND A.PartnerNumber = @PartnerNumber AND A.CustomerKey = @CustomerKey";
            if (Category != string.Empty)
            {
                zSQL += " AND A.CategoryKey IN (" + Category + ")";
            }
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.NVarChar).Value = CustomerKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Task_Model> zList = new List<Task_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Task_Model()
                {
                    TaskKey = r["TaskKey"].ToString(),
                    TaskFile = r["TaskFile"].ToString(),
                    Subject = r["Subject"].ToString(),
                    TaskContent = r["TaskContent"].ToString(),
                    StartDate = (r["StartDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["StartDate"]),
                    DueDate = (r["DueDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DueDate"]),
                    Duration = r["Duration"].ToFloat(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    PriorityKey = r["PriorityKey"].ToInt(),
                    PriorityName = r["PriorityName"].ToString(),
                    CompleteRate = r["CompleteRate"].ToInt(),
                    CompleteDate = (r["CompleteDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CompleteDate"]),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    GroupKey = r["GroupKey"].ToInt(),
                    GroupName = r["GroupName"].ToString(),
                    ParentKey = r["ParentKey"].ToString(),
                    CustomerKey = r["CustomerKey"].ToString(),
                    CustomerID = r["CustomerID"].ToString(),
                    CustomerName = r["CustomerName"].ToString(),
                    CustomerPhone = r["CustomerPhone"].ToString(),
                    CustomerAddress = r["CustomerAddress"].ToString(),
                    ContractKey = r["ContractKey"].ToString(),
                    ContractName = r["ContractName"].ToString(),
                    Reminder = (r["Reminder"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["Reminder"]),
                    ApproveBy = r["ApproveBy"].ToString(),
                    ApproveName = r["ApproveName"].ToString(),
                    OwnerBy = r["OwnerBy"].ToString(),
                    OwnerName = r["OwnerName"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    CodeLine = r["CodeLine"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToString(),
                    BranchKey = r["BranchKey"].ToString(),
                    OrganizationID = r["OrganizationID"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        public static List<Task_Model> ListGuest(string PartnerNumber, string CustomerKey, DateTime FromDate, DateTime ToDate, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM CRM_Task A WHERE A.RecordStatus != 99 AND A.PartnerNumber = @PartnerNumber AND A.CustomerKey = @CustomerKey";
            if (FromDate != DateTime.MinValue &&
                 ToDate != DateTime.MinValue)
            {
                zSQL += " AND A.CreatedOn BETWEEN @FromDate AND @ToDate";
            }
            zSQL += " ORDER BY CreatedOn DESC";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.NVarChar).Value = CustomerKey;
                if (FromDate != DateTime.MinValue &&
                 ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Task_Model> zList = new List<Task_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Task_Model()
                {
                    TaskKey = r["TaskKey"].ToString(),
                    TaskFile = r["TaskFile"].ToString(),
                    Subject = r["Subject"].ToString(),
                    TaskContent = r["TaskContent"].ToString(),
                    StartDate = (r["StartDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["StartDate"]),
                    DueDate = (r["DueDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DueDate"]),
                    Duration = r["Duration"].ToFloat(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    PriorityKey = r["PriorityKey"].ToInt(),
                    PriorityName = r["PriorityName"].ToString(),
                    CompleteRate = r["CompleteRate"].ToInt(),
                    CompleteDate = (r["CompleteDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CompleteDate"]),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    GroupKey = r["GroupKey"].ToInt(),
                    GroupName = r["GroupName"].ToString(),
                    ParentKey = r["ParentKey"].ToString(),
                    CustomerKey = r["CustomerKey"].ToString(),
                    CustomerID = r["CustomerID"].ToString(),
                    CustomerName = r["CustomerName"].ToString(),
                    CustomerPhone = r["CustomerPhone"].ToString(),
                    CustomerAddress = r["CustomerAddress"].ToString(),
                    ContractKey = r["ContractKey"].ToString(),
                    ContractName = r["ContractName"].ToString(),
                    Reminder = (r["Reminder"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["Reminder"]),
                    ApproveBy = r["ApproveBy"].ToString(),
                    ApproveName = r["ApproveName"].ToString(),
                    OwnerBy = r["OwnerBy"].ToString(),
                    OwnerName = r["OwnerName"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    CodeLine = r["CodeLine"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToString(),
                    BranchKey = r["BranchKey"].ToString(),
                    OrganizationID = r["OrganizationID"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        public static string AutoID(string Prefix, string PartnerNumber)
        {
            string zResult = "";
            string zSQL = @"SELECT dbo.Auto_TaskID(@Prefix, @PartnerNumber)";

            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Prefix", SqlDbType.NVarChar).Value = Prefix;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zResult;
        }

        public static int Count(string PartnerNumber, string Category, string CustomerKey)
        {
            int zResult = 0;
            string zSQL = @"SELECT COUNT(A.TaskKey) FROM CRM_Task A WHERE A.RecordStatus != 99 AND A.PartnerNumber = @PartnerNumber AND A.CustomerKey = @CustomerKey";
            if (Category != string.Empty)
            {
                zSQL += " AND A.CategoryKey IN (" + Category + ")";
            }
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.NVarChar).Value = CustomerKey;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zResult;
        }

        public static List<Task_Log_Model> Log(string TaskKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM CRM_Task_Log WHERE TaskKey = @TaskKey ";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TaskKey", SqlDbType.NVarChar).Value = TaskKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Task_Log_Model> zList = new List<Task_Log_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Task_Log_Model
                {
                    CategoryKey = r["CategoryKey"].ToString(),
                    CategoryName = r["CategoryName"].ToString(),
                    StatusKey = r["StatusKey"].ToString(),
                    StatusName = r["StatusName"].ToString(),
                    Description = r["Description"].ToString(),
                    CreatedOn = Convert.ToDateTime(r["CreatedOn"]),
                });
            }
            return zList;
        }
    }
}
