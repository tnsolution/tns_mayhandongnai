﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Mobile.Controllers
{
    public class AuthController : Controller
    {
        private readonly string _Partner = "99AF7431-AE45-4F47-9D17-4560C10255A8";

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult SignIn()
        {
            if (Request.Cookies["username"] != null &&
                Request.Cookies["password"] != null)
            {
                ViewBag.UserName = Request.Cookies["username"];
                ViewBag.Password = Request.Cookies["password"];
            }

            ViewBag.ListBranch = Branch_Data.List(_Partner);
            ViewBag.Message = "NotAuthen";
            return View("~/Views/Auth/SignIn.cshtml");
        }
        [HttpPost]
        public IActionResult SignIn(string UserName, string Password)
        {
            ViewBag.ListBranch = Branch_Data.List(_Partner);
            User_Info zInfo = new User_Info(UserName, Password);
            if (zInfo.Code == "200")
            {
                //check time
                // if (DateTime.Now.Hour < 8 ||
                //     DateTime.Now.Hour > 17)
                // {
                //     ViewBag.Message = "TimeOut";
                //     return View("~/Views/Auth/SignIn.cshtml");
                // }

                ViewBag.Message = "IsAuthen";

                //Get Partner
                var zUser = zInfo.User;
                zUser.Partner = new Partner_Info(zUser.PartnerNumber).Partner;

                //Get Employee
                var zEmployeeKey = zUser.Employee.EmployeeKey;
                if (zEmployeeKey != string.Empty)
                {
                    zUser.Employee = new Employee_Info(zEmployeeKey).Employee;
                }

                //get data access
                string temp = zInfo.User.DataAccess;
                if (temp != null && temp != string.Empty)
                {
                    var zList = JsonConvert.DeserializeObject<List<string>>(temp);
                    string zData = zList.ToStringComma();
                    zUser.DataAccess = zData;
                }

                //get role access
                zInfo.User.ListRole = User_Data.ReadUserRole(zUser.PartnerNumber, zInfo.User.UserKey, out string Message);

                // Save session
                var key = "UserLogged";
                var obj = JsonConvert.SerializeObject(zUser);
                HttpContext.Session.SetString(key, obj);

                //Update Logged
                zInfo.UpdateLogged();

                //set cookies
                Response.Cookies.Append("username", UserName,
                    new CookieOptions
                    {
                        Expires = DateTime.Now.AddDays(15),
                        IsEssential = true
                    });

                Response.Cookies.Append("password", Password,
                    new CookieOptions
                    {
                        Expires = DateTime.Now.AddDays(15),
                        IsEssential = true
                    });
            }
            else
            {
                ViewBag.Message = "405";
            }

            return RedirectToAction("Index", "Home");

            //return View("~/Views/Auth/SignIn.cshtml");
        }

        public IActionResult SignOut()
        {
            ViewBag.ListBranch = Branch_Data.List(_Partner);
            if (Request.Cookies["username"] != null &&
               Request.Cookies["password"] != null)
            {
                ViewBag.UserName = Request.Cookies["username"];
                ViewBag.Password = Request.Cookies["password"];
            }

            HttpContext.Session.Clear();
            return View("~/Views/Auth/SignIn.cshtml");
        }

        [HttpPost]
        public IActionResult Register(string Phone, string Password, string Name, string Address, string Branch)
        {
            var CheckPhone = new Employee_Info(Phone, _Partner);
            if (CheckPhone.Code == "200" ||
                CheckPhone.Code == "201")
            {
                ViewBag.Message = "Số điện thoại này đã được sử dụng, Bạn phải nhập số khác !.";
                return View("~/Views/Auth/SignIn.cshtml");
            }

            string NewKey = Guid.NewGuid().ToString();
            var zManager = Employee_Data.GetManager(_Partner, Branch, out string Message);

            var zInfo = new Employee_Info();
            var zModel = new Employee_Model
            {
                EmployeeKey = NewKey,
                BranchKey = Branch.ToInt(),
                ReportToKey = zManager.EmployeeKey,
                ReportToName = zManager.FullName,
                FirstName = Name,
                AddressContact = Address,
                MobiPhone = Phone,

                PartnerNumber = _Partner,
                Password = TN_Utils.HashPass(Password),

                CreatedName = UriHelper.GetEncodedUrl(Request),
                ModifiedName = UriHelper.GetEncodedUrl(Request),
            };

            zInfo.Employee.ClearNullable();
            zInfo.Employee = zModel;
            zInfo.Create_ClientKey();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                var zUser = new User_Info();
                zUser.User.Employee = zInfo.Employee;
                zUser.User.UserName = Phone;
                zUser.User.Password = TN_Utils.HashPass(Password);
                zUser.User.PartnerNumber = _Partner;
                zUser.User.ExpireDate = DateTime.Now.AddYears(1);
                zUser.User.Activate = true;
                zUser.User.CreatedName = UriHelper.GetEncodedUrl(Request);
                zUser.User.ModifiedName = UriHelper.GetEncodedUrl(Request);
                zUser.Create_ServerKey();

                // Save session
                var key = "UserLogged";
                var obj = JsonConvert.SerializeObject(zUser.User);
                HttpContext.Session.SetString(key, obj);

                ViewBag.Message = "IsAuthen";

                return RedirectToAction("Index", "Home");
            }

            ViewBag.Message = zInfo.Message.GetFirstLine();
            return View("~/Views/Auth/SignIn.cshtml");
        }

        public JsonResult CheckPhone(string Phone)
        {
            var zResult = new ServerResult();
            var zInfo = new Employee_Info(Phone, _Partner);
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zInfo.Employee);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = "";
            }
            return Json(zResult);
        }
    }
}