﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Mobile.Controllers
{
    public class GuestController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult List()
        {
            ViewBag.ListData = Customer_Data.List(UserLog.PartnerNumber, 0, string.Empty, out string Message);
            ViewBag.ListBranch = Branch_Data.List(UserLog.PartnerNumber);

            return View();
        }

        public IActionResult Detail(string CustomerKey)
        {
            var zInfo = new Customer_Info(CustomerKey);

            if (zInfo.Code != "200" &&
                zInfo.Code != "201")
            {
                ViewBag.Message = zInfo.Message.GetFirstLine();
            }
            else
            {
                ViewBag.History = Customer_Data.HistoryTask(UserLog.PartnerNumber, CustomerKey, out string Message);
            }

            return View(zInfo.Customer);
        }

        public IActionResult Edit(string CustomerKey)
        {
            var zInfo = new Customer_Info(CustomerKey);

            if (zInfo.Code != "200" &&
                zInfo.Code != "201")
            {
                ViewBag.Message = zInfo.Message.GetFirstLine();
            }

            return View(zInfo.Customer);
        }

        public IActionResult Save(string CustomerKey, string Name, string Phone, string Address, string Description)
        {
            CustomerKey = CustomerKey ?? "";
            var zModel = new Customer_Model
            {
                CustomerKey = CustomerKey,
                Phone = Phone,
                Address = Address,
                FullName = Name,
                Note = Description,

                PartnerNumber = UserLog.PartnerNumber,
                DepartmentKey = UserLog.Employee.DepartmentKey,
                BranchKey = UserLog.Employee.BranchKey,
                CreatedBy = UserLog.UserKey,
                CreatedName = UserLog.Employee.FullName,
                ModifiedBy = UserLog.UserKey,
                ModifiedName = UserLog.Employee.FullName
            };

            var zInfo = new Customer_Info(CustomerKey);
            zInfo.Customer = zModel.TrimStringProperties();
            zInfo.Customer.ClearNullable();
            if (CustomerKey.Length >= 36)
            {
                zInfo.Update();
            }
            else
            {
                zInfo.Create_ServerKey();
            }

            ServerResult zResult = new ServerResult();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                return RedirectToAction("List");
            }
            else
            {
                TempData["error"] = zInfo.Message.GetFirstLine();
                ViewBag.Message = zInfo.Message.GetFirstLine();
                return View("~/Views/Guest/Edit.cshtml", new { CustomerKey });
            }
        }

        public IActionResult Search(string Name, string Phone, string Branch)
        {
            Name = Name ?? "";
            Phone = Phone ?? "";
            Branch = Branch == null ? UserLog.DataAccess : "'" + Branch + "'";

            ViewBag.ListData = Customer_Data.List(UserLog.PartnerNumber, 0, Branch, Name.Trim(), Phone.Trim(), out string Message);
            ViewBag.ListBranch = Branch_Data.List(UserLog.PartnerNumber);

            return View("~/Views/Guest/List.cshtml");
        }

        public JsonResult Delete(string CustomerKey)
        {
            CustomerKey = CustomerKey ?? "";
            var zResult = new ServerResult();
            var zInfo = new Customer_Info(CustomerKey);
            ////Khách đã giao dịch không được xóa
            //if (zInfo.Customer.CategoryKey == 10)
            //{
            //    zResult.Success = false;
            //    zResult.Message = "Khách đã có giao dịch bạn không được xóa, vui lòng liên hệ quản lý !.";
            //    return Json(zResult);
            //}

            zInfo.Delete();
            if (zInfo.Code == "200")
            {
                zResult.Success = true;
                zResult.Message = string.Empty;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }

            return Json(zResult);
        }

        public JsonResult ResetPass(string CustomerKey)
        {
            CustomerKey = CustomerKey ?? "";
            var zResult = new ServerResult();
            var zInfo = new Customer_Info();
            string zNewPass = TN_Utils.RandomPassword();
            zInfo.ResetPass(CustomerKey, TN_Utils.HashPass(zNewPass));
            if (zInfo.Code == "200")
            {
                zResult.Success = true;
                zResult.Data = zNewPass;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }

            return Json(zResult);
        }

        public JsonResult Check_Customer(string Phone)
        {
            var zResult = new ServerResult();
            var zInfo = new Customer_Info(Phone, UserLog.PartnerNumber, true);
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zInfo.Customer);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = "";
            }
            return Json(zResult);
        }
    }
}