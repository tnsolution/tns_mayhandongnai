﻿using Microsoft.AspNetCore.Mvc;
using Mobile.Models;
using System;
using System.Diagnostics;
using System.Linq;

namespace Mobile.Controllers
{
    public class HomeController : BaseController
    {
        public IActionResult Index()
        {
            string Logo = UserLog.Partner.Logo_Large;
            if (Logo == string.Empty)
            {
                Logo = "~/assets/themes/img/unnamed.png";
            }

            //ViewBag.Baged = Task_Data.Delivery(UserLog.PartnerNumber, out string Message).Count;

            var Menu = UserLog.ListRole.Where(s => s.Slug == 9 && s.Level == 1).ToList();
            if (Menu != null && Menu.Count() > 0)
            {
                foreach (var item in Menu)
                {
                    if (item.Query.Length > 0)
                    {
                        item.Badge = Helper.ExecuteSQL(item.Query.Replace("@PartnerNumber", "'" + UserLog.PartnerNumber + "'"), out string Message).ToInt();
                    }
                }
            }

            ViewBag.Menu = Menu;
            ViewBag.Logo = Logo;
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
