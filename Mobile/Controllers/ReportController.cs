﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Globalization;

namespace Mobile.Controllers
{
    public class ReportController : BaseController
    {
        public IActionResult Index(string btnAction = "", string DateView = "", string FromDate = "", string ToDate = "")
        {
            if (btnAction == "btn_Search")
            {
                //ngày tự do
                DateTime zFromDate;
                DateTime zToDate;

                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);

                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

                ViewBag.ChartData = DataChart(zFromDate, zToDate);
                ViewBag.SalesData = Report.Data_BranchSales(UserLog.PartnerNumber, zFromDate, zToDate, out string Message);
                ViewBag.GuestData = Report.Data_BranchGuest(UserLog.PartnerNumber, zFromDate, zToDate, out Message);
                ViewBag.TaskData = Report.Data_BranchTask(UserLog.PartnerNumber, zFromDate, zToDate, out Message);

                ViewBag.Task = Report.Total_Task(UserLog.PartnerNumber, zFromDate, zToDate);
                ViewBag.Sales = Report.Total_Sale(UserLog.PartnerNumber, zFromDate, zToDate);
                ViewBag.Guest = Report.Total_Guest(UserLog.PartnerNumber, zFromDate, zToDate);
                ViewBag.DateView = FromDate + "-" + ToDate;
            }
            else
            {
                #region [Datetime string to fromdate - todate]
                int Month = DateTime.Now.Month;
                int Year = DateTime.Now.Year;
                if (DateView != "")
                {
                    Month = DateView.Split('/')[0].ToInt();
                    Year = DateView.Split('/')[1].ToInt();

                    if (Year.ToString().Length < 4)
                    {
                        Month = DateTime.Now.Month;
                        Year = DateTime.Now.Year;
                    }
                }

                switch (btnAction)
                {
                    case "btn_Prev":
                        Month = Month - 1;
                        break;
                    case "btn_Next":
                        Month = Month + 1;
                        break;
                }
                if (Month <= 0)
                {
                    Month = 12;
                    Year = Year - 1;
                }
                if (Month >= 13)
                {
                    Month = 1;
                    Year = Year + 1;
                }
                DateTime zDateView = DateTime.Now;
                zDateView = new DateTime(Year, Month, 1, 23, 59, 59);
                DateTime zFromDate = new DateTime(zDateView.Year, zDateView.Month, 1, 0, 0, 0);
                DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
                #endregion

                ViewBag.ChartData = DataChart(zFromDate, zToDate);
                ViewBag.SalesData = Report.Data_BranchSales(UserLog.PartnerNumber, zFromDate, zToDate, out string Message);
                ViewBag.GuestData = Report.Data_BranchGuest(UserLog.PartnerNumber, zFromDate, zToDate, out Message);
                ViewBag.TaskData = Report.Data_BranchTask(UserLog.PartnerNumber, zFromDate, zToDate, out Message);

                ViewBag.Task = Report.Total_Task(UserLog.PartnerNumber, zFromDate, zToDate);
                ViewBag.Sales = Report.Total_Sale(UserLog.PartnerNumber, zFromDate, zToDate);
                ViewBag.Guest = Report.Total_Guest(UserLog.PartnerNumber, zFromDate, zToDate);
                ViewBag.DateView = zToDate.Month + "/" + zToDate.Year;
            }

            ViewBag.Title = "Tổng hợp";

            return View();
        }

        public IActionResult Sales_Staffs(string btnAction = "", string DateView = "", string FromDate = "", string ToDate = "", string Branch = "")
        {
            ViewBag.SearchBranch = Branch;

            Branch = (Branch == "" || Branch == null) ? UserLog.DataAccess : "'" + Branch + "'";

            // chọn tìm lọc
            ViewBag.ListBranch = Branch_Data.List(UserLog.PartnerNumber, UserLog.DataAccess);

            if (btnAction == "btn_Search")
            {
                //ngày tự do

                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime zFromDate);
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime zToDate);

                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

                ViewBag.FromDate = zFromDate.ToString("dd/MM/yyyy");
                ViewBag.ToDate = zToDate.ToString("dd/MM/yyyy");
                ViewBag.DateView = FromDate + "-" + ToDate;
                ViewBag.Data = Report.Sales_Staff(UserLog.PartnerNumber, Branch, zFromDate, zToDate, out string Message);
            }
            else
            {
                //tìm theo tháng 
                #region Datetime string to fromdate - todate
                int Month = DateTime.Now.Month;
                int Year = DateTime.Now.Year;
                if (DateView != "")
                {
                    Month = DateView.Split('/')[0].ToInt();
                    Year = DateView.Split('/')[1].ToInt();

                    if (Year.ToString().Length < 4)
                    {
                        Month = DateTime.Now.Month;
                        Year = DateTime.Now.Year;
                    }
                }

                switch (btnAction)
                {
                    case "btn_Prev":
                        Month = Month - 1;
                        break;
                    case "btn_Next":
                        Month = Month + 1;
                        break;
                }
                if (Month <= 0)
                {
                    Month = 12;
                    Year = Year - 1;
                }
                if (Month >= 13)
                {
                    Month = 1;
                    Year = Year + 1;
                }
                DateTime zDateView = DateTime.Now;
                zDateView = new DateTime(Year, Month, 1, 23, 59, 59);
                DateTime zFromDate = new DateTime(zDateView.Year, zDateView.Month, 1, 0, 0, 0);
                DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
                #endregion

                ViewBag.FromDate = zFromDate.ToString("dd/MM/yyyy");
                ViewBag.ToDate = zToDate.ToString("dd/MM/yyyy");
                ViewBag.DateView = zToDate.Month + "/" + zToDate.Year;
                ViewBag.Data = Report.Sales_Staff(UserLog.PartnerNumber, Branch, zFromDate, zToDate, out string Message);
            }

            ViewBag.Title = "Doanh số nhân sự";
            return View();
        }

        public IActionResult Sales_Guest(string btnAction = "", string DateView = "", string FromDate = "", string ToDate = "")
        {
            if (btnAction == "btn_Search")
            {
                //ngày tự do
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime zFromDate);
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime zToDate);

                ViewBag.DateView = FromDate + "-" + ToDate;
                ViewBag.Data = Report.Sales_Guest(UserLog.PartnerNumber, zFromDate, zToDate, out string Message);
            }
            else
            {

                #region Datetime string to fromdate - todate
                int Month = DateTime.Now.Month;
                int Year = DateTime.Now.Year;
                if (DateView != "")
                {
                    Month = DateView.Split('/')[0].ToInt();
                    Year = DateView.Split('/')[1].ToInt();

                    if (Year.ToString().Length < 4)
                    {
                        Month = DateTime.Now.Month;
                        Year = DateTime.Now.Year;
                    }
                }

                switch (btnAction)
                {
                    case "btn_Prev":
                        Month = Month - 1;
                        break;
                    case "btn_Next":
                        Month = Month + 1;
                        break;
                }
                if (Month <= 0)
                {
                    Month = 12;
                    Year = Year - 1;
                }
                if (Month >= 13)
                {
                    Month = 1;
                    Year = Year + 1;
                }
                DateTime zDateView = DateTime.Now;
                zDateView = new DateTime(Year, Month, 1, 23, 59, 59);
                DateTime zFromDate = new DateTime(zDateView.Year, zDateView.Month, 1, 0, 0, 0);
                DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
                #endregion

                ViewBag.DateView = zToDate.Month + "/" + zToDate.Year;
                ViewBag.Data = Report.Sales_Guest(UserLog.PartnerNumber, zFromDate, zToDate, out string Message);
            }

            ViewBag.Title = "Doanh số khách hàng";
            return View();
        }

        public IActionResult Sales_Branch(string btnAction = "", string DateView = "", string FromDate = "", string ToDate = "")
        {
            if (btnAction == "btn_Search")
            {
                //ngày tự do

                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime zFromDate);
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime zToDate);

                ViewBag.DateView = FromDate + "-" + ToDate;
                ViewBag.Data = Report.Sales_Branch(UserLog.PartnerNumber, zFromDate, zToDate, out string Message);
            }
            else
            {
                #region Datetime string to fromdate - todate
                int Month = DateTime.Now.Month;
                int Year = DateTime.Now.Year;
                if (DateView != "")
                {
                    Month = DateView.Split('/')[0].ToInt();
                    Year = DateView.Split('/')[1].ToInt();

                    if (Year.ToString().Length < 4)
                    {
                        Month = DateTime.Now.Month;
                        Year = DateTime.Now.Year;
                    }
                }

                switch (btnAction)
                {
                    case "btn_Prev":
                        Month = Month - 1;
                        break;
                    case "btn_Next":
                        Month = Month + 1;
                        break;
                }
                if (Month <= 0)
                {
                    Month = 12;
                    Year = Year - 1;
                }
                if (Month >= 13)
                {
                    Month = 1;
                    Year = Year + 1;
                }
                DateTime zDateView = DateTime.Now;
                zDateView = new DateTime(Year, Month, 1, 23, 59, 59);
                DateTime zFromDate = new DateTime(zDateView.Year, zDateView.Month, 1, 0, 0, 0);
                DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
                #endregion

                ViewBag.DateView = zToDate.Month + "/" + zToDate.Year;
                ViewBag.Data = Report.Sales_Branch(UserLog.PartnerNumber, zFromDate, zToDate, out string Message);
            }

            ViewBag.Title = "Doanh số chi nhánh";
            return View();
        }

        private string DataChart(DateTime FromDate, DateTime ToDate)
        {
            var zList = Report.Sales_Branch(UserLog.PartnerNumber, FromDate, ToDate, out string Message);

            string zLabel = "";
            string zValue = "";
            foreach (var zItem in zList)
            {
                zLabel += "'" + zItem.ItemName + "',";
                zValue += zItem.ItemAmount.ToString() + ",";
            }
            zLabel = zLabel.Remove(zLabel.LastIndexOf(","));
            zLabel = " labels: [" + zLabel + "]";
            string zDataset = @" datasets: [{ label: 'Doanh số', data: [" + zValue + "], backgroundColor: ['rgb(255, 99, 132)', 'rgb(54, 162, 235)', 'rgb(255, 205, 86)'], hoverOffset: 4 }]";
            string zChartScript = "const data = { " + zLabel + "," + zDataset + " } ;";
            return zChartScript;
        }

        public IActionResult Sales_Personal(string btnAction = "", string DateView = "", string FromDate = "", string ToDate = "", string Employee = "")
        {
            Employee = (Employee == null || Employee == "") ? UserLog.Employee.EmployeeKey : Employee;
            var zInfo = new Employee_Info(Employee);

            if (btnAction == "btn_Search")
            {
                //ngày tự do

                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime zFromDate);
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime zToDate);

                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

                ViewBag.Data = Report.Sales_Personal(UserLog.PartnerNumber, Employee, zFromDate, zToDate, out string Message);
                ViewBag.Title = "Doanh số " + zInfo.Employee.FullName;
                ViewBag.DateView = FromDate + "-" + ToDate;
            }
            else
            {
                //tìm theo tháng 
                #region [Datetime string to fromdate - todate]
                int Month = DateTime.Now.Month;
                int Year = DateTime.Now.Year;
                if (DateView != "")
                {
                    Month = DateView.Split('/')[0].ToInt();
                    Year = DateView.Split('/')[1].ToInt();

                    if (Year.ToString().Length < 4)
                    {
                        Month = DateTime.Now.Month;
                        Year = DateTime.Now.Year;
                    }
                }

                switch (btnAction)
                {
                    case "btn_Prev":
                        Month = Month - 1;
                        break;
                    case "btn_Next":
                        Month = Month + 1;
                        break;
                }
                if (Month <= 0)
                {
                    Month = 12;
                    Year = Year - 1;
                }
                if (Month >= 13)
                {
                    Month = 1;
                    Year = Year + 1;
                }
                DateTime zDateView = DateTime.Now;
                zDateView = new DateTime(Year, Month, 1, 23, 59, 59);
                DateTime zFromDate = new DateTime(zDateView.Year, zDateView.Month, 1, 0, 0, 0);
                DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
                #endregion

                ViewBag.Data = Report.Sales_Personal(UserLog.PartnerNumber, Employee, zFromDate, zToDate, out string Message);
                ViewBag.Title = "Doanh số " + zInfo.Employee.FullName;
                ViewBag.DateView = zToDate.Month + "/" + zToDate.Year;
            }

            return View();
        }
    }
}