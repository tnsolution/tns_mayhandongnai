﻿using Microsoft.AspNetCore.Mvc;

namespace Mobile.Controllers
{
    public class SettingController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ChangePass()
        {
            return View("~/Views/Setting/ChangePass.cshtml");
        }

        public IActionResult UpdatePass(string OldPass = "", string NewPass = "", string RePass = "")
        {
            bool zSuccess = false;

            if (OldPass != string.Empty &&
                NewPass != string.Empty &&
                RePass != string.Empty)
            {
                string UserKey = UserLog.UserKey;
                var zInfo = new User_Info(UserKey);
                if (zInfo.User.Password == TN_Utils.HashPass(OldPass))
                {
                    if (NewPass == RePass)
                    {
                        zSuccess = true;
                        zInfo.ResetPass(UserKey, TN_Utils.HashPass(RePass));
                        ViewBag.Message = "Đã đổi mật khẩu thành công !.";
                    }
                    else
                    {
                        zSuccess = false;
                        ViewBag.Message = "Mật khẩu mới chưa giống nhau vui lòng nhập lại !.";
                    }
                }
                else
                {
                    zSuccess = false;
                    ViewBag.Message = "Mật khẩu cũ không đúng vui lòng nhập lại !.";
                }
            }

            if (!zSuccess)
            {
                return View("~/Views/Setting/ChangePass.cshtml");
            }
            else
            {
                return RedirectToAction("Index", "Setting");
            }
        }
    }
}