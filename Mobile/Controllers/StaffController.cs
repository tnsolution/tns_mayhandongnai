﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Mobile.Controllers
{
    public class StaffController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult List()
        {
            ViewBag.ListData = Employee_Data.List(UserLog.PartnerNumber, string.Empty);
            ViewBag.ListBranch = Branch_Data.List(UserLog.PartnerNumber);
            return View();
        }

        public IActionResult Detail(string EmployeeKey)
        {
            var zInfo = new Employee_Info(EmployeeKey);

            if (zInfo.Code != "200" &&
                zInfo.Code != "201")
            {
                ViewBag.Message = zInfo.Message.GetFirstLine();
            }
            else
            {
                ViewBag.History = Employee_Data.HistoryTask(UserLog.PartnerNumber, EmployeeKey, out string Message);
            }

            return View(zInfo.Employee);
        }

        public IActionResult Edit(string EmployeeKey)
        {
            ViewBag.ListData = Employee_Data.List(UserLog.PartnerNumber, string.Empty);
            ViewBag.ListPosition = Position_Data.List(UserLog.PartnerNumber);
            ViewBag.ListDepartment = Department_Data.List(UserLog.PartnerNumber);
            ViewBag.ListBranch = Branch_Data.List(UserLog.PartnerNumber);

            var zInfo = new Employee_Info(EmployeeKey);

            if (zInfo.Code != "200" &&
                zInfo.Code != "201")
            {
                zInfo.Employee.EmployeeID = Employee_Data.AutoID("MH", UserLog.PartnerNumber);
            }

            return View(zInfo.Employee);
        }

        public IActionResult Save(
                string EmployeeKey, string EmployeeID, string LastName, string FirstName,
                int PositionKey, string PositionName, string MobiPhone = "",
                string ReportToKey = "", string ReportToName = "",
                string BranchKey = "0", string BranchName = "",
                string DepartmentKey = "0", string DepartmentName = "")
        {
            EmployeeKey = EmployeeKey ?? "";
            var zInfo = new Employee_Info();
            var zModel = new Employee_Model
            {
                EmployeeID = EmployeeID.Trim(),
                LastName = LastName.Trim(),
                FirstName = FirstName.Trim(),
                PositionKey = PositionKey,
                PositionName = PositionName ?? "",

                BranchKey = BranchKey.ToInt(),
                BranchName = BranchName,
                DepartmentKey = DepartmentKey.ToInt(),
                DepartmentName = DepartmentName ?? "",

                MobiPhone = MobiPhone.Trim(),
                ReportToKey = ReportToKey ?? "",
                ReportToName = ReportToName ?? "",

                PartnerNumber = UserLog.PartnerNumber,
                CreatedBy = UserLog.UserKey,
                CreatedName = UserLog.Employee.FullName,
                ModifiedBy = UserLog.UserKey,
                ModifiedName = UserLog.Employee.FullName
            };

            zInfo.Employee.ClearNullable();
            if (EmployeeKey == "")
            {
                zInfo.Employee = zModel;
                zInfo.Create_ServerKey();
            }
            else
            {
                zModel.EmployeeKey = EmployeeKey;
                zInfo.Employee = zModel;
                zInfo.Update();
            }

            ServerResult zResult = new ServerResult();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                return RedirectToAction("List");
            }
            else
            {
                TempData["error"] = zInfo.Message.GetFirstLine();
                ViewBag.Message = zInfo.Message.GetFirstLine();
                return View("~/Views/Staff/Edit.cshtml", new { EmployeeKey });
            }
        }

        public IActionResult Search(string Name, string Phone, string Branch)
        {
            Name = Name ?? "";
            Phone = Phone ?? "";
            Branch = Branch == null ? UserLog.DataAccess : "'" + Branch + "'";

            ViewBag.ListData = Employee_Data.List(UserLog.PartnerNumber, Branch, Name, Phone, out string Message);
            ViewBag.ListBranch = Branch_Data.List(UserLog.PartnerNumber);
            return View("~/Views/Staff/List.cshtml");
        }

        public JsonResult Delete(string EmployeeKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Employee_Info();
            zInfo.Employee.EmployeeKey = EmployeeKey;
            zInfo.Delete();
            var zModel = zInfo.Employee;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult);
            }
        }
        public JsonResult GetID()
        {
            string ID = Employee_Data.AutoID("MH", UserLog.PartnerNumber);
            return Json(ID);
        }
        public JsonResult Check(string Phone)
        {
            var zResult = new ServerResult();
            var zInfo = new Employee_Info(Phone, UserLog.PartnerNumber);
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zInfo.Employee);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = "";
            }
            return Json(zResult);
        }
    }
}