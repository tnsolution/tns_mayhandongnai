﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Globalization;
using System.Data;

namespace Mobile.Controllers
{
    public class SurveyController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Map()
        {
            ViewBag.ListData = Company_Data.List(UserLog.PartnerNumber, string.Empty, string.Empty, string.Empty,out string Message);
            ViewBag.ListBranch = Branch_Data.List(UserLog.PartnerNumber);
            return View();
        }

        public IActionResult List()
        {
            ViewBag.ListData = Company_Data.List(UserLog.PartnerNumber, string.Empty, string.Empty, string.Empty,  out string Message);
            ViewBag.ListBranch = Branch_Data.List(UserLog.PartnerNumber);
            return View();
        }
        public IActionResult Search(string Name, string Phone, string Branch)
        {
            Name = Name ?? "";
            Phone = Phone ?? "";
            Branch = Branch == null ? UserLog.DataAccess : "'" + Branch + "'";

            ViewBag.ListData = Company_Data.List(UserLog.PartnerNumber, Branch, Name.Trim(), Phone.Trim(),  out string Message);
            ViewBag.ListBranch = Branch_Data.List(UserLog.PartnerNumber);

            return View("~/Views/Survey/List.cshtml");
        }
       
        public IActionResult Detail(string CompanyKey)
        {
            var zInfo = new Company_Info(CompanyKey);

            if (zInfo.Code != "200" &&
                zInfo.Code != "201")
            {
                ViewBag.Message = zInfo.Message.GetFirstLine();
            }
            else
            {
                ViewBag.ListDevice = Device_Data.ListDevice(UserLog.PartnerNumber, CompanyKey, out string Message);
                ViewBag.ListSurvey = Survey_Data.ListSurvey(UserLog.PartnerNumber, CompanyKey, out string zMessage);
                ViewBag.ListPhoto = Photo_Data.List(UserLog.PartnerNumber, CompanyKey, out string sMessage);
            }

            return View(zInfo.Company);
        }

        public IActionResult Edit(string CompanyKey)
        {
            CompanyKey = CompanyKey ?? "";
            var zInfo = new Company_Info(CompanyKey);

            if (zInfo.Code != "200" &&
                zInfo.Code != "201")
            {
                ViewBag.Message = zInfo.Message.GetFirstLine();
            }
            else
            {
                ViewBag.ListDevice = Device_Data.ListDevice(UserLog.PartnerNumber, CompanyKey, out string Message);
                ViewBag.ListSurvey = Survey_Data.ListSurvey(UserLog.PartnerNumber, CompanyKey, out string zMessage);
                ViewBag.ListPhoto = Photo_Data.List(UserLog.PartnerNumber, CompanyKey, out string sMessage);
            }
            return View(zInfo.Company);
        }
        [HttpPost]
        public async Task<JsonResult> Upload_ImgList()
        {
            var zResult = new ServerResult();
            var files = Request.Form.Files;
            if (files != null && files.Count > 0)
            {
                var list = new List<string>();
                foreach (var file in files)
                {
                    var filepath = await Helper.UploadAsync(file, "Survey", UserLog.PartnerNumber);
                    list.Add(filepath);
                }

                zResult.Data = JsonConvert.SerializeObject(list);
                zResult.Success = true;
                
            }
            else
            {
                zResult.Success = false;
                zResult.Message = "Bạn phải chọn hình";
            }
            return Json(zResult);
        }
        public JsonResult SavePhoto(string CompanyKey,string list )
        {
            string zMessage = "";
            var zResult = new ServerResult();
            Photo_Info zinfo;
            if (list != string.Empty)
            {
                var zList = JsonConvert.DeserializeObject<List<string>>(list);
                if (zList.Count > 0)
                {
                    foreach (string s in zList)
                    {
                        zinfo = new Photo_Info();
                        zinfo.Photo.CompanyKey = CompanyKey;
                        zinfo.Photo.PhotoPath = s;
                        zinfo.Photo.PartnerNumber = UserLog.PartnerNumber;
                        zinfo.Photo.CreatedBy = UserLog.UserKey;
                        zinfo.Photo.CreatedName = UserLog.Employee.FullName;
                        zinfo.Photo.ModifiedBy = UserLog.UserKey;
                        zinfo.Photo.ModifiedName = UserLog.Employee.FullName;
                        zinfo.Create_KeyAuto();
                        if (zinfo.Code != "200" && zinfo.Code != "201")
                        {
                            zMessage += zinfo.Message;
                        }
                    }
                }
            }
            if (zMessage == "")
            {
                zResult.Success = true;
                zResult.Message = string.Empty;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zMessage.GetFirstLine();
            }
            return Json(zResult);
        }
        public IActionResult Save(string CompanyKey, string CompanyName, string Phone, string Address, string WardName, string DistrictName, string ProvinceName, string Contact, string Point, string Status, string Description)
        {
            CompanyKey = CompanyKey ?? "";
            Point = Point ?? "0";
            Status = Status ?? "0";
            Description = Description ?? "";
            Contact = Contact ?? "";
            string zStatusName = "";
            if (Status.ToInt() == 1)
                zStatusName = "Tiềm năng";
            else
                zStatusName = "Không tiềm năng";
            var zModel = new Company_Model
            {
                CompanyKey = CompanyKey,
                Phone = Phone.Trim(),
                Address = Address.Trim().ToUpper(),
                WardName = WardName.Trim().ToUpper(),
                DistrictName = DistrictName.Trim().ToUpper(),
                ProvinceName = ProvinceName.Trim().ToUpper(),
                CompanyName = CompanyName.Trim().ToUpper(),
                Contact = Contact.Trim(),
                Point = Point.ToInt(),
                Description = Description,
                Status = Status.ToInt(),
                StatusName = zStatusName,


                PartnerNumber = UserLog.PartnerNumber,
                DepartmentKey = UserLog.Employee.DepartmentKey,
                BranchKey = UserLog.Employee.BranchKey,
                CreatedBy = UserLog.UserKey,
                CreatedName = UserLog.Employee.FullName,
                ModifiedBy = UserLog.UserKey,
                ModifiedName = UserLog.Employee.FullName
            };

            var zInfo = new Company_Info(CompanyKey);
            zInfo.Company = zModel.TrimStringProperties();
            zInfo.Company.ClearNullable();
            if (CompanyKey.Length >= 36)
            {
                zInfo.Update();
            }
            else
            {
                zInfo.Create_KeyManual();
            }

            ServerResult zResult = new ServerResult();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                return RedirectToAction("Edit", "Survey", new { zInfo.Company.CompanyKey });
            }
            else
            {
                TempData["error"] = zInfo.Message.GetFirstLine();
                ViewBag.Message = zInfo.Message.GetFirstLine();
                return View("~/Views/Survey/Edit.cshtml");
            }
        }
        public JsonResult Delete(string CompanyKey)
        {
            CompanyKey = CompanyKey ?? "";
            var zResult = new ServerResult();
            var zInfo = new Company_Info(CompanyKey);
            zInfo.Delete_AllItem();
            if (zInfo.Code == "200")
            {
                zResult.Success = true;
                zResult.Message = string.Empty;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }

            return Json(zResult);
        } 

        public JsonResult Delete_Photo(string AutoKey)
        {
            AutoKey = AutoKey ?? "0";
            var zResult = new ServerResult();
            var zInfo = new Photo_Info(AutoKey.ToInt());
            Helper.DeleteFile(zInfo.Photo.PhotoPath);
            zInfo.Delete();
            if (zInfo.Code == "200")
            {

                zResult.Success = true;
                zResult.Message = string.Empty;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }

            return Json(zResult);
        }

        public JsonResult Check_Company(string Phone)
        {
            var zResult = new ServerResult();
            var zInfo = new Company_Info(Phone, UserLog.PartnerNumber, true);
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zInfo.Company);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = "";
            }
            return Json(zResult);
        }

        public IActionResult Detail_Device(string AutoKey)
        {
            var zInfo = new Device_Info(AutoKey.ToInt());

            if (zInfo.Code != "200" &&
                zInfo.Code != "201")
            {
                ViewBag.Message = zInfo.Message.GetFirstLine();
            }

            return View("/Views/Survey/Detail_Device.cshtml", zInfo.Device);
        }
        public IActionResult Edit_Device(string AutoKey)
        {
            var zInfo = new Device_Info(AutoKey.ToInt());

            if (zInfo.Code != "200" &&
                zInfo.Code != "201")
            {
                ViewBag.Message = zInfo.Message.GetFirstLine();
            }

            return View("/Views/Survey/Edit_Device.cshtml", zInfo.Device);
        }
        public JsonResult Save_Device(string AutoKey, string CompanyKey, string DeviceName, string BrandName, string Model, string Value, string Description)
        {
            AutoKey = AutoKey ?? "0";
            Value = Value ?? "0";
            var zModel = new Device_Model
            {
                AutoKey = AutoKey.ToInt(),
                CompanyKey = CompanyKey,
                DeviceName = DeviceName.Trim().ToUpper(),
                BrandName = BrandName.Trim().ToUpper(),
                Model = Model.Trim().ToUpper(),
                Value = Value.ToFloat(),
                Description = Description,

                PartnerNumber = UserLog.PartnerNumber,
                DepartmentKey = UserLog.Employee.DepartmentKey,
                BranchKey = UserLog.Employee.BranchKey,
                CreatedBy = UserLog.UserKey,
                CreatedName = UserLog.Employee.FullName,
                ModifiedBy = UserLog.UserKey,
                ModifiedName = UserLog.Employee.FullName
            };

            var zInfo = new Device_Info(AutoKey.ToInt());
            zInfo.Device = zModel.TrimStringProperties();
            zInfo.Device.ClearNullable();
            if (AutoKey.ToInt() > 0)
            {
                zInfo.Update();
            }
            else
            {
                zInfo.Create_KeyAuto();
            }

            ServerResult zResult = new ServerResult();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
            }
            return Json(zResult);
        }
        public JsonResult Delete_Device(string AutoKey)
        {
            AutoKey = AutoKey ?? "0";
            var zResult = new ServerResult();
            var zInfo = new Device_Info(AutoKey.ToInt());
            zInfo.Delete();
            if (zInfo.Code == "200")
            {
                zResult.Success = true;
                zResult.Message = string.Empty;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }

            return Json(zResult);
        }

        public IActionResult Edit_Survey(string AutoKey)
        {
            var zInfo = new Survey_Info(AutoKey.ToInt());

            if (zInfo.Code != "200" &&
                zInfo.Code != "201")
            {
                ViewBag.Message = zInfo.Message.GetFirstLine();
            }

            return View("/Views/Survey/Edit_Survey.cshtml", zInfo.Survey);
        }
        public JsonResult Save_Survey(string AutoKey, string CompanyKey, string DateWrite, string Description)
        {
            AutoKey = AutoKey ?? "0";
            DateTime zDate = DateTime.MinValue;
            if (DateWrite != null)
                DateTime.TryParseExact(DateWrite, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zDate);

            var zModel = new Survey_Model
            {
                AutoKey = AutoKey.ToInt(),
                CompanyKey = CompanyKey,
                DateWrite = zDate,
                Description = Description,

                PartnerNumber = UserLog.PartnerNumber,
                DepartmentKey = UserLog.Employee.DepartmentKey,
                BranchKey = UserLog.Employee.BranchKey,
                CreatedBy = UserLog.UserKey,
                CreatedName = UserLog.Employee.FullName,
                ModifiedBy = UserLog.UserKey,
                ModifiedName = UserLog.Employee.FullName
            };

            var zInfo = new Survey_Info(AutoKey.ToInt());
            zInfo.Survey = zModel.TrimStringProperties();
            zInfo.Survey.ClearNullable();
            if (AutoKey.ToInt() > 0)
            {
                zInfo.Update();
            }
            else
            {
                zInfo.Create_KeyAuto();
            }

            ServerResult zResult = new ServerResult();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
            }
            return Json(zResult);
        }
        public JsonResult Delete_Survey(string AutoKey)
        {
            AutoKey = AutoKey ?? "0";
            var zResult = new ServerResult();
            var zInfo = new Survey_Info(AutoKey.ToInt());
            zInfo.Delete();
            if (zInfo.Code == "200")
            {
                zResult.Success = true;
                zResult.Message = string.Empty;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }

            return Json(zResult);
        }

        public IActionResult Report(string btnAction = "", string DateView = "", string FromDate = "", string ToDate = "")
        {

            #region [Datetime string to fromdate - todate]
            int Month = DateTime.Now.Month;
            int Year = DateTime.Now.Year;
            if (DateView != "")
            {
                Month = DateView.Split('/')[0].ToInt();
                Year = DateView.Split('/')[1].ToInt();

                if (Year.ToString().Length < 4)
                {
                    Month = DateTime.Now.Month;
                    Year = DateTime.Now.Year;
                }
            }

            switch (btnAction)
            {
                case "btn_Prev":
                    Month = Month - 1;
                    break;
                case "btn_Next":
                    Month = Month + 1;
                    break;
            }
            if (Month <= 0)
            {
                Month = 12;
                Year = Year - 1;
            }
            if (Month >= 13)
            {
                Month = 1;
                Year = Year + 1;
            }
            DateTime zDateView = DateTime.Now;
            zDateView = new DateTime(Year, Month, 1, 23, 59, 59);
            DateTime zFromDate = new DateTime(zDateView.Year, zDateView.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            #endregion

            //ViewBag.ChartData = DataChart(zFromDate, zToDate);
            ViewBag.BranchData = Survey_Data.ListBranch(UserLog.PartnerNumber, zFromDate, zToDate, out string Message);
            ViewBag.CompanyData = Survey_Data.ListCompany(UserLog.PartnerNumber, zFromDate, zToDate, string.Empty, 0, out Message);
            ViewBag.DeviceData = Survey_Data.ListDevice(UserLog.PartnerNumber, zFromDate, zToDate, string.Empty, 0, out Message);

            //ViewBag.Task = Report.Total_Task(UserLog.PartnerNumber, zFromDate, zToDate);
            //ViewBag.Sales = Report.Total_Sale(UserLog.PartnerNumber, zFromDate, zToDate);
            //ViewBag.Guest = Report.Total_Guest(UserLog.PartnerNumber, zFromDate, zToDate);
            ViewBag.DateView = zToDate.Month + "/" + zToDate.Year;

            ViewBag.Title = "Tổng hợp";

            return View();
        }

        public IActionResult Report_Staff(string btnAction = "", string DateView = "", string FromDate = "", string ToDate = "", string User = "",string UserName="")
        {

            #region [Datetime string to fromdate - todate]
            int Month = DateTime.Now.Month;
            int Year = DateTime.Now.Year;
            if (DateView != "")
            {
                Month = DateView.Split('/')[0].ToInt();
                Year = DateView.Split('/')[1].ToInt();

                if (Year.ToString().Length < 4)
                {
                    Month = DateTime.Now.Month;
                    Year = DateTime.Now.Year;
                }
            }

            switch (btnAction)
            {
                case "btn_Prev":
                    Month = Month - 1;
                    break;
                case "btn_Next":
                    Month = Month + 1;
                    break;
            }
            if (Month <= 0)
            {
                Month = 12;
                Year = Year - 1;
            }
            if (Month >= 13)
            {
                Month = 1;
                Year = Year + 1;
            }
            DateTime zDateView = DateTime.Now;
            zDateView = new DateTime(Year, Month, 1, 23, 59, 59);
            DateTime zFromDate = new DateTime(zDateView.Year, zDateView.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            #endregion

            
            ViewBag.BranchData = Survey_Data.ListStaff(UserLog.PartnerNumber, zFromDate, zToDate, User, 0, out string Message);
            ViewBag.CompanyData = Survey_Data.ListCompany(UserLog.PartnerNumber, zFromDate, zToDate, User, 0, out Message);
            ViewBag.DeviceData = Survey_Data.ListDevice(UserLog.PartnerNumber, zFromDate, zToDate, User, 0, out Message);
            ViewBag.ListBranch = Branch_Data.List(UserLog.PartnerNumber);
            ViewBag.ListEmployee = Employee_Data.ListUser(UserLog.PartnerNumber,out string zMess); 
            ViewBag.DateView = zToDate.Month + "/" + zToDate.Year;
            if (UserName==null||UserName == "")
                ViewBag.Title = "Tổng hợp nhân viên";
            else
                ViewBag.Title = UserName.Substring(1);
            ViewBag.UserKey = User;
            return View();
        }
        public IActionResult Report_Branch(string btnAction = "", string DateView = "", string FromDate = "", string ToDate = "",int Branch=0, string BranchName = "")
        {

            #region [Datetime string to fromdate - todate]
            int Month = DateTime.Now.Month;
            int Year = DateTime.Now.Year;
            if (DateView != "")
            {
                Month = DateView.Split('/')[0].ToInt();
                Year = DateView.Split('/')[1].ToInt();

                if (Year.ToString().Length < 4)
                {
                    Month = DateTime.Now.Month;
                    Year = DateTime.Now.Year;
                }
            }

            switch (btnAction)
            {
                case "btn_Prev":
                    Month = Month - 1;
                    break;
                case "btn_Next":
                    Month = Month + 1;
                    break;
            }
            if (Month <= 0)
            {
                Month = 12;
                Year = Year - 1;
            }
            if (Month >= 13)
            {
                Month = 1;
                Year = Year + 1;
            }
            DateTime zDateView = DateTime.Now;
            zDateView = new DateTime(Year, Month, 1, 23, 59, 59);
            DateTime zFromDate = new DateTime(zDateView.Year, zDateView.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            #endregion
            //ViewBag.ChartData = DataChart(zFromDate, zToDate);
            ViewBag.BranchData = Survey_Data.ListStaff(UserLog.PartnerNumber, zFromDate, zToDate, UserLog.UserKey, Branch, out string Message);
            ViewBag.CompanyData = Survey_Data.ListCompany(UserLog.PartnerNumber, zFromDate, zToDate, UserLog.UserKey, Branch, out Message);
            ViewBag.DeviceData = Survey_Data.ListDevice(UserLog.PartnerNumber, zFromDate, zToDate, UserLog.UserKey, Branch, out Message);
            ViewBag.ListBranch = Branch_Data.List(UserLog.PartnerNumber);
            ViewBag.DateView = zToDate.Month + "/" + zToDate.Year;

            if (BranchName == null || BranchName == "")
                ViewBag.Title = "Tổng hợp chi nhánh";
            else
                ViewBag.Title = BranchName;
            ViewBag.Branch = Branch;

            return View();
        }

        public JsonResult GhimMap(string CompanyKey, string Lat, string Long)
        {
            CompanyKey = CompanyKey ?? "";
            var zResult = new ServerResult();
            var zInfo = new Company_Info(CompanyKey);
            zInfo.Company.Lat = Lat;
            zInfo.Company.Long = Long;
            zInfo.Update();
            if (zInfo.Code == "200")
            {
                zResult.Success = true;
                zResult.Message = string.Empty;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }

            return Json(zResult);
        }

        public JsonResult ListMap()
        {
            var zResult = new ServerResult();

            zResult.Success = true;
            zResult.Message = string.Empty;
            string JSONresult;
            JSONresult = JsonConvert.SerializeObject(Survey_Data.ListMap(UserLog.PartnerNumber,string.Empty,string.Empty,0));
            zResult.Data = JSONresult;
            return Json(zResult);
        }
        public JsonResult Search_Map(string Name, string Phone, string Branch)
        {
            Name = Name ?? "";
            Phone = Phone ?? "";
            Branch = Branch == null ? UserLog.DataAccess : "'" + Branch + "'";
            var zResult = new ServerResult();

            zResult.Success = true;
            zResult.Message = string.Empty;
            string JSONresult;
            JSONresult = JsonConvert.SerializeObject(Survey_Data.ListMap(UserLog.PartnerNumber, Name,Phone,Branch.ToInt()));
            zResult.Data = JSONresult;
            return Json(zResult);
        }
    }
}
