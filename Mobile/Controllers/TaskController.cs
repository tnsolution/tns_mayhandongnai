﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Mobile.Controllers
{
    public class TaskController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Default()
        {
            return View();
        }

        //--------------Ngoai cua hang
        #region ---OutSite---
        public IActionResult OutSite_List(int CategoryKey)
        {
            var List = new List<Task_Model>();
            string Message = "";

            //kỹ thuật
            if (CategoryKey > 26)
            {
                if (UserLog.Employee.PositionKey == 54)
                {
                    List = Task_Data.Personal(UserLog.PartnerNumber, CategoryKey, UserLog.UserKey, out Message);
                    ViewBag.ListData = List;
                }
                else
                {
                    List = Task_Data.List(UserLog.PartnerNumber, CategoryKey, UserLog.DataAccess, out Message);
                    ViewBag.ListData = List;
                }
            }
            else
            {
                List = Task_Data.List(UserLog.PartnerNumber, CategoryKey, string.Empty, out Message);
                ViewBag.ListData = List;
            }

            ViewBag.ListData = List;
            ViewBag.ActiveCate = CategoryKey;
            ViewBag.Message = Message;
            return View("~/Views/Task/OutSite/List.cshtml");
        }
        public IActionResult OutSite_Detail(string TaskKey)
        {
            ViewBag.ListEmployee = Employee_Data.List(UserLog.PartnerNumber);

            TaskKey = TaskKey ?? "";
            var zTitle = "";
            var zTask = new Task_Model();
            if (TaskKey != string.Empty)
            {
                var zInfo = new Task_Info(TaskKey);
                zTask = zInfo.Task;
                zTitle = "Chi tiết";
            }
            int CategoryKey = zTask.CategoryKey;
            string btnAction = "";
            switch (CategoryKey)
            {
                case 26:
                    btnAction += @"<button id='btnYes' type='button' class='btn-sm btn btn-primary float-right'><i class='fas fa-check'></i> Nhận việc</button>";
                    break;

                case 27:
                    if (UserLog.UserKey == "7fa5dc11-e327-4636-a3a1-8af49dc8415a")
                    {
                        btnAction += @"<button id='btnChange' type='button' class='btn-sm btn btn-primary'><i class='fas fa-exchange-alt'></i> Đổi người</button>";
                    }

                    if (zTask.Style == "SM")
                    {
                        btnAction += @"<button id='btnYes' type='button' class='btn-sm btn btn-primary float-right'><i class='fas fa-check'></i> Đã sửa xong</button>";
                        btnAction += @"<button id='btnNo' type='button' class='btn-sm btn btn-primary float-right mr-1'><i class='fas fa-stop-circle'></i> Lấy máy về</button>";
                    }
                    else
                    {
                        btnAction += @"<button id='btnYes' type='button' class='btn-sm btn btn-primary float-right'><i class='fas fa-check'></i> Đã giao xong</button>";
                    }
                    break;

                    //case 28:
                    //    btnAction += @"<button id='btnYes' type='button' class='btn-sm btn btn-primary float-right'><i class='fas fa-check'></i>Đã sửa xong</button>";
                    //    btnAction += @"<button id='btnNo' type='button' class='btn-sm btn btn-primary float-right mr-1'><i class='fas fa-stop-circle'></i>Không sửa được</button>";
                    //    break;

                    //case 29:
                    //    btnAction += @"<button id='btnYes' type='button' class='btn-sm btn btn-primary float-right'><i class='fas fa-check'></i>Đã giao</button>";
                    //    break;
            }
            ViewBag.Button = btnAction;
            ViewBag.TitleTask = zTitle;
            return View("~/Views/Task/OutSite/Detail.cshtml", zTask);
        }
        public IActionResult OutSite_Edit(string TaskKey)
        {
            ViewBag.ListCustomer = Customer_Data.List(UserLog.PartnerNumber, out string Message);

            TaskKey = TaskKey ?? "";
            var zTitle = "";
            var zTask = new Task_Model();
            if (TaskKey != string.Empty)
            {
                var zInfo = new Task_Info(TaskKey);
                zTask = zInfo.Task;
                zTitle = "Chỉnh sửa";
            }
            else
            {
                zTask.CategoryKey = 26;
                zTitle = "Tạo mới";
            }

            ViewBag.TitleTask = zTitle;
            return View("~/Views/Task/OutSite/Edit.cshtml", zTask);
        }
        public IActionResult OutSite_Process(string TaskKey, int CategoryKey, int Status, string FixNote, string AutoTask, string Todo)
        {
            FixNote = FixNote ?? "";
            var zResult = new ServerResult();
            var zInfo = new Task_Info(TaskKey);
            string zSQL = "UPDATE CRM_Task SET ";
            string StatusName = "";
            string CategoryName = "";

            switch (CategoryKey)
            {
                case 26:     //1. Việc mới    1 - Nhận việc   
                    {
                        if (Status == 1)
                        {
                            StatusName = "Nhận việc";
                            CategoryKey = CategoryKey + 1;
                            CategoryName = Task_Data.TaskCategoryName(UserLog.PartnerNumber, CategoryKey);
                            zSQL += "";
                            zSQL += "StartDate = GETDATE(), ";
                            zSQL += "StatusKey = " + Status + ", ";
                            zSQL += "StatusName = N'" + StatusName + "', ";
                            zSQL += "CategoryKey = " + CategoryKey + ", ";
                            zSQL += "CategoryName = N'" + CategoryName + "', ";
                            zSQL += "OwnerBy = N'" + UserLog.UserKey + "', ";
                            zSQL += "OwnerName = N'" + UserLog.Employee.FullName + "', ";
                            zSQL += "OwnerKey = '" + UserLog.Employee.EmployeeKey + "', ";
                            zSQL += "OwnerPhone = '" + UserLog.Employee.MobiPhone + "', ";
                            zSQL += "BranchKey = '" + UserLog.Employee.BranchKey + "', ";
                        }
                    }
                    break;

                case 27:    //2. Giao máy/ đi sửa		1 - Giao xong/ sửa xong 	0 - nhận máy về
                    {
                        if (Status == 1)
                        {
                            if (zInfo.Task.Style == "GH")
                            {
                                StatusName = "Giao xong";
                                CategoryKey = 30;
                                CategoryName = Task_Data.TaskCategoryName(UserLog.PartnerNumber, CategoryKey);

                                zSQL += "CodeLine= 'Y',";
                                zSQL += "StatusKey = " + Status + ", ";
                                zSQL += "StatusName = N'" + StatusName + "', ";
                                zSQL += "CategoryKey = " + CategoryKey + ", ";
                                zSQL += "CategoryName = N'" + CategoryName + "', ";
                            }
                            if (zInfo.Task.Style == "SM")
                            {
                                StatusName = "Sửa xong";
                                CategoryKey = 30;
                                CategoryName = Task_Data.TaskCategoryName(UserLog.PartnerNumber, CategoryKey);
                                zSQL += "CodeLine= 'Y',";
                                zSQL += "StatusKey = " + Status + ", ";
                                zSQL += "StatusName = N'" + StatusName + "', ";
                                zSQL += "CategoryKey = " + CategoryKey + ", ";
                                zSQL += "CategoryName = N'" + CategoryName + "', ";
                                zSQL += "ToDo = N'" + Todo + "', ";
                            }
                        }
                        else
                        {
                            if (zInfo.Task.Style == "SM")
                            {
                                StatusName = "Không thể sửa tại chỗ khách";
                                CategoryKey = 30;
                                CategoryName = Task_Data.TaskCategoryName(UserLog.PartnerNumber, CategoryKey);
                                zSQL += "CodeLine= 'N',";
                                zSQL += "Note = N'" + FixNote + "', ";
                                zSQL += "StatusKey = " + Status + ", ";
                                zSQL += "StatusName = N'" + StatusName + "', ";
                                zSQL += "CategoryKey = " + CategoryKey + ", ";
                                zSQL += "CategoryName = N'" + CategoryName + "', ";
                            }
                        }
                    }
                    break;

                case 28:    //3. Nhận máy về    1 - Sửa xong
                    {
                        if (Status == 1)
                        {
                            StatusName = "Sửa xong";
                            CategoryKey = CategoryKey + 1;
                            CategoryName = Task_Data.TaskCategoryName(UserLog.PartnerNumber, CategoryKey);
                            zSQL += "ToDo = N'" + Todo + "', ";
                            zSQL += "CodeLine= 'Y',";
                            zSQL += "StatusKey = " + Status + ", ";
                            zSQL += "StatusName = N'" + StatusName + "', ";
                            zSQL += "CategoryKey = " + CategoryKey + ", ";
                            zSQL += "CategoryName = N'" + CategoryName + "', ";
                        }
                        else
                        {
                            //không sửa được
                            StatusName = "Không sửa được";
                            CategoryKey = CategoryKey + 1;
                            CategoryName = Task_Data.TaskCategoryName(UserLog.PartnerNumber, CategoryKey);
                            zSQL += "Note = N'" + FixNote + "', ";
                            zSQL += "CodeLine= 'N',";
                            zSQL += "StatusKey = " + Status + ", ";
                            zSQL += "StatusName = N'" + StatusName + "', ";
                            zSQL += "CategoryKey = " + CategoryKey + ", ";
                            zSQL += "CategoryName = N'" + CategoryName + "', ";
                        }
                    }
                    break;

                    //case 29:    //4. Chờ giao/ trả máy  1 - Đã gọi
                    //    {
                    //        if (Status == 1)
                    //        {
                    //            StatusName = "Đã gọi";
                    //            CategoryKey = CategoryKey + 1;
                    //            CategoryName = Task_Data.TaskCategoryName(UserLog.PartnerNumber, CategoryKey);

                    //            zSQL += "CompleteDate = GETDATE(), ";
                    //            zSQL += "StatusKey = " + Status + ", ";
                    //            zSQL += "StatusName = N'" + StatusName + "', ";
                    //            zSQL += "CategoryKey = " + CategoryKey + ", ";
                    //            zSQL += "CategoryName = N'" + CategoryName + "', ";
                    //        }
                    //    }
                    //    break;
            }

            if (!zSQL.Contains(","))
            {
                TempData["error"] = zInfo.Message.GetFirstLine();
                return RedirectToAction("OutSite_Detail", new { TaskKey });
            }

            zSQL = zSQL.Remove(zSQL.LastIndexOf(','), 1);
            zSQL += " WHERE TaskKey = '" + TaskKey + "'";

            zInfo.Process(zSQL);
            if (zInfo.Code != "200" &&
                zInfo.Code != "201")
            {
                TempData["error"] = zInfo.Message.GetFirstLine();
                return RedirectToAction("OutSite_Detail", new { TaskKey });
            }
            else
            {
                #region ---Auto Create Task---
                if (AutoTask.ToInt() > 0)
                {
                    var Model = zInfo.Task;
                    string CustomerKey = Model.CustomerKey;
                    string CustomerName = Model.CustomerName;
                    string CustomerPhone = Model.CustomerPhone;
                    string CustomerAddress = Model.CustomerAddress;

                    StringBuilder zSb = new StringBuilder();
                    zSb.AppendLine("Người lấy máy về: " + Model.OwnerName);
                    zSb.AppendLine("Nội dung: " + FixNote);

                    OnSite_Save(2, CustomerKey, CustomerName, CustomerPhone, CustomerAddress, zSb.ToString(), UserLog.Employee.BranchKey.ToString());
                }
                #endregion

                #region ---Save Log---
                var zModel = new Task_Model
                {
                    TaskKey = TaskKey,
                    CategoryKey = CategoryKey,
                    CategoryName = CategoryName,
                    StatusKey = Status,
                    StatusName = StatusName,
                    Note = FixNote
                };
                Save_Log(zModel, FixNote);
                #endregion
                return RedirectToAction("OutSite_List", new { CategoryKey });
            }
        }
        public IActionResult OutSite_Save(string TaskKey, string CustomerKey, string CustomerName, string CustomerPhone, string CustomerAddress, string TaskContent, int CategoryKey, string Style)
        {
            TaskKey = TaskKey ?? "";
            bool isValid = Guid.TryParse(CustomerKey, out Guid GuidKey);
            if (!isValid)
            {
                CustomerName = CustomerKey;
                CustomerKey = Save_Customer(CustomerPhone, CustomerName, CustomerAddress);
            }

            var zInfo = new Task_Info();
            var zTask = new Task_Model
            {
                TaskKey = TaskKey,

                //customer
                CustomerKey = CustomerKey,
                CustomerName = CustomerName,
                CustomerPhone = CustomerPhone.Trim(),
                CustomerAddress = CustomerAddress.Trim(),

                //info task
                Style = Style,// SM, GH
                GroupKey = 20,
                GroupName = "Việc ngoài cửa hàng",
                TaskContent = TaskContent.Trim(),
                CategoryKey = CategoryKey,
                CategoryName = Task_Data.TaskCategoryName(UserLog.PartnerNumber, CategoryKey),

                //Non SQL data
                PartnerNumber = UserLog.PartnerNumber,
                DepartmentKey = UserLog.Employee.DepartmentKey,
                BranchKey = UserLog.Employee.BranchKey,
                CreatedBy = UserLog.UserKey,
                CreatedName = UserLog.Employee.FullName,
                ModifiedBy = UserLog.UserKey,
                ModifiedName = UserLog.Employee.FullName,
            };

            zTask.ClearNullable();
            if (TaskKey != string.Empty)
            {
                zInfo = new Task_Info(TaskKey)
                {
                    Task = zTask
                };
                zInfo.Update();
            }
            else
            {
                zTask.TaskKey = Guid.NewGuid().ToString();
                zTask.TaskID = Task_Data.AutoID("RN", UserLog.PartnerNumber);
                zTask.StatusKey = 1;
                zTask.StatusName = "Việc mới gửi";
                zInfo = new Task_Info
                {
                    Task = zTask
                };
                zInfo.Create_ClientKey();

                //save log
                Save_Log(zTask, string.Empty);
            }

            if (zInfo.Code != "200" &&
                zInfo.Code != "201")
            {
                TempData["error"] = zInfo.Message.GetFirstLine();
                return View("~/Views/Task/OutSite/Edit.cshtml", zTask);
            }
            else
            {
                return RedirectToAction("OutSite_List", new { CategoryKey });
            }
        }
        #endregion

        //--------------Share function
        #region ---Function---
        public JsonResult Archive(string TaskKey)
        {
            var zResult = new ServerResult();
            
            string zSQL = "UPDATE CRM_Task SET Publish = 0 WHERE TaskKey = '" + TaskKey + "'";

            var zInfo = new Task_Info();
            zInfo.Process(zSQL);

            if (zInfo.Code != "200" &&
                zInfo.Code != "201")
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
                return Json(zResult);
            }
            else
            {
                zResult.Success = true;
                return Json(zResult);
            }          
        }

        public IActionResult Search(string Name, string Phone, int CategoryKey)
        {
            Name = Name ?? "";
            Phone = Phone ?? "";

            var List = Task_Data.Search(UserLog.PartnerNumber, CategoryKey, Name, Phone, UserLog.DataAccess, out string Message);
            ViewBag.ListData = List;
            ViewBag.Cate = CategoryKey;
            if (CategoryKey >= 21 && CategoryKey <= 25)
            {
                return View("~/Views/Task/InPlace/List.cshtml");
            }
            else
            {
                return View("~/Views/Task/OutSite/List.cshtml");
            }
        }
        public JsonResult Delete(string TaskKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Task_Info();
            zInfo.Task.TaskKey = TaskKey;
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }

            return Json(zResult);
        }
        private string Save_Customer(string Phone, string Name, string Address)
        {
            var zKey = Guid.NewGuid().ToString();
            var zInfo = new Customer_Info();
            var zModel = new Customer_Model()
            {
                Phone = Phone,
                FullName = Name,
                Address = Address,
                CustomerKey = zKey,
                CategoryKey = 9,
                CategoryName = "Vãng lai/ chưa giao dịch",
                DepartmentKey = UserLog.Employee.DepartmentKey,
                BranchKey = UserLog.Employee.BranchKey,
                PartnerNumber = UserLog.PartnerNumber,
                CreatedBy = UserLog.Employee.FullName,
                ModifiedBy = UserLog.Employee.FullName,
            };

            zInfo.Customer = zModel;
            zInfo.Create_ClientKey();
            return zKey;
        }

        //private string Save_Customer(string Phone, string Name, string Address, string CustomerKey)
        //{
        //    string zKey = "";
        //    bool isValid = Guid.TryParse(CustomerKey, out Guid GuidKey);
        //    if (!isValid)
        //    {
        //        zKey = Guid.NewGuid().ToString();
        //        var zInfo = new Customer_Info();
        //        var zModel = new Customer_Model()
        //        {
        //            Phone = Phone,
        //            FullName = Name,
        //            Address = Address,
        //            CustomerKey = zKey,
        //            CategoryKey = 9,
        //            CategoryName = "Vãng lai/ chưa giao dịch",
        //            DepartmentKey = UserLog.Employee.DepartmentKey,
        //            BranchKey = UserLog.Employee.BranchKey,
        //            PartnerNumber = UserLog.PartnerNumber,
        //            CreatedBy = UserLog.Employee.FullName,
        //            ModifiedBy = UserLog.Employee.FullName,
        //        };

        //        zInfo.Customer = zModel;
        //        zInfo.Create_ClientKey();
        //    }
        //    else
        //    {
        //        zKey = CustomerKey;
        //        var zInfo = new Customer_Info(CustomerKey);
        //        zInfo.Customer.Phone = Phone;
        //        zInfo.Customer.FullName = Name;
        //        zInfo.Customer.Address = Address;
        //        zInfo.Customer.ModifiedBy = UserLog.Employee.FullName,
        //        zInfo.Update();
        //    }

        //    return zKey;
        //}

        private void Save_Log(Task_Model Model, string Description)
        {
            var zLog = new Task_Log_Info();
            var zModel = new Task_Log_Model
            {
                TaskKey = Model.TaskKey,
                CategoryKey = Model.CategoryKey.ToString(),
                CategoryName = Model.CategoryName,
                StatusKey = Model.StatusKey.ToString(),
                StatusName = Model.StatusName,
                Description = Description
            };

            zLog.Task_Log = zModel;
            zLog.Create_ServerKey();
        }
        private void Save_Money(Task_Model Model)
        {
            string TaskKey = Model.TaskKey,
                TaskID = Model.TaskID,
                DoneBy = Model.OwnerName;
            double Amount = Model.TaskAmount;

            var zInfo = new Receipt_Info();
            var zReceipt = new Receipt_Model
            {
                ReceiptID = Receipt_Data.AutoID("T", UserLog.PartnerNumber),
                ReceiptDescription = "Thu tiền công việc có mã số [" + TaskID + "] người thực hiện " + DoneBy,
                AmountCurrencyMain = Amount,
                DocumentID = TaskKey,
                DepartmentKey = UserLog.Employee.DepartmentKey,
                BranchKey = UserLog.Employee.BranchKey,
                ReceiptDate = DateTime.Now,
                PartnerNumber = UserLog.PartnerNumber,
                CreatedBy = UserLog.UserKey,
                CreatedName = UserLog.Employee.FullName,
                ModifiedBy = UserLog.UserKey,
                ModifiedName = UserLog.Employee.FullName,
            };

            zInfo.Receipt = zReceipt;
            zInfo.Create_ServerKey();

            var zCus = new Customer_Info(Model.CustomerKey);
            zCus.Customer.CategoryKey = 10;
            zCus.Customer.CategoryName = "Khách đã giao dịch";
            zCus.Customer.ModifiedBy = UserLog.UserKey;
            zCus.Customer.ModifiedName = UserLog.Employee.FullName;
            zCus.HasPaid();
        }
        public JsonResult Change_Employee(string TaskKey, string Employee, string ToDo)
        {
            var zUser = new User_Info(Employee, true);
            var zUserModel = zUser.User;
            var zEmployee = new Employee_Info(Employee);
            var zModelEmployee = zEmployee.Employee;

            ToDo = ToDo ?? "";
            string zSQL = "";
            if (ToDo != string.Empty)
            {
                zSQL = @"UPDATE CRM_Task SET " +
                    "ToDo = N'" + ToDo + "', " +
                    "OwnerBy = N'" + zUserModel.UserKey + "', " +
                    "OwnerName = N'" + zModelEmployee.FullName + "', " +
                    "OwnerKey = N'" + zModelEmployee.EmployeeKey + "', " +
                    "DepartmentKey = N'" + zModelEmployee.DepartmentKey + "', " +
                    "BranchKey = N'" + zModelEmployee.BranchKey + "' " +
                    "WHERE TaskKey = '" + TaskKey + "' ";
            }
            else
            {
                zSQL = @"UPDATE CRM_Task SET " +
                    "OwnerBy = N'" + zUserModel.UserKey + "', " +
                    "OwnerName = N'" + zModelEmployee.FullName + "', " +
                    "OwnerKey = N'" + zModelEmployee.EmployeeKey + "', " +
                    "DepartmentKey = N'" + zModelEmployee.DepartmentKey + "', " +
                    "BranchKey = N'" + zModelEmployee.BranchKey + "' " +
                    "WHERE TaskKey = '" + TaskKey + "' ";
            }

            var zResult = new ServerResult();
            var zInfo = new Task_Info();
            zInfo.Process(zSQL);
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }

            return Json(zResult);
        }
        public JsonResult Get_CustomerPhone(string Phone)
        {
            var zResult = new ServerResult();
            var zInfo = new Customer_Info(Phone, UserLog.PartnerNumber, true);
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zInfo.Customer);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = "";
            }
            return Json(zResult);
        }
        public JsonResult Get_CustomerKey(string CustomerKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Customer_Info(CustomerKey);
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zInfo.Customer);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = "";
            }
            return Json(zResult);
        }

        #endregion

        //--------------Tracking Task function
        #region ---Tracking---

        public IActionResult Tracking(int Style = 1, int Group = 0)
        {
            var ListStaff = Employee_Data.List(UserLog.PartnerNumber);
            ViewBag.ListStaff = ListStaff;
            var ListBranch = Branch_Data.List(UserLog.PartnerNumber);
            ViewBag.ListBranch = ListBranch;
            //----------------------------------------------------------------------------

            string Cate = "";
            switch (Style)
            {
                //chưa thực hiện
                case 1:
                    Cate = "21,26";
                    break;
                //đang làm
                case 2:
                    Cate = "22,23,27,28";
                    break;
                //đã hoàn thành
                case 3:
                    Cate = "24,29";
                    break;
                //đã trả máy
                case 4:
                    Cate = "25,30";
                    break;
            }
            var zList = Task_Data.Tracking(UserLog.PartnerNumber, Group, string.Empty, string.Empty, string.Empty, Cate, out string Message);

            ViewBag.Count = Count_Task();
            ViewBag.ListTask = zList;
            return View("~/Views/Task/Track/List.cshtml");
        }
        public IActionResult Tracking_Filter(
    int Style = 1, int Group = 0, string Branch = "", string Employee = "",
    string GuestName = "", string GuestPhone = "", string FromDate = "", string ToDate = "")
        {
            var ListStaff = Employee_Data.List(UserLog.PartnerNumber);
            ViewBag.ListStaff = ListStaff;
            var ListBranch = Branch_Data.List(UserLog.PartnerNumber);
            ViewBag.ListBranch = ListBranch;
            //----------------------------------------------------------------------------

            Branch = Branch ?? "";
            Employee = Employee ?? "";
            GuestName = GuestName ?? "";
            GuestPhone = GuestPhone ?? "";

            DateTime zFromDate = DateTime.MinValue;
            if (FromDate != null && FromDate != string.Empty)
            {
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
            }
            DateTime zToDate = DateTime.MinValue;
            if (ToDate != null && ToDate != string.Empty)
            {
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            }

            string Cate = "";
            switch (Style)
            {
                //chưa thực hiện
                case 1:
                    Cate = "21,26";
                    break;
                //đang làm
                case 2:
                    Cate = "22,23,27,28";
                    break;
                //đã hoàn thành
                case 3:
                    Cate = "24,29";
                    break;
                //đã trả máy
                case 4:
                    Cate = "25,30";
                    break;
            }
            var zList = Task_Data.Tracking(UserLog.PartnerNumber, Group, Employee, GuestName, GuestPhone, Cate, zFromDate, zToDate, out string Message);

            ViewBag.Count = Count_Task();
            ViewBag.ListTask = zList;
            ViewBag.Style = Style;
            return View("~/Views/Task/Track/List.cshtml");
        }
        public IActionResult Tracking_Detail(string TaskKey)
        {
            TaskKey = TaskKey ?? "";
            var zTask = new Task_Model();
            if (TaskKey != string.Empty)
            {
                var zInfo = new Task_Info(TaskKey);
                zTask = zInfo.Task;
            }

            var Logs = Task_Data.Log(TaskKey, out string Message);
            ViewBag.History = Logs;
            return View("~/Views/Task/Track/Detail.cshtml", zTask);
        }
        private List<string> Count_Task()
        {
            var data = new List<string>();

            string Cate = "21,26";
            int Count = Task_Data.Count(UserLog.PartnerNumber, Cate);
            data.Add(Count.ToString());

            //chưa thực hiện
            Cate = "22,23,27,28";
            Count = Task_Data.Count(UserLog.PartnerNumber, Cate);
            data.Add(Count.ToString());

            //đang làm
            Cate = "24,29";
            Count = Task_Data.Count(UserLog.PartnerNumber, Cate);
            data.Add(Count.ToString());

            //đã hoàn thành
            Cate = "25,30";
            Count = Task_Data.Count(UserLog.PartnerNumber, Cate);
            data.Add(Count.ToString());

            return data;
        }
        #endregion

        //--------------UPDATE NEW
        public IActionResult SetInput(int Option)
        {
            ViewBag.ListCustomer = Customer_Data.List(UserLog.PartnerNumber, out string Message);
            return PartialView("~/Views/Task/OnSite/_SelectCustomer.cshtml");

            //if (Option == 1)
            //{
            //    return PartialView("~/Views/Task/OnSite/_InputCustomer.cshtml");
            //}
            //else
            //{
            //    ViewBag.ListCustomer = Customer_Data.List(UserLog.PartnerNumber, out string Message);
            //    return PartialView("~/Views/Task/OnSite/_SelectCustomer.cshtml");
            //}
        }

        public IActionResult OnSite_New()
        {
            var ListBranch = Branch_Data.List(UserLog.PartnerNumber);
            ViewBag.ListBranch = ListBranch;

            var ListOption = Helper.ListOption();
            ViewBag.ListOption = ListOption;

            return View("~/Views/Task/OnSite/New.cshtml", new Task_Model());
        }

        public IActionResult OnSite_Save(int OptionTask, string CustomerKey, string CustomerName, string CustomerPhone, string CustomerAddress, string TaskContent, string Branch)
        {
            var zInfo = new Task_Info();

            bool isValid = Guid.TryParse(CustomerKey, out Guid GuidKey);
            if (!isValid)
            {
                CustomerName = CustomerKey;
                CustomerKey = Save_Customer(CustomerPhone, CustomerName, CustomerAddress);

                //if (OptionTask == 1)
                //{
                //    CustomerKey = Save_Customer(CustomerPhone, CustomerName, CustomerAddress);
                //}
                //else
                //{
                //    CustomerName = CustomerKey;
                //    CustomerKey = Save_Customer(CustomerPhone, CustomerName, CustomerAddress);
                //}
            }

            var zTask = new Task_Model
            {
                //customer
                CustomerKey = CustomerKey,
                CustomerName = CustomerName.Trim(),
                CustomerPhone = CustomerPhone.Trim(),
                CustomerAddress = CustomerAddress.Trim(),

                //info task
                TaskKey = Guid.NewGuid().ToString(),
                TaskID = Task_Data.AutoID("CH", UserLog.PartnerNumber),
                OptionTask = OptionTask,
                GroupKey = 19,
                CategoryKey = 21,
                StatusKey = 1,
                StatusName = "Mới gửi y/c",
                CategoryName = "1. Đang báo giá",
                GroupName = "Việc tại cửa hàng",
                TaskContent = TaskContent.Trim(),

                //Non SQL data
                PartnerNumber = UserLog.PartnerNumber,
                CreatedBy = UserLog.UserKey,
                CreatedName = UserLog.Employee.FullName,
                ModifiedBy = UserLog.UserKey,
                ModifiedName = UserLog.Employee.FullName,
            };

            zTask.ClearNullable();

            zInfo = new Task_Info
            {
                Task = zTask
            };

            //nếu chỉ định cửa hàng
            if (Branch != null )
            {
                zTask.BranchKey = Branch.ToInt();
            }
            else
            {
                zTask.BranchKey = UserLog.Employee.BranchKey;
            }

            zInfo.Create_ClientKey();
            if (zInfo.Code != "200" &&
                zInfo.Code != "201")
            {
                TempData["error"] = zInfo.Message.GetFirstLine();
                return View("~/Views/Task/OnSite/New.cshtml", zTask);
            }
            else
            {
                switch (OptionTask)
                {
                    //Khách vãng lai
                    default:
                    case 1:
                        return RedirectToAction("OnSite_List", new { Option = 1, Branch = string.Empty });

                    //Máy mang về
                    case 2:
                        return RedirectToAction("OnSite_List", new { Option = 2, Branch = string.Empty });

                    //Chỉ định cửa hàng
                    case 3:
                        return RedirectToAction("OnSite_List", new { Option = 3, Branch = Branch, Category = 21 });
                }
            }
        }

        public IActionResult OnSite_Update(string TaskKey, int OptionTask, string CustomerKey, string CustomerName, string CustomerPhone, string CustomerAddress, string TaskContent, string Branch)
        {
            var zInfo = new Task_Info(TaskKey);

            bool isValid = Guid.TryParse(CustomerKey, out Guid GuidKey);
            if (!isValid)
            {
                CustomerKey = Save_Customer(CustomerPhone, CustomerName, CustomerAddress);
            }

            var zTask = zInfo.Task;
            //zTask.CustomerKey = CustomerKey;
            //zTask.CustomerName = CustomerName.Trim();
            //zTask.CustomerPhone = CustomerPhone.Trim();
            //zTask.CustomerAddress = CustomerAddress.Trim();
            //zTask.OptionTask = OptionTask;

            zTask.TaskContent = TaskContent.Trim();
            zTask.ModifiedBy = UserLog.UserKey;
            zTask.ModifiedName = UserLog.Employee.FullName;
            //nếu chỉ định cửa hàng
            if (Branch != null )
            {
                zTask.BranchKey = Branch.ToInt();
            }

            zTask.ClearNullable();
            zInfo.Task = zTask;
            zInfo.Update();
            if (zInfo.Code != "200" &&
                zInfo.Code != "201")
            {
                TempData["error"] = zInfo.Message.GetFirstLine();
                return View("~/Views/Task/OnSite/New.cshtml", zTask);
            }
            else
            {
                switch (zTask.OptionTask)
                {
                    //Khách vãng lai
                    default:
                    case 1:
                        return RedirectToAction("OnSite_List", new { Option = 1, Branch = string.Empty });

                    //Máy mang về
                    case 2:
                        return RedirectToAction("OnSite_List", new { Option = 2, Branch = string.Empty });

                    //Chỉ định cửa hàng
                    case 3:
                        return RedirectToAction("OnSite_List", new { Option = 3, Branch = Branch, Category = zTask.CategoryKey });
                }
            }
        }

        public IActionResult OnSite_List(int Option, string Branch, int Category)
        {
            if (Category == 0)
            {
                Category = 21;
            }
            Branch = Branch ?? "";
            var zList = new List<Task_Model>();

            //đã có người nhận việc
            if (Category > 21)
            {
                //cá nhân
                if (UserLog.Employee.PositionKey == 54)
                {
                    zList = Task_Data.OnSite_List(UserLog.PartnerNumber, out _, Option, Branch, Category, UserLog.UserKey);
                }
                else
                {
                    //quản lý
                    zList = Task_Data.OnSite_List(UserLog.PartnerNumber, out _, Option, Branch, Category, string.Empty);
                }
            }
            else
            {
                //chưa ai nhận việc
                zList = Task_Data.OnSite_List(UserLog.PartnerNumber, out _, Option, Branch, Category, string.Empty);
            }

            ViewBag.Data = zList;

            switch (Option)
            {
                default:
                case 1:

                    //ViewBag.Cate1 = zList.Count(s => s.CategoryKey == 21);
                    //ViewBag.Cate2 = zList.Count(s => s.CategoryKey == 22);
                    //ViewBag.Cate3 = zList.Count(s => s.CategoryKey == 23);
                    //ViewBag.Cate4 = zList.Count(s => s.CategoryKey == 24);

                    return View("~/Views/Task/OnSite/Individual/List.cshtml");

                case 2:

                    //ViewBag.Cate1 = zList.Count(s => s.CategoryKey == 21);
                    //ViewBag.Cate2 = zList.Count(s => s.CategoryKey == 22);
                    //ViewBag.Cate3 = zList.Count(s => s.CategoryKey == 24);

                    return View("~/Views/Task/OnSite/TakeBack/List.cshtml");

                case 3:

                    //ViewBag.Cate1 = zList.Count(s => s.CategoryKey == 21);
                    //ViewBag.Cate2 = zList.Count(s => s.CategoryKey == 22);
                    //ViewBag.Cate4 = zList.Count(s => s.CategoryKey == 24);

                    ViewBag.Branch = Branch;
                    ViewBag.Title = new Branch_Info(Branch).Branch.BranchName;
                    return View("~/Views/Task/OnSite/Branch/List.cshtml");
            }
        }

        public IActionResult OnSite_Edit(string TaskKey)
        {
            var ListBranch = Branch_Data.List(UserLog.PartnerNumber);
            ViewBag.ListBranch = ListBranch;

            var ListOption = Helper.ListOption();
            ViewBag.ListOption = ListOption;

            var zInfo = new Task_Info(TaskKey);
            var zTask = zInfo.Task;

            switch (zTask.OptionTask)
            {
                default:
                case 1:
                    return View("~/Views/Task/OnSite/Individual/Edit.cshtml", zTask);
                case 2:
                    return View("~/Views/Task/OnSite/TakeBack/Edit.cshtml", zTask);
                case 3:
                    ViewBag.Branch = zTask.BranchKey;
                    ViewBag.Title = new Branch_Info(zTask.BranchKey.ToString()).Branch.BranchName;
                    return View("~/Views/Task/OnSite/Branch/Edit.cshtml", zTask);
            }

        }

        public IActionResult OnSite_Detail(string TaskKey)
        {
            ViewBag.ListEmployee = Employee_Data.List(UserLog.PartnerNumber);

            TaskKey = TaskKey ?? "";
            var zTask = new Task_Model();
            if (TaskKey != string.Empty)
            {
                var zInfo = new Task_Info(TaskKey);
                zTask = zInfo.Task;
                string btnAction = "";
                switch (zTask.CategoryKey)
                {
                    case 21:
                        btnAction += @"<a id='btnYes' href='#' class='btn-sm btn btn-primary float-right' onclick='Process(1)'><i class='fas fa-check'></i> Nhận máy</a>";
                        btnAction += @"<a id='btnNo' href='#' class='btn-sm btn btn-primary float-right mr-1'><i class='fas fa-stop-circle'></i> Trả máy</a>";
                        break;

                    case 22:
                        if (UserLog.UserKey == "7fa5dc11-e327-4636-a3a1-8af49dc8415a")
                        {
                            btnAction += @"<button id='btnChange' type='button' class='btn-sm btn btn-primary'><i class='fas fa-exchange-alt'></i> Đổi người</button>";
                        }

                        btnAction += @"<button id='btnYes' type='button' class='btn-sm btn btn-primary float-right' onclick='Process(1)'><i class='fas fa-check'></i> Đã sửa xong</button>";
                        btnAction += @"<button id='btnNo' type='button' class='btn-sm btn btn-primary float-right mr-1'><i class='fas fa-stop-circle'></i> Không sửa được</button>";
                        break;

                    case 23:
                        btnAction += @"<button id='btnYes' type='button' class='btn-sm btn btn-primary float-right mr-1' onclick='Process(1)'><i class='fas fa-check'></i> Đã gọi</button>";
                        break;

                    case 24:
                        btnAction += @"<button id='btnYes' type='button' class='btn-sm btn btn-primary float-right' onclick='Process(1)'><i class='fas fa-check'></i> Đã nhận</button>";
                        break;
                }

                ViewBag.Button = btnAction;
            }
            else
            {
                TempData["error"] = "Không tìm thấy thông tin này [" + TaskKey + "], có thể đã bị xóa vui lòng liên hệ quản lý !.";
            }

            switch (zTask.OptionTask)
            {
                default:
                case 1:
                    return View("~/Views/Task/OnSite/Individual/Detail.cshtml", zTask);
                case 2:
                    return View("~/Views/Task/OnSite/TakeBack/Detail.cshtml", zTask);
                case 3:
                    ViewBag.Branch = zTask.BranchKey;
                    ViewBag.Title = new Branch_Info(zTask.BranchKey.ToString()).Branch.BranchName;
                    return View("~/Views/Task/OnSite/Branch/Detail.cshtml", zTask);
            }

        }

        public JsonResult OnSite_Process(string TaskKey, int CategoryKey, int Status, string TaskAmount, string FixNote, string ToDo, string Quote)
        {
            var zResult = new ServerResult();

            Quote = Quote ?? "";
            FixNote = FixNote ?? "";
            ToDo = ToDo ?? "";

            var zIsFixed = false;
            var zInfo = new Task_Info(TaskKey);
            var zModel = zInfo.Task;
            string zSQL = "UPDATE CRM_Task SET ";
            string StatusName = "";
            string CategoryName = "";

            #region [ Process ]
            switch (zModel.OptionTask)
            {
                //máy - khách vãng lai
                default:
                case 1:
                    switch (CategoryKey)
                    {
                        case 21:     //1.Đang báo giá   thực hiện action 1 - Nhận sửa    0 - Không sửa
                            {
                                if (Status == 1)
                                {
                                    StatusName = "Nhận sửa";
                                    CategoryKey = CategoryKey + 1;
                                    CategoryName = Task_Data.TaskCategoryName(UserLog.PartnerNumber, CategoryKey);

                                    zSQL += "Quote = N'" + Quote + "', ";
                                    zSQL += "TaskAmount = " + TaskAmount.ToDouble() + ", ";
                                    zSQL += "StartDate = GETDATE(), ";
                                    zSQL += "StatusKey = " + Status + ", ";
                                    zSQL += "StatusName = N'" + StatusName + "', ";
                                    zSQL += "CategoryKey = " + CategoryKey + ", ";
                                    zSQL += "CategoryName = N'" + CategoryName + "', ";
                                    zSQL += "OwnerBy = N'" + UserLog.UserKey + "', ";
                                    zSQL += "OwnerName = N'" + UserLog.Employee.FullName + "', ";
                                    zSQL += "OwnerKey = '" + UserLog.Employee.EmployeeKey + "', ";
                                    zSQL += "OwnerPhone = '" + UserLog.Employee.MobiPhone + "', ";
                                }
                                else
                                {
                                    StatusName = "Trả máy";
                                    CategoryKey = 24;
                                    CategoryName = Task_Data.TaskCategoryName(UserLog.PartnerNumber, 24);

                                    zSQL += "Note = N'" + FixNote + "', ";
                                    zSQL += "CodeLine= 'N',";
                                    zSQL += "TaskAmount = " + TaskAmount.ToDouble() + ", ";
                                    zSQL += "StartDate = GETDATE(), ";
                                    zSQL += "CompleteDate = GETDATE(), ";
                                    zSQL += "StatusKey = " + Status + ", ";
                                    zSQL += "StatusName = N'" + StatusName + "', ";
                                    zSQL += "CategoryKey = " + CategoryKey + ", ";
                                    zSQL += "CategoryName = N'" + CategoryName + "', ";
                                    zSQL += "OwnerBy = N'" + UserLog.UserKey + "', ";
                                    zSQL += "OwnerName = N'" + UserLog.Employee.FullName + "', ";
                                    zSQL += "OwnerKey = '" + UserLog.Employee.EmployeeKey + "', ";
                                    zSQL += "OwnerPhone = '" + UserLog.Employee.MobiPhone + "', ";
                                }
                            }
                            break;

                        case 22:    //2. Đang sửa		thực hiện action 1 - Đã xong 	0 - Không sửa được
                            {
                                if (Status == 1)
                                {
                                    zIsFixed = true;

                                    StatusName = "Đã xong";
                                    CategoryKey = CategoryKey + 1;
                                    CategoryName = Task_Data.TaskCategoryName(UserLog.PartnerNumber, CategoryKey);

                                    zSQL += "CodeLine= 'Y',";
                                    zSQL += "StatusKey = " + Status + ", ";
                                    zSQL += "StatusName = N'" + StatusName + "', ";
                                    zSQL += "CategoryKey = " + CategoryKey + ", ";
                                    zSQL += "CategoryName = N'" + CategoryName + "', ";
                                    zSQL += "ToDo = N'" + ToDo + "', ";
                                }
                                else
                                {
                                    StatusName = "Không sửa được";
                                    CategoryKey = 24;
                                    CategoryName = Task_Data.TaskCategoryName(UserLog.PartnerNumber, 24);

                                    zSQL += "ToDo = N'" + ToDo + "', ";
                                    zSQL += "Note = N'" + FixNote + "', ";
                                    zSQL += "CodeLine= 'N',";
                                    zSQL += "CompleteDate = GETDATE(), ";
                                    zSQL += "StatusKey = " + Status + ", ";
                                    zSQL += "StatusName = N'" + StatusName + "', ";
                                    zSQL += "CategoryKey = " + CategoryKey + ", ";
                                    zSQL += "CategoryName = N'" + CategoryName + "', ";
                                }
                            }
                            break;

                        case 23:    //3. Đã sửa xong/Chưa gọi		thực hiện action 1 - Đã gọi	0 - Chưa gọi
                            {
                                if (Status == 1)
                                {
                                    StatusName = "Đã gọi";
                                    CategoryKey = CategoryKey + 1;
                                    CategoryName = Task_Data.TaskCategoryName(UserLog.PartnerNumber, CategoryKey);

                                    zSQL += "StatusKey = " + Status + ", ";
                                    zSQL += "StatusName = N'" + StatusName + "', ";
                                    zSQL += "CategoryKey = " + CategoryKey + ", ";
                                    zSQL += "CategoryName = N'" + CategoryName + "', ";
                                }
                            }
                            break;

                        case 24:    //4. Chờ lấy		thực hiện action 1 - Đã nhận	0 - Chưa nhận
                            {
                                if (Status == 1)
                                {
                                    StatusName = "Đã nhận máy";
                                    CategoryKey = CategoryKey + 1;
                                    CategoryName = Task_Data.TaskCategoryName(UserLog.PartnerNumber, CategoryKey);

                                    zSQL += "CompleteDate = GETDATE(), ";
                                    zSQL += "StatusKey = " + Status + ", ";
                                    zSQL += "StatusName = N'" + StatusName + "', ";
                                    zSQL += "CategoryKey = " + CategoryKey + ", ";
                                    zSQL += "CategoryName = N'" + CategoryName + "', ";
                                }
                            }
                            break;
                    }
                    break;

                //máy - mang về
                case 2:
                    switch (CategoryKey)
                    {
                        case 21:     //1.Đang báo giá   thực hiện action 1 - Nhận sửa    0 - Không sửa
                            {
                                if (Status == 1)
                                {
                                    StatusName = "Nhận sửa";
                                    CategoryKey = CategoryKey + 1;
                                    CategoryName = Task_Data.TaskCategoryName(UserLog.PartnerNumber, CategoryKey);

                                    zSQL += "Quote = N'" + Quote + "', ";
                                    zSQL += "TaskAmount = " + TaskAmount.ToDouble() + ", ";
                                    zSQL += "StartDate = GETDATE(), ";
                                    zSQL += "StatusKey = " + Status + ", ";
                                    zSQL += "StatusName = N'" + StatusName + "', ";
                                    zSQL += "CategoryKey = " + CategoryKey + ", ";
                                    zSQL += "CategoryName = N'" + CategoryName + "', ";
                                    zSQL += "OwnerBy = N'" + UserLog.UserKey + "', ";
                                    zSQL += "OwnerName = N'" + UserLog.Employee.FullName + "', ";
                                    zSQL += "OwnerKey = '" + UserLog.Employee.EmployeeKey + "', ";
                                    zSQL += "OwnerPhone = '" + UserLog.Employee.MobiPhone + "', ";
                                }
                                else
                                {
                                    StatusName = "Trả máy";
                                    CategoryKey = 24;
                                    CategoryName = Task_Data.TaskCategoryName(UserLog.PartnerNumber, 24);

                                    zSQL += "Note = N'" + FixNote + "', ";
                                    zSQL += "CodeLine= 'N',";
                                    zSQL += "TaskAmount = " + TaskAmount.ToDouble() + ", ";
                                    zSQL += "StartDate = GETDATE(), ";
                                    zSQL += "CompleteDate = GETDATE(), ";
                                    zSQL += "StatusKey = " + Status + ", ";
                                    zSQL += "StatusName = N'" + StatusName + "', ";
                                    zSQL += "CategoryKey = " + CategoryKey + ", ";
                                    zSQL += "CategoryName = N'" + CategoryName + "', ";
                                    zSQL += "OwnerBy = N'" + UserLog.UserKey + "', ";
                                    zSQL += "OwnerName = N'" + UserLog.Employee.FullName + "', ";
                                    zSQL += "OwnerKey = '" + UserLog.Employee.EmployeeKey + "', ";
                                    zSQL += "OwnerPhone = '" + UserLog.Employee.MobiPhone + "', ";
                                }
                            }
                            break;

                        case 22:    //2. Đang sửa		thực hiện action 1 - Đã xong 	0 - Không sửa được
                            {
                                if (Status == 1)
                                {
                                    zIsFixed = true;

                                    StatusName = "Đã xong";
                                    CategoryKey = 24;
                                    CategoryName = Task_Data.TaskCategoryName(UserLog.PartnerNumber, CategoryKey);

                                    zSQL += "CodeLine= 'Y',";
                                    zSQL += "StatusKey = " + Status + ", ";
                                    zSQL += "StatusName = N'" + StatusName + "', ";
                                    zSQL += "CategoryKey = " + CategoryKey + ", ";
                                    zSQL += "CategoryName = N'" + CategoryName + "', ";
                                    zSQL += "ToDo = N'" + ToDo + "', ";
                                }
                                else
                                {
                                    StatusName = "Không sửa được";
                                    CategoryKey = 24;
                                    CategoryName = Task_Data.TaskCategoryName(UserLog.PartnerNumber, 24);

                                    zSQL += "ToDo = N'" + ToDo + "', ";
                                    zSQL += "Note = N'" + FixNote + "', ";
                                    zSQL += "CodeLine= 'N', ";
                                    zSQL += "CompleteDate = GETDATE(), ";
                                    zSQL += "StatusKey = " + Status + ", ";
                                    zSQL += "StatusName = N'" + StatusName + "', ";
                                    zSQL += "CategoryKey = " + CategoryKey + ", ";
                                    zSQL += "CategoryName = N'" + CategoryName + "', ";
                                }
                            }
                            break;

                        case 24:    //4. Chờ lấy		thực hiện action 1 - Đã nhận	0 - Chưa nhận
                            {
                                if (Status == 1)
                                {
                                    StatusName = "Đã nhận máy";
                                    CategoryKey = CategoryKey + 1;
                                    CategoryName = Task_Data.TaskCategoryName(UserLog.PartnerNumber, CategoryKey);

                                    zSQL += "CompleteDate = GETDATE(), ";
                                    zSQL += "StatusKey = " + Status + ", ";
                                    zSQL += "StatusName = N'" + StatusName + "', ";
                                    zSQL += "CategoryKey = " + CategoryKey + ", ";
                                    zSQL += "CategoryName = N'" + CategoryName + "', ";
                                }
                            }
                            break;
                    }
                    break;

                //máy - chỉ định cửa hàng
                case 3:
                    switch (CategoryKey)
                    {
                        case 21:     //1.Đang báo giá   thực hiện action 1 - Nhận sửa    0 - Không sửa
                            {
                                if (Status == 1)
                                {
                                    StatusName = "Nhận sửa";
                                    CategoryKey = CategoryKey + 1;
                                    CategoryName = Task_Data.TaskCategoryName(UserLog.PartnerNumber, CategoryKey);

                                    zSQL += "Quote = N'" + Quote + "', ";
                                    zSQL += "TaskAmount = " + TaskAmount.ToDouble() + ", ";
                                    zSQL += "StartDate = GETDATE(), ";
                                    zSQL += "StatusKey = " + Status + ", ";
                                    zSQL += "StatusName = N'" + StatusName + "', ";
                                    zSQL += "CategoryKey = " + CategoryKey + ", ";
                                    zSQL += "CategoryName = N'" + CategoryName + "', ";
                                    zSQL += "OwnerBy = N'" + UserLog.UserKey + "', ";
                                    zSQL += "OwnerName = N'" + UserLog.Employee.FullName + "', ";
                                    zSQL += "OwnerKey = '" + UserLog.Employee.EmployeeKey + "', ";
                                    zSQL += "OwnerPhone = '" + UserLog.Employee.MobiPhone + "', ";
                                }
                                else
                                {
                                    StatusName = "Trả máy";
                                    CategoryKey = 24;
                                    CategoryName = Task_Data.TaskCategoryName(UserLog.PartnerNumber, 24);

                                    zSQL += "Note = N'" + FixNote + "', ";
                                    zSQL += "CodeLine= 'N',";
                                    zSQL += "TaskAmount = " + TaskAmount.ToDouble() + ", ";
                                    zSQL += "StartDate = GETDATE(), ";
                                    zSQL += "CompleteDate = GETDATE(), ";
                                    zSQL += "StatusKey = " + Status + ", ";
                                    zSQL += "StatusName = N'" + StatusName + "', ";
                                    zSQL += "CategoryKey = " + CategoryKey + ", ";
                                    zSQL += "CategoryName = N'" + CategoryName + "', ";
                                    zSQL += "OwnerBy = N'" + UserLog.UserKey + "', ";
                                    zSQL += "OwnerName = N'" + UserLog.Employee.FullName + "', ";
                                    zSQL += "OwnerKey = '" + UserLog.Employee.EmployeeKey + "', ";
                                    zSQL += "OwnerPhone = '" + UserLog.Employee.MobiPhone + "', ";
                                }
                            }
                            break;

                        case 22:    //2. Đang sửa		thực hiện action 1 - Đã xong 	0 - Không sửa được
                            {
                                if (Status == 1)
                                {
                                    zIsFixed = true;

                                    StatusName = "Đã xong";
                                    CategoryKey = 24;
                                    CategoryName = Task_Data.TaskCategoryName(UserLog.PartnerNumber, CategoryKey);

                                    zSQL += "CodeLine= 'Y',";
                                    zSQL += "StatusKey = " + Status + ", ";
                                    zSQL += "StatusName = N'" + StatusName + "', ";
                                    zSQL += "CategoryKey = " + CategoryKey + ", ";
                                    zSQL += "CategoryName = N'" + CategoryName + "', ";
                                    zSQL += "ToDo = N'" + ToDo + "', ";
                                }
                                else
                                {
                                    StatusName = "Không sửa được";
                                    CategoryKey = 24;
                                    CategoryName = Task_Data.TaskCategoryName(UserLog.PartnerNumber, 24);

                                    zSQL += "ToDo = N'" + ToDo + "', ";
                                    zSQL += "Note = N'" + FixNote + "', ";
                                    zSQL += "CodeLine= 'N', ";
                                    zSQL += "CompleteDate = GETDATE(), ";
                                    zSQL += "StatusKey = " + Status + ", ";
                                    zSQL += "StatusName = N'" + StatusName + "', ";
                                    zSQL += "CategoryKey = " + CategoryKey + ", ";
                                    zSQL += "CategoryName = N'" + CategoryName + "', ";
                                }
                            }
                            break;

                        case 24:    //4. Chờ lấy		thực hiện action 1 - Đã nhận	0 - Chưa nhận
                            {
                                if (Status == 1)
                                {
                                    StatusName = "Đã nhận máy";
                                    CategoryKey = CategoryKey + 1;
                                    CategoryName = Task_Data.TaskCategoryName(UserLog.PartnerNumber, CategoryKey);

                                    zSQL += "CompleteDate = GETDATE(), ";
                                    zSQL += "StatusKey = " + Status + ", ";
                                    zSQL += "StatusName = N'" + StatusName + "', ";
                                    zSQL += "CategoryKey = " + CategoryKey + ", ";
                                    zSQL += "CategoryName = N'" + CategoryName + "', ";
                                }
                            }
                            break;
                    }
                    break;
            }
            #endregion

            if (!zSQL.Contains(","))
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
                return Json(zResult);
            }

            zSQL = zSQL.Remove(zSQL.LastIndexOf(','), 1);
            zSQL += " WHERE TaskKey = '" + TaskKey + "'";

            zInfo.Process(zSQL);
            if (zInfo.Code != "200" &&
                zInfo.Code != "201")
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
                return Json(zResult);
            }
            else
            {
                #region [Save Log]             
                Save_Log(zModel, Quote + FixNote + ToDo);
                #endregion

                //nếu đang ở tình trạng là đang sửa kết qua sửa được
                if (zIsFixed &&
                    zModel.CategoryKey == 22)
                {
                    Save_Money(zModel);
                }

                //trả về view theo trường hợp
                switch (zModel.OptionTask)
                {
                    default:
                    case 1:
                        zResult.Success = true;
                        zResult.Data = Url.Action("OnSite_List", "Task", new { Option = 1, Category = CategoryKey });
                        return Json(zResult);


                    case 2:
                        zResult.Success = true;
                        zResult.Data = Url.Action("OnSite_List", "Task", new { Option = 2, Category = CategoryKey });
                        return Json(zResult);

                    case 3:
                        zResult.Success = true;
                        zResult.Data = Url.Action("OnSite_List", "Task", new { Option = 3, Branch = zInfo.Task.BranchKey, Category = CategoryKey });
                        return Json(zResult);
                }
            }
        }

        public IActionResult OnSite_Search(int Option, string Branch, int Category, string Phone, string Name, string Content, string FromDate, string ToDate)
        {
            //dùng lưu cái Category cho lần tìm kế tiếp
            if (TempData["Category"] != null)
            {
                //nếu khác thì lưu cái cate mới nhất
                if (TempData["Category"].ToInt() != 0 && 
                    TempData["Category"].ToInt() != Category)
                {
                    TempData["Category"] = Category;
                }
                else
                {
                    //còn không thì tìm lại cate cũ
                    Category = TempData["Category"].ToInt();
                }
            }
            else
            {
                Category = TempData["Category"].ToInt();
            }
            //

            Name = Name ?? "";
            Phone = Phone ?? "";
            Branch = Branch ?? "";

            var zList = new List<Task_Model>();
            if (UserLog.Employee.PositionKey == 54)
            {
                zList = Task_Data.OnSite_Search(UserLog.PartnerNumber, out _, Option, Branch, Category, UserLog.UserKey, Name, Phone, Content, FromDate, ToDate);
            }
            else
            {
                zList = Task_Data.OnSite_Search(UserLog.PartnerNumber, out _, Option, Branch, Category, string.Empty, Name, Phone, Content, FromDate, ToDate);
            }
            
            ViewBag.Data = zList;           
            
            

            switch (Option)
            {
                default:
                case 1:
                    ViewBag.TitleSearch = "Kết quả tìm kiếm";
                    return View("~/Views/Task/OnSite/Individual/List.cshtml");

                case 2:
                    ViewBag.TitleSearch = "Kết quả tìm kiếm";
                    return View("~/Views/Task/OnSite/TakeBack/List.cshtml");

                case 3:
                    ViewBag.TitleSearch = "Kết quả tìm kiếm";
                    ViewBag.Branch = Branch;
                    ViewBag.Title = new Branch_Info(Branch).Branch.BranchName;
                    return View("~/Views/Task/OnSite/Branch/List.cshtml");
            }
        }
    }
}