﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace Mobile
{
    public class Helper
    {
        public static string ConnectionString = "";
        public static string MessageUpload { get; set; } = "";
        public static List<TN_Item> ListOption()
        {
            var List = new List<TN_Item>();
            List.Add(new TN_Item
            {
                Value = "1",
                Text = "Khách vãng lai",
            });

            List.Add(new TN_Item
            {
                Value = "2",
                Text = "Máy mang về",
            });

            List.Add(new TN_Item
            {
                Value = "3",
                Text = "Chỉ định cửa hàng",
            });

            return List;
        }
        public static string ExecuteSQL(string SQL, out string Message)
        {
            string zResult = "";
            string zConnectionString = ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(SQL, zConnect);
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                Message = "200 OK";
            }
            catch (Exception Err)
            {
                Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public static async Task<string> UploadAsync(IFormFile file, string Folder,string PartnerNumber)
        {
            var zResult = "";

            if (file == null)
                return zResult;

            var zWebPath = "_FileUpload/" + PartnerNumber + "/" + Folder + "/";
            try
            {
                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/" + zWebPath);
                var extention = Path.GetExtension(file.FileName);
                var newname = Guid.NewGuid().ToString();
                var fileNameWithPath = string.Concat(filePath, "\\", file.FileName.ToString());

                if (File.Exists(filePath))
                {
                    // If file found, delete it    
                    File.Delete(filePath);
                }
                Directory.CreateDirectory(filePath);

                var stream = new FileStream(fileNameWithPath, FileMode.Create);
                await file.CopyToAsync(stream);

                zResult = "~/" + zWebPath + file.FileName.ToString();
            }
            catch (Exception ex)
            {
                MessageUpload = ex.ToString();
            }

            return zResult;
        }

        public static void DeleteFile(string WebPath)
        {
            WebPath = WebPath.Substring(2);
            var filePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/" + WebPath);

            if (File.Exists(filePath))
            {
                // If file found, delete it    
                File.Delete(filePath);
            }
        }
    }

    public class TN_Item
    {
        public string Value { get; set; } = "";
        public string Text { get; set; } = "";
    }
}
