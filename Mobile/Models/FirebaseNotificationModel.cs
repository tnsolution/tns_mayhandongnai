﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace Mobile.Models
{
    public class FirebaseNotificationModel
    {
        [JsonProperty(PropertyName = "to")]
        public string To { get; set; }

        [JsonProperty(PropertyName = "notification")]
        public NotificationModel Notification { get; set; }
    }

}
