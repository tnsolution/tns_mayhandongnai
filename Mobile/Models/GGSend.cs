﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Mobile
{
    public class GgSend
    {
        public static async void Send(FirebaseNotificationModel firebaseModel)
        {
            HttpRequestMessage httpRequest = null;
            HttpClient httpClient = null;

            var authorizationKey = string.Format("key={0}", "AAAAvk_sckA:APA91bH3a6YldWQ9KCUuRT_3aV48lLwUDbE9E8nfLU_Zuk3Dg_5hvHtmvo2DAFPcZH6IQ36fZD1FxzBckCWqecGtgAFksKBofJe9g-GymM4WprhdhdhIj6YblzwmzfbPzRJxgQewZSqC");
            var jsonBody = JsonConvert.SerializeObject(firebaseModel);

            try
            {
                httpRequest = new HttpRequestMessage(HttpMethod.Post, "https://fcm.googleapis.com/fcm/send");

                httpRequest.Headers.TryAddWithoutValidation("Authorization", authorizationKey);
                httpRequest.Content = new StringContent(jsonBody, Encoding.UTF8, "application/json");

                httpClient = new HttpClient();
                using (await httpClient.SendAsync(httpRequest))
                {

                }
            }
            catch
            {
                throw;
            }
            finally
            {
                httpRequest.Dispose();
                httpClient.Dispose();
            }
        }
    }
    
}
