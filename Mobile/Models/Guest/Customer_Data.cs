﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Mobile
{
    public class Customer_Data
    {
        public static List<Customer_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM CRM_Customer WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Customer_Model> zList = new List<Customer_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Customer_Model()
                {
                    CustomerKey = r["CustomerKey"].ToString(),
                    CustomerID = r["CustomerID"].ToString(),
                    FullName = r["FullName"].ToString(),
                    CompanyName = r["CompanyName"].ToString(),
                    Aliases = r["Aliases"].ToString(),
                    JobTitle = r["JobTitle"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    CustomerType = r["CustomerType"].ToInt(),
                    CustomerTypeName = r["CustomerTypeName"].ToString(),
                    CustomerVendor = r["CustomerVendor"].ToInt(),
                    TaxNumber = r["TaxNumber"].ToString(),
                    IDCard = r["IDCard"].ToString(),
                    Address = r["Address"].ToString(),
                    City = r["City"].ToString(),
                    Country = r["Country"].ToString(),
                    ZipCode = r["ZipCode"].ToString(),
                    Phone = r["Phone"].ToString(),
                    Email = r["Email"].ToString(),
                    Note = r["Note"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    CodeLine = r["CodeLine"].ToString(),
                    BankAccount = r["BankAccount"].ToString(),
                    BankName = r["BankName"].ToString(),
                    Referrer = r["Referrer"].ToString(),
                    Password = r["Password"].ToString(),
                    Parent = r["Parent"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    Contact = r["Contact"].ToString(),
                    OrganizationID = r["OrganizationID"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToInt(),
                    BranchKey = r["BranchKey"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        public static List<Customer_Model> List(string PartnerNumber, int CategoryKey, string BranchKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM CRM_Customer WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (BranchKey != string.Empty)
            {
                zSQL += " AND BranchKey IN (" + BranchKey + ")";
            }
            if (CategoryKey != 0)
            {
                zSQL += " AND CategoryKey = @CategoryKey";
            }

            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Customer_Model> zList = new List<Customer_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Customer_Model()
                {
                    CustomerKey = r["CustomerKey"].ToString(),
                    CustomerID = r["CustomerID"].ToString(),
                    FullName = r["FullName"].ToString(),
                    CompanyName = r["CompanyName"].ToString(),
                    Aliases = r["Aliases"].ToString(),
                    JobTitle = r["JobTitle"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    CustomerType = r["CustomerType"].ToInt(),
                    CustomerTypeName = r["CustomerTypeName"].ToString(),
                    CustomerVendor = r["CustomerVendor"].ToInt(),
                    TaxNumber = r["TaxNumber"].ToString(),
                    IDCard = r["IDCard"].ToString(),
                    Address = r["Address"].ToString(),
                    City = r["City"].ToString(),
                    Country = r["Country"].ToString(),
                    ZipCode = r["ZipCode"].ToString(),
                    Phone = r["Phone"].ToString(),
                    Email = r["Email"].ToString(),
                    Note = r["Note"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    CodeLine = r["CodeLine"].ToString(),
                    BankAccount = r["BankAccount"].ToString(),
                    BankName = r["BankName"].ToString(),
                    Referrer = r["Referrer"].ToString(),
                    Password = r["Password"].ToString(),
                    Parent = r["Parent"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    Contact = r["Contact"].ToString(),
                    OrganizationID = r["OrganizationID"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToInt(),
                    BranchKey = r["BranchKey"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        public static List<Customer_Model> List(string PartnerNumber, int CategoryKey, string BranchKey, string Name, string Phone, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM CRM_Customer WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (BranchKey != string.Empty)
            {
                zSQL += " AND (BranchKey IN (" + BranchKey + ") OR LEN(BranchKey) = 0) ";
            }
            if (CategoryKey != 0)
            {
                zSQL += " AND CategoryKey = @CategoryKey";
            }

            if (Name != string.Empty)
            {
                zSQL += " AND FullName LIKE @Name";
            }

            if (Phone != string.Empty)
            {
                zSQL += " AND Phone = @Phone";
            }

            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = Phone;

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Customer_Model> zList = new List<Customer_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Customer_Model()
                {
                    CustomerKey = r["CustomerKey"].ToString(),
                    CustomerID = r["CustomerID"].ToString(),
                    FullName = r["FullName"].ToString(),
                    CompanyName = r["CompanyName"].ToString(),
                    Aliases = r["Aliases"].ToString(),
                    JobTitle = r["JobTitle"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    CustomerType = r["CustomerType"].ToInt(),
                    CustomerTypeName = r["CustomerTypeName"].ToString(),
                    CustomerVendor = r["CustomerVendor"].ToInt(),
                    TaxNumber = r["TaxNumber"].ToString(),
                    IDCard = r["IDCard"].ToString(),
                    Address = r["Address"].ToString(),
                    City = r["City"].ToString(),
                    Country = r["Country"].ToString(),
                    ZipCode = r["ZipCode"].ToString(),
                    Phone = r["Phone"].ToString(),
                    Email = r["Email"].ToString(),
                    Note = r["Note"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    CodeLine = r["CodeLine"].ToString(),
                    BankAccount = r["BankAccount"].ToString(),
                    BankName = r["BankName"].ToString(),
                    Referrer = r["Referrer"].ToString(),
                    Password = r["Password"].ToString(),
                    Parent = r["Parent"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    Contact = r["Contact"].ToString(),
                    OrganizationID = r["OrganizationID"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToInt(),
                    BranchKey = r["BranchKey"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        public static List<Task_Model> HistoryTask(string PartnerNumber, string CustomerKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM CRM_Task WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND CustomerKey = @CustomerKey";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.NVarChar).Value = CustomerKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Task_Model> zList = new List<Task_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Task_Model()
                {
                    TaskKey = r["TaskKey"].ToString(),
                    TaskFile = r["TaskFile"].ToString(),
                    Subject = r["Subject"].ToString(),
                    TaskContent = r["TaskContent"].ToString(),
                    StartDate = (r["StartDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["StartDate"]),
                    DueDate = (r["DueDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DueDate"]),
                    Duration = r["Duration"].ToFloat(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    PriorityKey = r["PriorityKey"].ToInt(),
                    PriorityName = r["PriorityName"].ToString(),
                    CompleteRate = r["CompleteRate"].ToInt(),
                    CompleteDate = (r["CompleteDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CompleteDate"]),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    GroupKey = r["GroupKey"].ToInt(),
                    GroupName = r["GroupName"].ToString(),
                    ParentKey = r["ParentKey"].ToString(),
                    CustomerKey = r["CustomerKey"].ToString(),
                    CustomerID = r["CustomerID"].ToString(),
                    CustomerName = r["CustomerName"].ToString(),
                    CustomerPhone = r["CustomerPhone"].ToString(),
                    CustomerAddress = r["CustomerAddress"].ToString(),
                    ContractKey = r["ContractKey"].ToString(),
                    ContractName = r["ContractName"].ToString(),
                    Reminder = (r["Reminder"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["Reminder"]),
                    ApproveBy = r["ApproveBy"].ToString(),
                    ApproveName = r["ApproveName"].ToString(),
                    OwnerBy = r["OwnerBy"].ToString(),
                    OwnerName = r["OwnerName"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    CodeLine = r["CodeLine"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToInt(),
                    BranchKey = r["BranchKey"].ToInt(),
                    OrganizationID = r["OrganizationID"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
