﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Mobile
{
    public class Report
    {
        public static List<rpt_ItemSale> Data_BranchGuest(string PartnerNumber, DateTime FromDate, DateTime ToDate, out string Message)
        {
            string zSQL = @"
SELECT 
CAST(A.BranchKey AS NVARCHAR(50)) AS BranchKey, 
A.BranchName, 
ISNULL(dbo.Mayhan_KhachChiNhanh(A.BranchKey, @FromDate, @ToDate),0) AS INCOME
FROM HRM_Branch A
Where A.RecordStatus <> 99 AND A.PartnerNumber = @PartnerNumber
UNION ALL
SELECT '' AS BranchKey, 
N'KHÁCH ĐĂNG KÝ APP' AS BranchName, 
COUNT(CustomerKey) AS INCOME
FROM CRM_Customer 
WHERE 
RecordStatus <> 99 
AND LEN(BranchKey)=0
AND PartnerNumber = @PartnerNumber
AND CreatedOn BETWEEN @FromDate AND @ToDate";
            DataTable zTable = new DataTable();
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                if (FromDate != DateTime.MinValue &&
                    ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<rpt_ItemSale> zList = new List<rpt_ItemSale>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new rpt_ItemSale
                {
                    ItemKey = r["BranchKey"].ToString(),
                    ItemName = r["BranchName"].ToString(),
                    ItemAmount = Convert.ToDouble(r["INCOME"]),
                });
            }

            return zList;
        }
        public static List<rpt_ItemSale> Data_BranchTask(string PartnerNumber, DateTime FromDate, DateTime ToDate, out string Message)
        {
            string zSQL = @"
SELECT 
CAST(A.BranchKey AS NVARCHAR(50)) AS BranchKey, 
A.BranchName, 
ISNULL(dbo.Mayhan_CongViecChiNhanh(A.BranchKey, @FromDate, @ToDate),0) AS INCOME
FROM HRM_Branch A
WHERE A.RecordStatus <> 99 AND A.PartnerNumber = @PartnerNumber
UNION ALL
SELECT '' AS BranchKey, 
N'VIỆC CHƯA NHẬN' AS BranchName, 
COUNT(TASKKEY) AS INCOME
FROM CRM_Task 
WHERE 
RecordStatus <> 99
AND LEN(BranchKey) = 0
AND PartnerNumber = @PartnerNumber
AND CreatedOn BETWEEN @FromDate AND @ToDate
";
            DataTable zTable = new DataTable();
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                if (FromDate != DateTime.MinValue &&
                    ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<rpt_ItemSale> zList = new List<rpt_ItemSale>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new rpt_ItemSale
                {
                    ItemKey = r["BranchKey"].ToString(),
                    ItemName = r["BranchName"].ToString(),
                    ItemAmount = Convert.ToDouble(r["INCOME"]),
                });
            }

            return zList;
        }
        public static List<rpt_ItemSale> Data_BranchSales(string PartnerNumber, DateTime FromDate, DateTime ToDate, out string Message)
        {
            string zSQL = @"
SELECT A.BranchKey, A.BranchName, 
ISNULL(dbo.Mayhan_DoanhSoChiNhanh(A.BranchKey, @FromDate, @ToDate),0) AS INCOME
FROM HRM_Branch A
WHERE A.RecordStatus <> 99 
AND A.PartnerNumber = @PartnerNumber
ORDER BY BranchName";

            DataTable zTable = new DataTable();
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<rpt_ItemSale> zList = new List<rpt_ItemSale>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new rpt_ItemSale
                {
                    ItemKey = r["BranchKey"].ToString(),
                    ItemName = r["BranchName"].ToString(),
                    ItemAmount = Convert.ToDouble(r["INCOME"]),
                });
            }

            return zList;
        }

        public static List<rpt_ItemSale> Sales_Branch(string PartnerNumber, DateTime FromDate, DateTime ToDate, out string Message)
        {
            string zSQL = @"
SELECT A.BranchKey, A.BranchName, ISNULL(dbo.Mayhan_DoanhSoChiNhanh(A.BranchKey, @FromDate, @ToDate),0) AS INCOME
FROM HRM_Branch A
WHERE A.RecordStatus <> 99 
AND A.PartnerNumber = @PartnerNumber
ORDER BY BranchName";

            DataTable zTable = new DataTable();
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<rpt_ItemSale> zList = new List<rpt_ItemSale>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new rpt_ItemSale
                {
                    ItemKey = r["BranchKey"].ToString(),
                    ItemName = r["BranchName"].ToString(),
                    ItemAmount = Convert.ToDouble(r["INCOME"]),
                });
            }

            return zList;
        }
        public static List<rpt_ItemSale> Sales_Staff(string PartnerNumber, DateTime FromDate, DateTime ToDate, out string Message)
        {
            string zSQL = @"
SELECT A.LastName + ' ' + A.FirstName AS FullName, A.EmployeeKey, 
ISNULL(dbo.Mayhan_GetRepairedAmount(A.EmployeeKey, @FromDate, @ToDate),0) AS INCOME,
ISNULL(dbo.Mayhan_SoDauViec(A.EmployeeKey, @FromDate, @ToDate, 19),0) AS TAICH,
ISNULL(dbo.Mayhan_SoDauViec(A.EmployeeKey, @FromDate, @ToDate, 20),0) AS NGOAICH
FROM HRM_Employee A
WHERE 
A.RecordStatus <> 99 
AND A.PartnerNumber = @PartnerNumber
ORDER BY INCOME DESC";

            DataTable zTable = new DataTable();
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<rpt_ItemSale> zList = new List<rpt_ItemSale>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new rpt_ItemSale
                {
                    ItemKey = r["EmployeeKey"].ToString(),
                    ItemName = r["FullName"].ToString(),
                    ItemAmount = Convert.ToDouble(r["INCOME"]),
                    ItemNoTaskInSite = float.Parse(r["TAICH"].ToString()),
                    ItemNoTaskOutSite = float.Parse(r["NGOAICH"].ToString()),
                });
            }

            return zList;
        }
        public static List<rpt_ItemSale> Sales_Staff(string PartnerNumber, string Branch, DateTime FromDate, DateTime ToDate, out string Message)
        {
            string zSQL = @"
SELECT A.LastName + ' ' + A.FirstName AS FullName, A.EmployeeKey, 
ISNULL(dbo.Mayhan_GetRepairedAmount(A.EmployeeKey, @FromDate, @ToDate),0) AS INCOME,
ISNULL(dbo.Mayhan_SoDauViec(A.EmployeeKey, @FromDate, @ToDate, 19),0) AS TAICH,
ISNULL(dbo.Mayhan_SoDauViec(A.EmployeeKey, @FromDate, @ToDate, 20),0) AS NGOAICH
FROM HRM_Employee A
WHERE 
A.RecordStatus <> 99 
AND A.PartnerNumber = @PartnerNumber
AND A.BranchKey IN (" + Branch + ") ORDER BY INCOME DESC";

            DataTable zTable = new DataTable();
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<rpt_ItemSale> zList = new List<rpt_ItemSale>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new rpt_ItemSale
                {
                    ItemKey = r["EmployeeKey"].ToString(),
                    ItemName = r["FullName"].ToString(),
                    ItemAmount = Convert.ToDouble(r["INCOME"]),
                    ItemNoTaskInSite = float.Parse(r["TAICH"].ToString()),
                    ItemNoTaskOutSite = float.Parse(r["NGOAICH"].ToString()),
                });
            }

            return zList;
        }
        public static List<rpt_ItemSale> Sales_Guest(string PartnerNumber, DateTime FromDate, DateTime ToDate, out string Message)
        {
            string zSQL = @"
SELECT A.CustomerKey, A.FullName, 
ISNULL(dbo.Mayhan_GetGuestAmount(A.CustomerKey, @FromDate, @ToDate), 0) AS INCOME,
ISNULL(dbo.Mayhan_SoDauViecGuest(A.CustomerKey, @FromDate, @ToDate, 19),0) AS TAICH,
ISNULL(dbo.Mayhan_SoDauViecGuest(A.CustomerKey, @FromDate, @ToDate, 20),0) AS NGOAICH
FROM CRM_Customer a
WHERE 
RecordStatus <> 99
AND A.PartnerNumber = @PartnerNumber
AND A.CreatedOn BETWEEN @FromDate AND @ToDate";

            DataTable zTable = new DataTable();
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<rpt_ItemSale> zList = new List<rpt_ItemSale>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new rpt_ItemSale
                {
                    ItemKey = r["CustomerKey"].ToString(),
                    ItemName = r["FullName"].ToString(),
                    ItemAmount = Convert.ToDouble(r["INCOME"]),
                    ItemNoTaskInSite = float.Parse(r["TAICH"].ToString()),
                    ItemNoTaskOutSite = float.Parse(r["NGOAICH"].ToString()),
                });
            }
            return zList;
        }

        public static int Total_Task(string PartnerNumber, DateTime FromDate, DateTime ToDate)
        {
            int zResult = 0;
            string zSQL = @"SELECT COUNT(TaskKey) FROM CRM_Task WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (FromDate != DateTime.MinValue &&
                ToDate != DateTime.MinValue)
            {
                zSQL += " AND CreatedOn BETWEEN @FromDate AND @ToDate ";
            }
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                if (FromDate != DateTime.MinValue &&
                    ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }

                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zResult;
        }
        public static double Total_Sale(string PartnerNumber, DateTime FromDate, DateTime ToDate)
        {
            double zResult = 0;
            string zSQL = @"SELECT SUM(AmountCurrencyMain) FROM FNC_Receipt WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (FromDate != DateTime.MinValue &&
                ToDate != DateTime.MinValue)
            {
                zSQL += " AND ReceiptDate BETWEEN @FromDate AND @ToDate ";
            }
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                if (FromDate != DateTime.MinValue &&
                    ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }

                zResult = Convert.ToDouble(zCommand.ExecuteScalar());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zResult;
        }
        public static int Total_Guest(string PartnerNumber, DateTime FromDate, DateTime ToDate)
        {
            int zResult = 0;
            string zSQL = @"SELECT COUNT(CustomerKey) FROM CRM_Customer WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (FromDate != DateTime.MinValue &&
                ToDate != DateTime.MinValue)
            {
                zSQL += " AND CreatedOn BETWEEN @FromDate AND @ToDate ";
            }
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                if (FromDate != DateTime.MinValue &&
                    ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }

                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zResult;
        }

        public static List<Task_Model> Sales_Personal(string PartnerNumber, string OwnerKey, DateTime FromDate, DateTime ToDate, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT
A.TaskKey, A.TaskID, A.TaskContent, A.StatusKey, A.StatusName, A.CategoryKey, A.CategoryName,
A.GroupKey, A.GroupName, A.CustomerName, A.CustomerPhone, A.CustomerAddress,
A.TaskAmount, A.OwnerBy, A.OwnerKey, A.OwnerName, A.CreatedOn,
ISNULL(dbo.Mayhan_GetPaidAmount(A.TaskKey),0) AS Paid
FROM CRM_Task A
WHERE 
A.RecordStatus <> 99
AND A.PartnerNumber = @PartnerNumber
AND A.OwnerKey = @OwnerKey
";
            if (FromDate != DateTime.MinValue &&
               ToDate != DateTime.MinValue)
            {
                zSQL += " AND A.CreatedOn BETWEEN @FromDate AND @ToDate ";
            }
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@OwnerKey", SqlDbType.NVarChar).Value = OwnerKey;
                if (FromDate != DateTime.MinValue &&
                   ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Task_Model> zList = new List<Task_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Task_Model()
                {
                    CreatedOn = Convert.ToDateTime(r["CreatedOn"]),
                    TaskKey = r["TaskKey"].ToString(),
                    TaskID = r["TaskID"].ToString(),
                    TaskContent = r["TaskContent"].ToString(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    GroupKey = r["GroupKey"].ToInt(),
                    GroupName = r["GroupName"].ToString(),
                    CustomerName = r["CustomerName"].ToString(),
                    CustomerPhone = r["CustomerPhone"].ToString(),
                    CustomerAddress = r["CustomerAddress"].ToString(),
                    TaskAmount = Convert.ToDouble(r["TaskAmount"]),
                    PaidAmount = Convert.ToDouble(r["Paid"])
                });
            }
            return zList;
        }
    }
}
