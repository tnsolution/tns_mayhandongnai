﻿namespace Mobile
{
    public class rpt_ItemSale
    {
        public string ItemKey { get; set; } = "";
        public string ItemName { get; set; } = "";
        public double ItemAmount { get; set; } = 0;
        public float ItemNoTaskInSite { get; set; } = 0;
        public float ItemNoTaskOutSite { get; set; } = 0;
    }
}
