﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Mobile
{
    public class Employee_Data
    {
        public static List<Employee_Model> List(string PartnerNumber, string Employee)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, B.[Rank] 
FROM HRM_Employee A 
LEFT JOIN HRM_ListPosition B ON A.PositionKey = B.PositionKey 
LEFT JOIN HRM_Department C ON A.DepartmentKey = C.DepartmentKey
WHERE 
A.RecordStatus != 99 
AND A.PartnerNumber = @PartnerNumber";
            if (Employee.Length >= 36)
            {
                zSQL += " AND A.EmployeeKey = @Employee";
            }
            zSQL += " ORDER BY A.EmployeeID, C.[Rank] ASC, B.[Rank] ASC";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Employee", SqlDbType.NVarChar).Value = Employee;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Employee_Model> zList = new List<Employee_Model>();

            foreach (DataRow r in zTable.Rows)
            {
                DateTime zBirthday = DateTime.MinValue;
                DateTime.TryParse(r["Birthday"].ToString(), out zBirthday);

                DateTime zStartingDate = DateTime.MinValue;
                DateTime.TryParse(r["StartingDate"].ToString(), out zStartingDate);

                zList.Add(new Employee_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    PositionKey = r["PositionKey"].ToInt(),
                    StartingDate = zStartingDate,
                    PositionName = r["PositionName"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                    Email = r["Email"].ToString(),
                    MobiPhone = r["MobiPhone"].ToString(),
                    Birthday = zBirthday,
                    Gender = r["Gender"].ToInt(),
                    AddressRegister = r["AddressRegister"].ToString(),
                    PassportNumber = r["PassportNumber"].ToString(),
                    OrganizationName = r["OrganizationName"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                });
            }

            return zList;
        }
        public static List<Employee_Model> List(string PartnerNumber, string Branch, string Name, string Phone, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, B.[Rank] 
FROM HRM_Employee A 
LEFT JOIN HRM_ListPosition B ON A.PositionKey = B.PositionKey 
LEFT JOIN HRM_Department C ON A.DepartmentKey = C.DepartmentKey
WHERE 
A.RecordStatus != 99 
AND A.PartnerNumber = @PartnerNumber";

            if (Phone != string.Empty)
            {
                zSQL += " AND A.MobiPhone = @Phone";
            }
            if (Name != string.Empty)
            {
                zSQL += " AND (A.LastName LIKE @Name OR A.FirstName LIKE @Name)";
            }
            if (Branch != string.Empty)
            {
                zSQL += " AND A.BranchKey IN (" + Branch + ")";
            }

            zSQL += " ORDER BY A.EmployeeID, C.[Rank] ASC, B.[Rank] ASC";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = Phone;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<Employee_Model> zList = new List<Employee_Model>();

            foreach (DataRow r in zTable.Rows)
            {
                DateTime zBirthday = DateTime.MinValue;
                DateTime.TryParse(r["Birthday"].ToString(), out zBirthday);

                DateTime zStartingDate = DateTime.MinValue;
                DateTime.TryParse(r["StartingDate"].ToString(), out zStartingDate);

                zList.Add(new Employee_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    PositionKey = r["PositionKey"].ToInt(),
                    StartingDate = zStartingDate,
                    BranchKey = r["BranchKey"].ToInt(),
                    BranchName = r["BranchName"].ToString(),
                    PositionName = r["PositionName"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                    Email = r["Email"].ToString(),
                    MobiPhone = r["MobiPhone"].ToString(),
                    Birthday = zBirthday,
                    Gender = r["Gender"].ToInt(),
                    AddressRegister = r["AddressRegister"].ToString(),
                    PassportNumber = r["PassportNumber"].ToString(),
                    OrganizationName = r["OrganizationName"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                });
            }

            return zList;
        }
        public static List<Employee_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, B.[Rank] 
FROM HRM_Employee A 
LEFT JOIN HRM_ListPosition B ON A.PositionKey = B.PositionKey 
LEFT JOIN HRM_Department C ON A.DepartmentKey = C.DepartmentKey
WHERE 
A.RecordStatus != 99 
AND A.PartnerNumber = @PartnerNumber";

            zSQL += " ORDER BY A.EmployeeID, C.[Rank] ASC, B.[Rank] ASC";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Employee_Model> zList = new List<Employee_Model>();

            foreach (DataRow r in zTable.Rows)
            {
                DateTime zBirthday = DateTime.MinValue;
                DateTime.TryParse(r["Birthday"].ToString(), out zBirthday);

                DateTime zStartingDate = DateTime.MinValue;
                DateTime.TryParse(r["StartingDate"].ToString(), out zStartingDate);

                zList.Add(new Employee_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    PositionKey = r["PositionKey"].ToInt(),
                    StartingDate = zStartingDate,
                    PositionName = r["PositionName"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                    Email = r["Email"].ToString(),
                    MobiPhone = r["MobiPhone"].ToString(),
                    Birthday = zBirthday,
                    Gender = r["Gender"].ToInt(),
                    AddressRegister = r["AddressRegister"].ToString(),
                    PassportNumber = r["PassportNumber"].ToString(),
                    OrganizationName = r["OrganizationName"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                });
            }

            return zList;
        }
        public static List<Task_Model> HistoryTask(string PartnerNumber, string Empoyee, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM CRM_Task WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND OwnerBy = dbo.Mayhan_GetUserKey_ByEmployee(@Empoyee)";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Empoyee", SqlDbType.NVarChar).Value = Empoyee;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Task_Model> zList = new List<Task_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Task_Model()
                {
                    TaskKey = r["TaskKey"].ToString(),
                    TaskFile = r["TaskFile"].ToString(),
                    Subject = r["Subject"].ToString(),
                    TaskContent = r["TaskContent"].ToString(),
                    StartDate = (r["StartDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["StartDate"]),
                    DueDate = (r["DueDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DueDate"]),
                    Duration = r["Duration"].ToFloat(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    PriorityKey = r["PriorityKey"].ToInt(),
                    PriorityName = r["PriorityName"].ToString(),
                    CompleteRate = r["CompleteRate"].ToInt(),
                    CompleteDate = (r["CompleteDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CompleteDate"]),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    GroupKey = r["GroupKey"].ToInt(),
                    GroupName = r["GroupName"].ToString(),
                    ParentKey = r["ParentKey"].ToString(),
                    CustomerKey = r["CustomerKey"].ToString(),
                    CustomerID = r["CustomerID"].ToString(),
                    CustomerName = r["CustomerName"].ToString(),
                    CustomerPhone = r["CustomerPhone"].ToString(),
                    CustomerAddress = r["CustomerAddress"].ToString(),
                    ContractKey = r["ContractKey"].ToString(),
                    ContractName = r["ContractName"].ToString(),
                    Reminder = (r["Reminder"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["Reminder"]),
                    ApproveBy = r["ApproveBy"].ToString(),
                    ApproveName = r["ApproveName"].ToString(),
                    OwnerBy = r["OwnerBy"].ToString(),
                    OwnerName = r["OwnerName"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    CodeLine = r["CodeLine"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToInt(),
                    BranchKey = r["BranchKey"].ToInt(),
                    OrganizationID = r["OrganizationID"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static string AutoID(string Prefix, string PartnerNumber)
        {
            string zResult = "";
            string zSQL = @"SELECT dbo.Auto_EmployeeID(@Prefix, @PartnerNumber)";

            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Prefix", SqlDbType.NVarChar).Value = Prefix;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zResult;
        }

        public static Employee_Model GetManager(string PartnerNumber, string Branch, out string Message)
        {
            var Employee = new Employee_Model();

            string zSQL = @"
SELECT A.*
FROM HRM_Employee A 
WHERE A.RecordStatus != 99 
AND A.PartnerNumber = @PartnerNumber
AND A.BranchKey = @BranchKey 
AND A.PositionKey = 55";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Branch;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();

                    #region Employee
                    Employee.DepartmentKey = zReader["DepartmentKey"].ToInt();
                    Employee.DepartmentName = zReader["DepartmentName"].ToString();

                    Employee.EmployeeKey = zReader["EmployeeKey"].ToString();
                    Employee.EmployeeID = zReader["EmployeeID"].ToString();
                    Employee.LastName = zReader["LastName"].ToString();
                    Employee.FirstName = zReader["FirstName"].ToString();
                    Employee.ReportToKey = zReader["ReportToKey"].ToString();
                    Employee.ReportToName = zReader["ReportToName"].ToString();
                    if (zReader["OrganizationKey"] != DBNull.Value)
                    {
                        Employee.OrganizationKey = int.Parse(zReader["OrganizationKey"].ToString());
                    }

                    Employee.OrganizationPath = zReader["OrganizationPath"].ToString();
                    Employee.OrganizationName = zReader["OrganizationName"].ToString();
                    Employee.BranchKey = zReader["BranchKey"].ToInt();
                    Employee.BranchName = zReader["BranchName"].ToString();
                    if (zReader["JobKey"] != DBNull.Value)
                    {
                        Employee.JobKey = int.Parse(zReader["JobKey"].ToString());
                    }

                    Employee.JobName = zReader["JobName"].ToString();
                    if (zReader["PositionKey"] != DBNull.Value)
                    {
                        Employee.PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    }

                    Employee.PositionName = zReader["PositionName"].ToString();
                    if (zReader["WorkingStatusKey"] != DBNull.Value)
                    {
                        Employee.WorkingStatusKey = int.Parse(zReader["WorkingStatusKey"].ToString());
                    }

                    Employee.WorkingStatusName = zReader["WorkingStatusName"].ToString();
                    if (zReader["StartingDate"] != DBNull.Value)
                    {
                        Employee.StartingDate = (DateTime)zReader["StartingDate"];
                    }

                    if (zReader["LeavingDate"] != DBNull.Value)
                    {
                        Employee.LeavingDate = (DateTime)zReader["LeavingDate"];
                    }

                    if (zReader["ContractTypeKey"] != DBNull.Value)
                    {
                        Employee.ContractTypeKey = int.Parse(zReader["ContractTypeKey"].ToString());
                    }

                    Employee.ContractTypeName = zReader["ContractTypeName"].ToString();
                    Employee.WorkingAddress = zReader["WorkingAddress"].ToString();
                    Employee.WorkingLocation = zReader["WorkingLocation"].ToString();
                    Employee.CompanyPhone = zReader["CompanyPhone"].ToString();
                    Employee.CompanyEmail = zReader["CompanyEmail"].ToString();
                    if (zReader["Gender"] != DBNull.Value)
                    {
                        Employee.Gender = int.Parse(zReader["Gender"].ToString());
                    }

                    if (zReader["Birthday"] != DBNull.Value)
                    {
                        Employee.Birthday = (DateTime)zReader["Birthday"];
                    }

                    Employee.BirthPlace = zReader["BirthPlace"].ToString();
                    if (zReader["MaritalStatusKey"] != DBNull.Value)
                    {
                        Employee.MaritalStatusKey = int.Parse(zReader["MaritalStatusKey"].ToString());
                    }

                    Employee.Nationality = zReader["Nationality"].ToString();
                    Employee.Ethnicity = zReader["Ethnicity"].ToString();
                    Employee.PassportNumber = zReader["PassportNumber"].ToString();
                    if (zReader["IssueDate"] != DBNull.Value)
                    {
                        Employee.IssueDate = (DateTime)zReader["IssueDate"];
                    }

                    if (zReader["ExpireDate"] != DBNull.Value)
                    {
                        Employee.ExpireDate = (DateTime)zReader["ExpireDate"];
                    }

                    Employee.IssuePlace = zReader["IssuePlace"].ToString();
                    Employee.AddressRegister = zReader["AddressRegister"].ToString();
                    Employee.CityRegister = zReader["CityRegister"].ToString();
                    Employee.AddressContact = zReader["AddressContact"].ToString();
                    Employee.CityContact = zReader["CityContact"].ToString();
                    Employee.EmergencyContact = zReader["EmergencyContact"].ToString();
                    Employee.MobiPhone = zReader["MobiPhone"].ToString();
                    Employee.Email = zReader["Email"].ToString();

                    Employee.PhotoPath = zReader["PhotoPath"].ToString();
                    Employee.TaxNumber = zReader["TaxNumber"].ToString();
                    Employee.BankAccount = zReader["BankAccount"].ToString();
                    Employee.BankName = zReader["BankName"].ToString();
                    Employee.Note = zReader["Note"].ToString();
                    if (zReader["Slug"] != DBNull.Value)
                    {
                        Employee.Slug = int.Parse(zReader["Slug"].ToString());
                    }

                    Employee.Style = zReader["Style"].ToString();
                    Employee.Class = zReader["Class"].ToString();
                    Employee.CodeLine = zReader["CodeLine"].ToString();
                    Employee.NickName = zReader["NickName"].ToString();
                    Employee.Password = zReader["Password"].ToString();
                    Employee.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Employee.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    Employee.CreatedBy = zReader["CreatedBy"].ToString();
                    Employee.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Employee.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Employee.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Employee.ModifiedName = zReader["ModifiedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Employee.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }
                    #endregion


                    Message = "200 OK";
                }
                else
                {
                    Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }

            return Employee;
        }

        public static List<Employee_Model> ListUser(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT B.UserKey,A.FirstName,A.LastName,A.BranchKey,A.BranchName
FROM HRM_Employee A 
LEFT JOIN  [dbo].[SYS_User] B On B.EmployeeKey =A.EmployeeKey
WHERE
A.RecordStatus != 99 AND B.RecordStatus <> 99
AND A.PartnerNumber = @PartnerNumber";

            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<Employee_Model> zList = new List<Employee_Model>();

            foreach (DataRow r in zTable.Rows)
            {
                

                zList.Add(new Employee_Model()
                {
                    EmployeeKey = r["UserKey"].ToString(),
                    BranchKey = r["BranchKey"].ToInt(),
                    BranchName = r["BranchName"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                });
            }

            return zList;
        }
    }
}