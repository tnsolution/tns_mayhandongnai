﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace Mobile
{
    public partial class Company_Data
    {
        public static List<Company_Model> List(string PartnerNumber, string BranchKey, string Name, string Phone, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SAL_Company WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (BranchKey != string.Empty && BranchKey != "''")
            {
                zSQL += " AND BranchKey IN (" + BranchKey + ")";
            }
            if (Name != string.Empty)
            {
                zSQL += " AND CompanyName LIKE @Name";
            }

            if (Phone != string.Empty)
            {
                zSQL += " AND Phone = @Phone";
            }
            
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = Phone;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Company_Model> zList = new List<Company_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Company_Model()
                {
                    CompanyKey = (r["CompanyKey"] == DBNull.Value ? "" : r["CompanyKey"].ToString()),
                    CompanyID = (r["CompanyID"] == DBNull.Value ? "" : r["CompanyID"].ToString()),
                    CompanyName = (r["CompanyName"] == DBNull.Value ? "" : r["CompanyName"].ToString()),
                    Phone = (r["Phone"] == DBNull.Value ? "" : r["Phone"].ToString()),
                    Address = (r["Address"] == DBNull.Value ? "" : r["Address"].ToString()),
                    WardName = (r["WardName"] == DBNull.Value ? "" : r["WardName"].ToString()),
                    DistrictName = (r["DistrictName"] == DBNull.Value ? "" : r["DistrictName"].ToString()),
                    ProvinceName = (r["ProvinceName"] == DBNull.Value ? "" : r["ProvinceName"].ToString()),
                    Lat = (r["Lat"] == DBNull.Value ? "" : r["Lat"].ToString()),
                    Long = (r["Long"] == DBNull.Value ? "" : r["Long"].ToString()),
                    Description = (r["Description"] == DBNull.Value ? "" : r["Description"].ToString()),
                    Contact = (r["Contact"] == DBNull.Value ? "" : r["Contact"].ToString()),
                    Status = (r["Status"] == DBNull.Value ? 0 : int.Parse(r["Status"].ToString())),
                    StatusName = (r["StatusName"] == DBNull.Value ? "" : r["StatusName"].ToString()),
                    Point = (r["Point"] == DBNull.Value ? 0 : int.Parse(r["Point"].ToString())),
                    BranchKey = (r["BranchKey"] == DBNull.Value ? 0 : int.Parse(r["BranchKey"].ToString())),
                    DepartmentKey = (r["DepartmentKey"] == DBNull.Value ? 0 : int.Parse(r["DepartmentKey"].ToString())),
                    OrganizationID = (r["OrganizationID"] == DBNull.Value ? "" : r["OrganizationID"].ToString()),
                    PartnerNumber = (r["PartnerNumber"] == DBNull.Value ? "" : r["PartnerNumber"].ToString()),
                    RecordStatus = (r["RecordStatus"] == DBNull.Value ? 0 : int.Parse(r["RecordStatus"].ToString())),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = (r["CreatedBy"] == DBNull.Value ? "" : r["CreatedBy"].ToString()),
                    CreatedName = (r["CreatedName"] == DBNull.Value ? "" : r["CreatedName"].ToString()),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = (r["ModifiedBy"] == DBNull.Value ? "" : r["ModifiedBy"].ToString()),
                    ModifiedName = (r["ModifiedName"] == DBNull.Value ? "" : r["ModifiedName"].ToString()),
                });
            }
            return zList;
        }
    }
}
