﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace Mobile
{
    public partial class Company_Info
    {

        public Company_Model Company = new Company_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Company_Info()
        {
        }
        public Company_Info(string CompanyKey)
        {
            string zSQL = "SELECT * FROM SAL_Company WHERE CompanyKey = @CompanyKey AND RecordStatus != 99 ";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CompanyKey", SqlDbType.NVarChar).Value = CompanyKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Company.CompanyKey = zReader["CompanyKey"].ToString();
                    Company.CompanyID = zReader["CompanyID"].ToString();
                    Company.CompanyName = zReader["CompanyName"].ToString();
                    Company.Phone = zReader["Phone"].ToString();
                    Company.Address = zReader["Address"].ToString();
                    Company.WardName = zReader["WardName"].ToString();
                    Company.DistrictName = zReader["DistrictName"].ToString();
                    Company.ProvinceName = zReader["ProvinceName"].ToString();
                    Company.Lat = zReader["Lat"].ToString();
                    Company.Long = zReader["Long"].ToString();
                    Company.Description = zReader["Description"].ToString();
                    Company.Contact = zReader["Contact"].ToString();
                    if (zReader["Status"] != DBNull.Value)
                        Company.Status = int.Parse(zReader["Status"].ToString());
                    Company.StatusName = zReader["StatusName"].ToString();
                    if (zReader["Point"] != DBNull.Value)
                        Company.Point = int.Parse(zReader["Point"].ToString());
                    if (zReader["BranchKey"] != DBNull.Value)
                        Company.BranchKey = int.Parse(zReader["BranchKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        Company.DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    Company.OrganizationID = zReader["OrganizationID"].ToString();
                    Company.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Company.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Company.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Company.CreatedBy = zReader["CreatedBy"].ToString();
                    Company.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Company.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Company.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Company.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public Company_Info(string Phone, string PartnerNumber, bool isPhone)
        {
            string zSQL = "SELECT * FROM SAL_Company WHERE CompanyKey = @CompanyKey AND RecordStatus != 99 ";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = Phone;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Company.CompanyKey = zReader["CompanyKey"].ToString();
                    Company.CompanyID = zReader["CompanyID"].ToString();
                    Company.CompanyName = zReader["CompanyName"].ToString();
                    Company.Phone = zReader["Phone"].ToString();
                    Company.Address = zReader["Address"].ToString();
                    Company.WardName = zReader["WardName"].ToString();
                    Company.DistrictName = zReader["DistrictName"].ToString();
                    Company.ProvinceName = zReader["ProvinceName"].ToString();
                    Company.Lat = zReader["Lat"].ToString();
                    Company.Long = zReader["Long"].ToString();
                    Company.Description = zReader["Description"].ToString();
                    Company.Contact = zReader["Contact"].ToString();
                    if (zReader["Status"] != DBNull.Value)
                        Company.Status = int.Parse(zReader["Status"].ToString());
                    Company.StatusName = zReader["StatusName"].ToString();
                    if (zReader["Point"] != DBNull.Value)
                        Company.Point = int.Parse(zReader["Point"].ToString());
                    if (zReader["BranchKey"] != DBNull.Value)
                        Company.BranchKey = int.Parse(zReader["BranchKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        Company.DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    Company.OrganizationID = zReader["OrganizationID"].ToString();
                    Company.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Company.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Company.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Company.CreatedBy = zReader["CreatedBy"].ToString();
                    Company.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Company.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Company.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Company.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create_KeyAuto()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SAL_Company ("
         + " CompanyID , CompanyName , Phone , Address , WardName , DistrictName , ProvinceName , Lat,Long , Description , Contact , Status , StatusName , Point , BranchKey , DepartmentKey , OrganizationID , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @CompanyID , @CompanyName , @Phone , @Address , @WardName , @DistrictName , @ProvinceName , @Lat, @Long , @Description , @Contact , @Status , @StatusName , @Point , @BranchKey , @DepartmentKey , @OrganizationID , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CompanyID", SqlDbType.NVarChar).Value = Company.CompanyID;
                zCommand.Parameters.Add("@CompanyName", SqlDbType.NVarChar).Value = Company.CompanyName;
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = Company.Phone;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = Company.Address;
                zCommand.Parameters.Add("@WardName", SqlDbType.NVarChar).Value = Company.WardName;
                zCommand.Parameters.Add("@DistrictName", SqlDbType.NVarChar).Value = Company.DistrictName;
                zCommand.Parameters.Add("@ProvinceName", SqlDbType.NVarChar).Value = Company.ProvinceName;
                zCommand.Parameters.Add("@Lat", SqlDbType.NVarChar).Value = Company.Lat;
                zCommand.Parameters.Add("@Long", SqlDbType.NVarChar).Value = Company.Long;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Company.Description;
                zCommand.Parameters.Add("@Contact", SqlDbType.NVarChar).Value = Company.Contact;
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = Company.Status;
                zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = Company.StatusName;
                zCommand.Parameters.Add("@Point", SqlDbType.Int).Value = Company.Point;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = Company.BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Company.DepartmentKey;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Company.OrganizationID;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Company.PartnerNumber;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Company.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Company.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Company.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Company.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Company.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_KeyManual()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SAL_Company("
         + " CompanyKey , CompanyID , CompanyName , Phone , Address , WardName , DistrictName , ProvinceName , Lat, Long , Description , Contact , Status , StatusName , Point , BranchKey , DepartmentKey , OrganizationID , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @CompanyKey , @CompanyID , @CompanyName , @Phone , @Address , @WardName , @DistrictName , @ProvinceName , @Lat, @Long , @Description , @Contact , @Status , @StatusName , @Point , @BranchKey , @DepartmentKey , @OrganizationID , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                Company.CompanyKey = Guid.NewGuid().ToString();
                zCommand.Parameters.Add("@CompanyKey", SqlDbType.NVarChar).Value = Company.CompanyKey;
                zCommand.Parameters.Add("@CompanyID", SqlDbType.NVarChar).Value = Company.CompanyID;
                zCommand.Parameters.Add("@CompanyName", SqlDbType.NVarChar).Value = Company.CompanyName;
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = Company.Phone;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = Company.Address;
                zCommand.Parameters.Add("@WardName", SqlDbType.NVarChar).Value = Company.WardName;
                zCommand.Parameters.Add("@DistrictName", SqlDbType.NVarChar).Value = Company.DistrictName;
                zCommand.Parameters.Add("@ProvinceName", SqlDbType.NVarChar).Value = Company.ProvinceName;
                zCommand.Parameters.Add("@Lat", SqlDbType.NVarChar).Value = Company.Lat;
                zCommand.Parameters.Add("@Long", SqlDbType.NVarChar).Value = Company.Long;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Company.Description;
                zCommand.Parameters.Add("@Contact", SqlDbType.NVarChar).Value = Company.Contact;
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = Company.Status;
                zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = Company.StatusName;
                zCommand.Parameters.Add("@Point", SqlDbType.Int).Value = Company.Point;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = Company.BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Company.DepartmentKey;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Company.OrganizationID;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Company.PartnerNumber;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Company.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Company.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Company.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Company.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Company.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE SAL_Company SET "
                        + " CompanyID = @CompanyID,"
                        + " CompanyName = @CompanyName,"
                        + " Phone = @Phone,"
                        + " Address = @Address,"
                        + " WardName = @WardName,"
                        + " DistrictName = @DistrictName,"
                        + " ProvinceName = @ProvinceName,"
                        + " Lat = @Lat,"
                        + " Long = @Long,"
                        + " Description = @Description,"
                        + " Contact = @Contact,"
                        + " Status = @Status,"
                        + " StatusName = @StatusName,"
                        + " Point = @Point,"
                        + " BranchKey = @BranchKey,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " OrganizationID = @OrganizationID,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE CompanyKey = @CompanyKey";
            string zResult = "";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CompanyKey", SqlDbType.NVarChar).Value = Company.CompanyKey;
                zCommand.Parameters.Add("@CompanyID", SqlDbType.NVarChar).Value = Company.CompanyID;
                zCommand.Parameters.Add("@CompanyName", SqlDbType.NVarChar).Value = Company.CompanyName;
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = Company.Phone;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = Company.Address;
                zCommand.Parameters.Add("@WardName", SqlDbType.NVarChar).Value = Company.WardName;
                zCommand.Parameters.Add("@DistrictName", SqlDbType.NVarChar).Value = Company.DistrictName;
                zCommand.Parameters.Add("@ProvinceName", SqlDbType.NVarChar).Value = Company.ProvinceName;
                zCommand.Parameters.Add("@Lat", SqlDbType.NVarChar).Value = Company.Lat;
                zCommand.Parameters.Add("@Long", SqlDbType.NVarChar).Value = Company.Long;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Company.Description;
                zCommand.Parameters.Add("@Contact", SqlDbType.NVarChar).Value = Company.Contact;
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = Company.Status;
                zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = Company.StatusName;
                zCommand.Parameters.Add("@Point", SqlDbType.Int).Value = Company.Point;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = Company.BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Company.DepartmentKey;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Company.OrganizationID;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Company.PartnerNumber;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Company.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Company.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Company.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SAL_Company SET RecordStatus = 99 WHERE CompanyKey = @CompanyKey";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CompanyKey", SqlDbType.NVarChar).Value = Company.CompanyKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SAL_Company WHERE CompanyKey = @CompanyKey";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CompanyKey", SqlDbType.NVarChar).Value = Company.CompanyKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Delete_AllItem()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE SAL_Company SET RecordStatus = 99 WHERE CompanyKey = @CompanyKey
                            UPDATE SAL_Device SET RecordStatus = 99 WHERE CompanyKey = @CompanyKey AND RecordStatus <> 99
                            UPDATE SAL_Survey SET RecordStatus = 99 WHERE CompanyKey = @CompanyKey AND RecordStatus <> 99
                            UPDATE SAL_Photo SET RecordStatus = 99 WHERE CompanyKey = @CompanyKey AND RecordStatus <> 99
";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CompanyKey", SqlDbType.NVarChar).Value = Company.CompanyKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
