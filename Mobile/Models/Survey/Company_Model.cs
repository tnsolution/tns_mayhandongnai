﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace Mobile
{
    public partial class Company_Model
    {
        #region [ Field Name ]
        private string _CompanyKey = "";
        private string _CompanyID = "";
        private string _CompanyName = "";
        private string _Phone = "";
        private string _Address = "";
        private string _WardName = "";
        private string _DistrictName = "";
        private string _ProvinceName = "";
        private string _Lat = "";
        private string _Long = "";
        private string _Description = "";
        private string _Contact = "";
        private int _Status = 0;
        private string _StatusName = "";
        private int _Point = 0;
        private int _BranchKey = 0;
        private int _DepartmentKey = 0;
        private string _OrganizationID = "";
        private string _PartnerNumber = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public string CompanyKey
        {
            get { return _CompanyKey; }
            set { _CompanyKey = value; }
        }
        public string CompanyID
        {
            get { return _CompanyID; }
            set { _CompanyID = value; }
        }
        public string CompanyName
        {
            get { return _CompanyName; }
            set { _CompanyName = value; }
        }
        public string Phone
        {
            get { return _Phone; }
            set { _Phone = value; }
        }
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }
        public string WardName
        {
            get { return _WardName; }
            set { _WardName = value; }
        }
        public string DistrictName
        {
            get { return _DistrictName; }
            set { _DistrictName = value; }
        }
        public string ProvinceName
        {
            get { return _ProvinceName; }
            set { _ProvinceName = value; }
        }
        public string Lat
        {
            get { return _Lat; }
            set { _Lat = value; }
        }
        public string Long
        {
            get { return _Long; }
            set { _Long = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string Contact
        {
            get { return _Contact; }
            set { _Contact = value; }
        }
        public int Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
        public string StatusName
        {
            get { return _StatusName; }
            set { _StatusName = value; }
        }
        public int Point
        {
            get { return _Point; }
            set { _Point = value; }
        }
        public int BranchKey
        {
            get { return _BranchKey; }
            set { _BranchKey = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public string OrganizationID
        {
            get { return _OrganizationID; }
            set { _OrganizationID = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        #endregion
    }
}
