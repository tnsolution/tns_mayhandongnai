﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace Mobile
{
    public partial class Device_Data
    {
        public static List<Device_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SAL_Device WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Device_Model> zList = new List<Device_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Device_Model()
                {
                    AutoKey = (r["AutoKey"] == DBNull.Value ? 0 : int.Parse(r["AutoKey"].ToString())),
                    CompanyKey = (r["CompanyKey"] == DBNull.Value ? "" : r["CompanyKey"].ToString()),
                    DeviceName = (r["DeviceName"] == DBNull.Value ? "" : r["DeviceName"].ToString()),
                    BrandName = (r["BrandName"] == DBNull.Value ? "" : r["BrandName"].ToString()),
                    Model = (r["Model"] == DBNull.Value ? "" : r["Model"].ToString()),
                    Value = (r["Value"] == DBNull.Value ? 0 : float.Parse(r["Value"].ToString())),
                    Description = (r["Description"] == DBNull.Value ? "" : r["Description"].ToString()),
                    BranchKey = (r["BranchKey"] == DBNull.Value ? 0 : int.Parse(r["BranchKey"].ToString())),
                    DepartmentKey = (r["DepartmentKey"] == DBNull.Value ? 0 : int.Parse(r["DepartmentKey"].ToString())),
                    OrganizationID = (r["OrganizationID"] == DBNull.Value ? "" : r["OrganizationID"].ToString()),
                    PartnerNumber = (r["PartnerNumber"] == DBNull.Value ? "" : r["PartnerNumber"].ToString()),
                    RecordStatus = (r["RecordStatus"] == DBNull.Value ? 0 : int.Parse(r["RecordStatus"].ToString())),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = (r["CreatedBy"] == DBNull.Value ? "" : r["CreatedBy"].ToString()),
                    CreatedName = (r["CreatedName"] == DBNull.Value ? "" : r["CreatedName"].ToString()),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = (r["ModifiedBy"] == DBNull.Value ? "" : r["ModifiedBy"].ToString()),
                    ModifiedName = (r["ModifiedName"] == DBNull.Value ? "" : r["ModifiedName"].ToString()),
                });
            }
            return zList;
        }
        public static List<Device_Model> ListDevice(string PartnerNumber,string CompanyKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SAL_Device WHERE RecordStatus != 99 AND CompanyKey =@CompanyKey AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CompanyKey", SqlDbType.NVarChar).Value = CompanyKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Device_Model> zList = new List<Device_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Device_Model()
                {
                    AutoKey = (r["AutoKey"] == DBNull.Value ? 0 : int.Parse(r["AutoKey"].ToString())),
                    CompanyKey = (r["CompanyKey"] == DBNull.Value ? "" : r["CompanyKey"].ToString()),
                    DeviceName = (r["DeviceName"] == DBNull.Value ? "" : r["DeviceName"].ToString()),
                    BrandName = (r["BrandName"] == DBNull.Value ? "" : r["BrandName"].ToString()),
                    Model = (r["Model"] == DBNull.Value ? "" : r["Model"].ToString()),
                    Value = (r["Value"] == DBNull.Value ? 0 : float.Parse(r["Value"].ToString())),
                    Description = (r["Description"] == DBNull.Value ? "" : r["Description"].ToString()),
                    BranchKey = (r["BranchKey"] == DBNull.Value ? 0 : int.Parse(r["BranchKey"].ToString())),
                    DepartmentKey = (r["DepartmentKey"] == DBNull.Value ? 0 : int.Parse(r["DepartmentKey"].ToString())),
                    OrganizationID = (r["OrganizationID"] == DBNull.Value ? "" : r["OrganizationID"].ToString()),
                    PartnerNumber = (r["PartnerNumber"] == DBNull.Value ? "" : r["PartnerNumber"].ToString()),
                    RecordStatus = (r["RecordStatus"] == DBNull.Value ? 0 : int.Parse(r["RecordStatus"].ToString())),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = (r["CreatedBy"] == DBNull.Value ? "" : r["CreatedBy"].ToString()),
                    CreatedName = (r["CreatedName"] == DBNull.Value ? "" : r["CreatedName"].ToString()),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = (r["ModifiedBy"] == DBNull.Value ? "" : r["ModifiedBy"].ToString()),
                    ModifiedName = (r["ModifiedName"] == DBNull.Value ? "" : r["ModifiedName"].ToString()),
                });
            }
            return zList;
        }
    }
}
