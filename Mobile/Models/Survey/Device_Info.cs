﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace Mobile
{
    public partial class Device_Info
    {

        public Device_Model Device = new Device_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Device_Info()
        {
        }
        public Device_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM SAL_Device WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        Device.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    Device.CompanyKey = zReader["CompanyKey"].ToString();
                    Device.DeviceName = zReader["DeviceName"].ToString();
                    Device.BrandName = zReader["BrandName"].ToString();
                    Device.Model = zReader["Model"].ToString();
                    if (zReader["Value"] != DBNull.Value)
                        Device.Value = float.Parse(zReader["Value"].ToString());
                    Device.Description = zReader["Description"].ToString();
                    if (zReader["BranchKey"] != DBNull.Value)
                        Device.BranchKey = int.Parse(zReader["BranchKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        Device.DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    Device.OrganizationID = zReader["OrganizationID"].ToString();
                    Device.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Device.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Device.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Device.CreatedBy = zReader["CreatedBy"].ToString();
                    Device.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Device.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Device.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Device.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_KeyAuto()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SAL_Device ("
         + " CompanyKey , DeviceName , BrandName , Model , Value , Description , BranchKey , DepartmentKey , OrganizationID , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @CompanyKey , @DeviceName , @BrandName , @Model , @Value , @Description , @BranchKey , @DepartmentKey , @OrganizationID , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CompanyKey", SqlDbType.NVarChar).Value = Device.CompanyKey;
                zCommand.Parameters.Add("@DeviceName", SqlDbType.NVarChar).Value = Device.DeviceName;
                zCommand.Parameters.Add("@BrandName", SqlDbType.NVarChar).Value = Device.BrandName;
                zCommand.Parameters.Add("@Model", SqlDbType.NVarChar).Value = Device.Model;
                zCommand.Parameters.Add("@Value", SqlDbType.Float).Value = Device.Value;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Device.Description;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = Device.BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Device.DepartmentKey;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Device.OrganizationID;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Device.PartnerNumber;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Device.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Device.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Device.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Device.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Device.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_KeyManual()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SAL_Device("
         + " AutoKey , CompanyKey , DeviceName , BrandName , Model , Value , Description , BranchKey , DepartmentKey , OrganizationID , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @CompanyKey , @DeviceName , @BrandName , @Model , @Value , @Description , @BranchKey , @DepartmentKey , @OrganizationID , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Device.AutoKey;
                zCommand.Parameters.Add("@CompanyKey", SqlDbType.NVarChar).Value = Device.CompanyKey;
                zCommand.Parameters.Add("@DeviceName", SqlDbType.NVarChar).Value = Device.DeviceName;
                zCommand.Parameters.Add("@BrandName", SqlDbType.NVarChar).Value = Device.BrandName;
                zCommand.Parameters.Add("@Model", SqlDbType.NVarChar).Value = Device.Model;
                zCommand.Parameters.Add("@Value", SqlDbType.Float).Value = Device.Value;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Device.Description;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = Device.BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Device.DepartmentKey;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Device.OrganizationID;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Device.PartnerNumber;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Device.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Device.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Device.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Device.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Device.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE SAL_Device SET "
                        + " CompanyKey = @CompanyKey,"
                        + " DeviceName = @DeviceName,"
                        + " BrandName = @BrandName,"
                        + " Model = @Model,"
                        + " Value = @Value,"
                        + " Description = @Description,"
                        + " BranchKey = @BranchKey,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " OrganizationID = @OrganizationID,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Device.AutoKey;
                zCommand.Parameters.Add("@CompanyKey", SqlDbType.NVarChar).Value = Device.CompanyKey;
                zCommand.Parameters.Add("@DeviceName", SqlDbType.NVarChar).Value = Device.DeviceName;
                zCommand.Parameters.Add("@BrandName", SqlDbType.NVarChar).Value = Device.BrandName;
                zCommand.Parameters.Add("@Model", SqlDbType.NVarChar).Value = Device.Model;
                zCommand.Parameters.Add("@Value", SqlDbType.Float).Value = Device.Value;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Device.Description;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = Device.BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Device.DepartmentKey;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Device.OrganizationID;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Device.PartnerNumber;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Device.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Device.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Device.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SAL_Device SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Device.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SAL_Device WHERE AutoKey = @AutoKey";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Device.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
