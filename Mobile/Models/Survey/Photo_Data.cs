﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace Mobile
{
    public partial class Photo_Data
    {
        public static List<Photo_Model> List(string PartnerNumber,string CompanyKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SAL_Photo WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND CompanyKey =@CompanyKey ";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CompanyKey", SqlDbType.NVarChar).Value = CompanyKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Photo_Model> zList = new List<Photo_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Photo_Model()
                {
                    AutoKey = (r["AutoKey"] == DBNull.Value ? 0 : int.Parse(r["AutoKey"].ToString())),
                    CompanyKey = (r["CompanyKey"] == DBNull.Value ? "" : r["CompanyKey"].ToString()),
                    PhotoPath = (r["PhotoPath"] == DBNull.Value ? "" : r["PhotoPath"].ToString()),
                    PhotoTitle = (r["PhotoTitle"] == DBNull.Value ? "" : r["PhotoTitle"].ToString()),
                    PhotoDescription = (r["PhotoDescription"] == DBNull.Value ? "" : r["PhotoDescription"].ToString()),
                    Rank = (r["Rank"] == DBNull.Value ? 0 : int.Parse(r["Rank"].ToString())),
                    PartnerNumber = (r["PartnerNumber"] == DBNull.Value ? "" : r["PartnerNumber"].ToString()),
                    RecordStatus = (r["RecordStatus"] == DBNull.Value ? 0 : int.Parse(r["RecordStatus"].ToString())),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = (r["CreatedBy"] == DBNull.Value ? "" : r["CreatedBy"].ToString()),
                    CreatedName = (r["CreatedName"] == DBNull.Value ? "" : r["CreatedName"].ToString()),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = (r["ModifiedBy"] == DBNull.Value ? "" : r["ModifiedBy"].ToString()),
                    ModifiedName = (r["ModifiedName"] == DBNull.Value ? "" : r["ModifiedName"].ToString()),
                });
            }
            return zList;
        }
    }
}
