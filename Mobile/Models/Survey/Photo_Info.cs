﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace Mobile
{
    public partial class Photo_Info
    {

        public Photo_Model Photo = new Photo_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Photo_Info()
        {
        }
        public Photo_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM SAL_Photo WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        Photo.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    Photo.CompanyKey = zReader["CompanyKey"].ToString();
                    Photo.PhotoPath = zReader["PhotoPath"].ToString();
                    Photo.PhotoTitle = zReader["PhotoTitle"].ToString();
                    Photo.PhotoDescription = zReader["PhotoDescription"].ToString();
                    if (zReader["Rank"] != DBNull.Value)
                        Photo.Rank = int.Parse(zReader["Rank"].ToString());
                    Photo.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Photo.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Photo.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Photo.CreatedBy = zReader["CreatedBy"].ToString();
                    Photo.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Photo.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Photo.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Photo.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_KeyAuto()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SAL_Photo ("
         + " CompanyKey , PhotoPath , PhotoTitle , PhotoDescription , Rank , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @CompanyKey , @PhotoPath , @PhotoTitle , @PhotoDescription , @Rank , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CompanyKey", SqlDbType.NVarChar).Value = Photo.CompanyKey;
                zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = Photo.PhotoPath;
                zCommand.Parameters.Add("@PhotoTitle", SqlDbType.NVarChar).Value = Photo.PhotoTitle;
                zCommand.Parameters.Add("@PhotoDescription", SqlDbType.NVarChar).Value = Photo.PhotoDescription;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Photo.Rank;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Photo.PartnerNumber;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Photo.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Photo.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Photo.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Photo.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Photo.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_KeyManual()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SAL_Photo("
         + " AutoKey , CompanyKey , PhotoPath , PhotoTitle , PhotoDescription , Rank , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @CompanyKey , @PhotoPath , @PhotoTitle , @PhotoDescription , @Rank , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Photo.AutoKey;
                zCommand.Parameters.Add("@CompanyKey", SqlDbType.NVarChar).Value = Photo.CompanyKey;
                zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = Photo.PhotoPath;
                zCommand.Parameters.Add("@PhotoTitle", SqlDbType.NVarChar).Value = Photo.PhotoTitle;
                zCommand.Parameters.Add("@PhotoDescription", SqlDbType.NVarChar).Value = Photo.PhotoDescription;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Photo.Rank;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Photo.PartnerNumber;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Photo.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Photo.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Photo.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Photo.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Photo.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE SAL_Photo SET "
                        + " CompanyKey = @CompanyKey,"
                        + " PhotoPath = @PhotoPath,"
                        + " PhotoTitle = @PhotoTitle,"
                        + " PhotoDescription = @PhotoDescription,"
                        + " Rank = @Rank,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Photo.AutoKey;
                zCommand.Parameters.Add("@CompanyKey", SqlDbType.NVarChar).Value = Photo.CompanyKey;
                zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = Photo.PhotoPath;
                zCommand.Parameters.Add("@PhotoTitle", SqlDbType.NVarChar).Value = Photo.PhotoTitle;
                zCommand.Parameters.Add("@PhotoDescription", SqlDbType.NVarChar).Value = Photo.PhotoDescription;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Photo.Rank;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Photo.PartnerNumber;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Photo.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Photo.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Photo.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SAL_Photo SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Photo.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SAL_Photo WHERE AutoKey = @AutoKey";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Photo.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
