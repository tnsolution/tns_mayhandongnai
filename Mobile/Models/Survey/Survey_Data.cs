﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace Mobile
{
    public partial class Survey_Data
    {
        public static List<Survey_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SAL_Survey WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Survey_Model> zList = new List<Survey_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Survey_Model()
                {
                    AutoKey = (r["AutoKey"] == DBNull.Value ? 0 : int.Parse(r["AutoKey"].ToString())),
                    CompanyKey = (r["CompanyKey"] == DBNull.Value ? "" : r["CompanyKey"].ToString()),
                    DateWrite = (r["DateWrite"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DateWrite"]),
                    Description = (r["Description"] == DBNull.Value ? "" : r["Description"].ToString()),
                    BranchKey = (r["BranchKey"] == DBNull.Value ? 0 : int.Parse(r["BranchKey"].ToString())),
                    DepartmentKey = (r["DepartmentKey"] == DBNull.Value ? 0 : int.Parse(r["DepartmentKey"].ToString())),
                    OrganizationID = (r["OrganizationID"] == DBNull.Value ? "" : r["OrganizationID"].ToString()),
                    PartnerNumber = (r["PartnerNumber"] == DBNull.Value ? "" : r["PartnerNumber"].ToString()),
                    RecordStatus = (r["RecordStatus"] == DBNull.Value ? 0 : int.Parse(r["RecordStatus"].ToString())),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = (r["CreatedBy"] == DBNull.Value ? "" : r["CreatedBy"].ToString()),
                    CreatedName = (r["CreatedName"] == DBNull.Value ? "" : r["CreatedName"].ToString()),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = (r["ModifiedBy"] == DBNull.Value ? "" : r["ModifiedBy"].ToString()),
                    ModifiedName = (r["ModifiedName"] == DBNull.Value ? "" : r["ModifiedName"].ToString()),
                });
            }
            return zList;
        }
        public static List<Survey_Model> ListSurvey(string PartnerNumber,string CompanyKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SAL_Survey WHERE RecordStatus != 99 AND CompanyKey =@CompanyKey  AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CompanyKey", SqlDbType.NVarChar).Value = CompanyKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Survey_Model> zList = new List<Survey_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Survey_Model()
                {
                    AutoKey = (r["AutoKey"] == DBNull.Value ? 0 : int.Parse(r["AutoKey"].ToString())),
                    CompanyKey = (r["CompanyKey"] == DBNull.Value ? "" : r["CompanyKey"].ToString()),
                    DateWrite = (r["DateWrite"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DateWrite"]),
                    Description = (r["Description"] == DBNull.Value ? "" : r["Description"].ToString()),
                    BranchKey = (r["BranchKey"] == DBNull.Value ? 0 : int.Parse(r["BranchKey"].ToString())),
                    DepartmentKey = (r["DepartmentKey"] == DBNull.Value ? 0 : int.Parse(r["DepartmentKey"].ToString())),
                    OrganizationID = (r["OrganizationID"] == DBNull.Value ? "" : r["OrganizationID"].ToString()),
                    PartnerNumber = (r["PartnerNumber"] == DBNull.Value ? "" : r["PartnerNumber"].ToString()),
                    RecordStatus = (r["RecordStatus"] == DBNull.Value ? 0 : int.Parse(r["RecordStatus"].ToString())),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = (r["CreatedBy"] == DBNull.Value ? "" : r["CreatedBy"].ToString()),
                    CreatedName = (r["CreatedName"] == DBNull.Value ? "" : r["CreatedName"].ToString()),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = (r["ModifiedBy"] == DBNull.Value ? "" : r["ModifiedBy"].ToString()),
                    ModifiedName = (r["ModifiedName"] == DBNull.Value ? "" : r["ModifiedName"].ToString()),
                });
            }
            return zList;
        }

        public static List<rptSurvey> ListBranch(string PartnerNumber,DateTime FromDate, DateTime ToDate, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT BranchName,COUNT (CompanyKey) AS SOKHACH, SUM(SOMAY) AS SOMAY FROM( --Chỉ lấy số lượng máy lần cuối khảo sát
            SELECT  DISTINCT A.CompanyKey, A.CompanyName, A.Phone,
	        (select sum(value) from[dbo].[SAL_Device] where RecordStatus<> 99 and CompanyKey = A.CompanyKey ) as SOMAY,
	        C.BranchKey,C.BranchName
            FROM[dbo].[SAL_Company] A
            LEFT JOIN[dbo].[SAL_Survey] B ON B.CompanyKey = A.CompanyKey
            LEFT JOIN[dbo].[HRM_Branch] C ON C.BranchKey = B.BranchKey
            WHERE A.RecordStatus <> 99 AND B.RecordStatus <> 99 AND C.RecordStatus <> 99
            AND A.PartnerNumber = @PartnerNumber
            AND B.DateWrite BETWEEN @FromDate AND @ToDate
	)X
    GROUP BY BranchName ";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<rptSurvey> zList = new List<rptSurvey>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new rptSurvey()
                {
                    BranchName = (r["BranchName"] == DBNull.Value ? "" :r["BranchName"].ToString()),
                    Number = (r["SOKHACH"] == DBNull.Value ? 0 : float.Parse(r["SOKHACH"].ToString())),
                    Value = (r["SOMAY"] == DBNull.Value ? 0 : float.Parse(r["SOMAY"].ToString())),
                });
            }
            return zList;
        }
        public static List<rptSurvey> ListCompany(string PartnerNumber, DateTime FromDate, DateTime ToDate,string Staff, int BranchKey, out string Message)
        {

            DataTable zTable = new DataTable();
            string zFilter = "";
            if (Staff != string.Empty)
            {
                zFilter += " AND B.CreatedBy = @CreatedBy ";
            }
            if (BranchKey != 0)
            {
                zFilter += " AND C.BranchKey = @BranchKey ";
            }
            string zSQL = @"
--declare @FromDate Datetime ='2021-10-01 00:00:00'
--declare @ToDate Datetime ='2021-10-30 23:59:59'
SELECT CompanyKey,CompanyName,SUM(SOLAN) AS SOLAN, SUM(SOMAY) AS SOMAY FROM( --Chỉ lấy số lượng máy lần cuối khảo sát
            SELECT  DISTINCT A.CompanyKey, A.CompanyName, A.Phone,
			(select count(CompanyKey) from[dbo].[SAL_Survey] where RecordStatus<> 99 and CompanyKey = A.CompanyKey ) as SOLAN,
	        (select sum(value) from[dbo].[SAL_Device] where RecordStatus<> 99 and CompanyKey = A.CompanyKey ) as SOMAY
            FROM[dbo].[SAL_Company] A
            LEFT JOIN[dbo].[SAL_Survey] B ON B.CompanyKey = A.CompanyKey
            LEFT JOIN[dbo].[HRM_Branch] C ON C.BranchKey = B.BranchKey
            WHERE A.RecordStatus <> 99 AND B.RecordStatus <> 99 AND C.RecordStatus <> 99
            AND A.PartnerNumber = @PartnerNumber
            AND B.DateWrite BETWEEN @FromDate AND @ToDate
            @Filter 
	)X
    GROUP BY CompanyKey,CompanyName
	ORDER BY SOMAY DESC
";
            zSQL = zSQL.Replace("@Filter", zFilter);
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Staff;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<rptSurvey> zList = new List<rptSurvey>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new rptSurvey()
                {
                    CompanyKey = (r["CompanyKey"] == DBNull.Value ? "" : r["CompanyKey"].ToString()),
                    CompanyName = (r["CompanyName"] == DBNull.Value ? "" : r["CompanyName"].ToString()),
                    Number = (r["SOLAN"] == DBNull.Value ? 0 : float.Parse(r["SOLAN"].ToString())),
                    Value = (r["SOMAY"] == DBNull.Value ? 0 : float.Parse(r["SOMAY"].ToString())),
                });
            }
            return zList;
        }
        public static List<rptSurvey> ListDevice(string PartnerNumber, DateTime FromDate, DateTime ToDate, string Staff, int BranchKey, out string Message)
        {
            DataTable zTable = new DataTable();

            string zFilter = "";
            if(Staff!=string.Empty)
            {
                zFilter += " AND C.CreatedBy = @CreatedBy ";
            }    
            if(BranchKey!=0)
            {
                zFilter += " AND D.BranchKey = @BranchKey ";
            }    
            string zSQL = @"
--declare @FromDate Datetime ='2021-10-01 00:00:00'
--declare @ToDate Datetime ='2021-10-30 23:59:59'
--declare @PartnerNumber nvarchar(50) ='99AF7431-AE45-4F47-9D17-4560C10255A8' 

	SELECT DeviceName , Model,Sum(SOMAY) AS SOMAY FROM (
		SELECT DISTINCT A.DeviceName,A.Model, A.Value AS SOMAY FROM SAL_Device A
		LEFT JOIN SAL_Company B ON B.CompanyKey =A.CompanyKey
		LEFT JOIN[dbo].[SAL_Survey] C ON B.CompanyKey = C.CompanyKey
        LEFT JOIN[dbo].[HRM_Branch] D ON D.BranchKey = C.BranchKey --mới
		WHERE A.RecordStatus <> 99 AND B.RecordStatus <> 99 AND C.RecordStatus <> 99
		AND A.PartnerNumber= @PartnerNumber
		AND C.DateWrite BETWEEN @FromDate AND @ToDate
        @Filter 
	)X
GROUP BY DeviceName,Model
ORDER BY SOMAY DESC
";
            zSQL = zSQL.Replace("@Filter", zFilter);
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Staff;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<rptSurvey> zList = new List<rptSurvey>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new rptSurvey()
                {
                    DeviceName = (r["DeviceName"] == DBNull.Value ? "" : r["DeviceName"].ToString()),
                    Model = (r["Model"] == DBNull.Value ? "" : r["Model"].ToString()),
                    Value = (r["SOMAY"] == DBNull.Value ? 0 : float.Parse(r["SOMAY"].ToString())),
                });
            }
            return zList;
        }
        public static List<rptSurvey> ListStaff(string PartnerNumber, DateTime FromDate, DateTime ToDate, string Staff,int BranchKey, out string Message)
        {
            string zFilter = "";
            if (Staff != string.Empty)
            {
                zFilter += " AND B.CreatedBy = @CreatedBy ";
            }
            if (BranchKey != 0)
            {
                zFilter += " AND C.BranchKey = @BranchKey ";
            }
            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate Datetime ='2021-10-01 00:00:00'
--declare @ToDate Datetime ='2021-10-30 23:59:59'
--declare @PartnerNumber nvarchar(50) ='99AF7431-AE45-4F47-9D17-4560C10255A8' 

SELECT  CreatedName ,COUNT (CompanyKey) AS SOKHACH, SUM(SOMAY) AS SOMAY FROM( --Chỉ lấy số lượng máy lần cuối khảo sát
            SELECT  DISTINCT A.CompanyKey, A.CompanyName, A.Phone,
	        (select sum(value) from[dbo].[SAL_Device] where RecordStatus<> 99 and CompanyKey = A.CompanyKey ) as SOMAY,
	        B.CreatedBy,B.CreatedName
            FROM[dbo].[SAL_Company] A
            LEFT JOIN[dbo].[SAL_Survey] B ON B.CompanyKey = A.CompanyKey
            LEFT JOIN[dbo].[HRM_Branch] C ON C.BranchKey = B.BranchKey
            WHERE A.RecordStatus <> 99 AND B.RecordStatus <> 99 AND C.RecordStatus <> 99
            AND A.PartnerNumber = @PartnerNumber
            AND B.DateWrite BETWEEN @FromDate AND @ToDate
            @Filter 
	)X
    GROUP BY CreatedName";
            zSQL = zSQL.Replace("@Filter", zFilter);
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Staff;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<rptSurvey> zList = new List<rptSurvey>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new rptSurvey()
                {
                    BranchName = (r["CreatedName"] == DBNull.Value ? "" : r["CreatedName"].ToString()),
                    Number = (r["SOKHACH"] == DBNull.Value ? 0 : float.Parse(r["SOKHACH"].ToString())),
                    Value = (r["SOMAY"] == DBNull.Value ? 0 : float.Parse(r["SOMAY"].ToString())),
                });
            }
            return zList;
        }
        public static DataTable ListMap(string PartnerNumber, string Name, string Phone,int Branch)
        {
            string zFilter = "";
            if (Name != string.Empty)
            {
                zFilter += " AND A.CompanyName LIKE @Name ";
            }
            if (Phone != string.Empty)
            {
                zFilter += " AND A.Phone LIKE @Phone ";
            }
            if (Branch != 0)
            {
                zFilter += " AND A.BranchKey = @BranchKey ";
            }
            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate Datetime ='2021-11-01 00:00:00'
--declare @ToDate Datetime ='2021-11-30 23:59:59'
--declare @PartnerNumber nvarchar(50) ='99AF7431-AE45-4F47-9D17-4560C10255A8' 
            SELECT  DISTINCT A.CompanyKey, A.CompanyName, A.Phone,A.Lat,A.Long,
	        Status,StatusName,CONCAT(A.Address,', ',A.WardName+', '+A.DistrictName+', '+A.ProvinceName) AS Address
            FROM[dbo].[SAL_Company] A
            WHERE A.RecordStatus <> 99 
            AND A.PartnerNumber = @PartnerNumber
            @Filter

--SELECT CompanyKey,CompanyName,Phone,Status,StatusName,Lat,Long,Address ,SUM(SOMAY) AS SOMAY FROM( --Chỉ lấy số lượng máy lần cuối khảo sát
            --SELECT  DISTINCT A.CompanyKey, A.CompanyName, A.Phone,A.Lat,A.Long,
	        --(select sum(value) from[dbo].[SAL_Device] where RecordStatus<> 99 and CompanyKey = A.CompanyKey ) as SOMAY,
	        --Status,StatusName,CONCAT(A.Address,'-',A.WardName+'-'+A.DistrictName+'-'+A.ProvinceName) AS Address
            --FROM[dbo].[SAL_Company] A
            --LEFT JOIN[dbo].[SAL_Survey] B ON B.CompanyKey = A.CompanyKey
            --LEFT JOIN[dbo].[HRM_Branch] C ON C.BranchKey = B.BranchKey
            --WHERE A.RecordStatus <> 99 AND B.RecordStatus <> 99 AND C.RecordStatus <> 99
            --AND A.PartnerNumber = @PartnerNumber
            --@Filter
            --AND B.DateWrite BETWEEN @FromDate AND @ToDate
	--)X
    --GROUP BY CompanyKey,CompanyName,Phone,Status,StatusName,Lat,Long,Address";
            zSQL = zSQL.Replace("@Filter", zFilter);
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%"+Name+"%";
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = Phone.Trim();
                //zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                //zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                //zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Staff;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = Branch;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); 
                //Message = string.Empty;
            }
            catch (Exception ex)
            {
                //Message = ex.ToString();
            }
            return zTable;
        }
    }
}
