﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace Mobile
{
    public partial class Survey_Info
    {

        public Survey_Model Survey = new Survey_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Survey_Info()
        {
        }
        public Survey_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM SAL_Survey WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        Survey.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    Survey.CompanyKey = zReader["CompanyKey"].ToString();
                    if (zReader["DateWrite"] != DBNull.Value)
                        Survey.DateWrite = (DateTime)zReader["DateWrite"];
                    Survey.Description = zReader["Description"].ToString();
                    if (zReader["BranchKey"] != DBNull.Value)
                        Survey.BranchKey = int.Parse(zReader["BranchKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        Survey.DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    Survey.OrganizationID = zReader["OrganizationID"].ToString();
                    Survey.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Survey.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Survey.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Survey.CreatedBy = zReader["CreatedBy"].ToString();
                    Survey.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Survey.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Survey.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Survey.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_KeyAuto()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SAL_Survey ("
         + " CompanyKey , DateWrite , Description , BranchKey , DepartmentKey , OrganizationID , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @CompanyKey , @DateWrite , @Description , @BranchKey , @DepartmentKey , @OrganizationID , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CompanyKey", SqlDbType.NVarChar).Value = Survey.CompanyKey;
                if (Survey.DateWrite == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = Survey.DateWrite;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Survey.Description;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = Survey.BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Survey.DepartmentKey;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Survey.OrganizationID;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Survey.PartnerNumber;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Survey.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Survey.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Survey.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Survey.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Survey.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_KeyManual()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SAL_Survey("
         + " AutoKey , CompanyKey , DateWrite , Description , BranchKey , DepartmentKey , OrganizationID , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @CompanyKey , @DateWrite , @Description , @BranchKey , @DepartmentKey , @OrganizationID , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Survey.AutoKey;
                zCommand.Parameters.Add("@CompanyKey", SqlDbType.NVarChar).Value = Survey.CompanyKey;
                if (Survey.DateWrite == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = Survey.DateWrite;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Survey.Description;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = Survey.BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Survey.DepartmentKey;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Survey.OrganizationID;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Survey.PartnerNumber;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Survey.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Survey.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Survey.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Survey.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Survey.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE SAL_Survey SET "
                        + " CompanyKey = @CompanyKey,"
                        + " DateWrite = @DateWrite,"
                        + " Description = @Description,"
                        + " BranchKey = @BranchKey,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " OrganizationID = @OrganizationID,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Survey.AutoKey;
                zCommand.Parameters.Add("@CompanyKey", SqlDbType.NVarChar).Value = Survey.CompanyKey;
                if (Survey.DateWrite == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = Survey.DateWrite;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Survey.Description;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = Survey.BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Survey.DepartmentKey;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Survey.OrganizationID;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Survey.PartnerNumber;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Survey.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Survey.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Survey.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SAL_Survey SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Survey.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SAL_Survey WHERE AutoKey = @AutoKey";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Survey.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
