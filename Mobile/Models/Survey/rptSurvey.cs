﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile
{
    public class rptSurvey
    {
        private string _CompanyKey = "";
        private string _CompanyName = "";
        private int _BranchKey = 0;
        private string _BranchName = "";
        private string _DeviceName = "";
        private string _BrandName = "";
        private string _Model = "";
        private string _Phone = "";
        private float _Number = 0;
        private float _Value = 0;

        #region[Propertive]
        public string CompanyKey
        {
            get
            {
                return _CompanyKey;
            }

            set
            {
                _CompanyKey = value;
            }
        }

        public string CompanyName
        {
            get
            {
                return _CompanyName;
            }

            set
            {
                _CompanyName = value;
            }
        }

        public string Phone
        {
            get
            {
                return _Phone;
            }

            set
            {
                _Phone = value;
            }
        }

        public float Value
        {
            get
            {
                return _Value;
            }

            set
            {
                _Value = value;
            }
        }

        public int BranchKey
        {
            get
            {
                return _BranchKey;
            }

            set
            {
                _BranchKey = value;
            }
        }

        public string BranchName
        {
            get
            {
                return _BranchName;
            }

            set
            {
                _BranchName = value;
            }
        }

        public float Number
        {
            get
            {
                return _Number;
            }

            set
            {
                _Number = value;
            }
        }

        public string BrandName
        {
            get
            {
                return _BrandName;
            }

            set
            {
                _BrandName = value;
            }
        }

        public string Model
        {
            get
            {
                return _Model;
            }

            set
            {
                _Model = value;
            }
        }

        public string DeviceName
        {
            get
            {
                return _DeviceName;
            }

            set
            {
                _DeviceName = value;
            }
        }
        #endregion
    }
}
