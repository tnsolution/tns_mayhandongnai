﻿using System;
namespace Mobile
{
    public class Task_Log_Model
    {
        #region [ Field Name ]
        private int _AutoKey = 0;
        private string _TaskKey = "";
        private string _CategoryKey = "";
        private string _CategoryName = "";
        private string _StatusKey = "";
        private string _StatusName = "";
        private string _Description = "";
        private string _JsonData = "";
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string TaskKey
        {
            get { return _TaskKey; }
            set { _TaskKey = value; }
        }
        public string CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string JsonData
        {
            get { return _JsonData; }
            set { _JsonData = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }

        public string Description
        {
            get
            {
                return _Description;
            }

            set
            {
                _Description = value;
            }
        }

        public string StatusKey
        {
            get
            {
                return _StatusKey;
            }

            set
            {
                _StatusKey = value;
            }
        }

        public string StatusName
        {
            get
            {
                return _StatusName;
            }

            set
            {
                _StatusName = value;
            }
        }

        public string CategoryName
        {
            get
            {
                return _CategoryName;
            }

            set
            {
                _CategoryName = value;
            }
        }
        #endregion
    }
}
