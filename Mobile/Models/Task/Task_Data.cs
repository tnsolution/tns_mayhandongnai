﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Mobile
{
    public class Task_Data
    {
        public static List<Task_Model> OnSite_Search(
            string PartnerNumber, out string Message, int Option,
            string Branch, int CategoryKey, string OwnerBy, string Name, string Phone,
            string Content, string FromDate, string ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM CRM_Task WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (CategoryKey != 0)
            {
                zSQL += " AND CategoryKey = @CategoryKey";
            }
            if (Option != 0)
            {
                zSQL += " AND OptionTask = @Option";
            }
            if (Name != string.Empty)
            {
                zSQL += " AND CustomerName LIKE @Name";
            }
            if (Phone != string.Empty)
            {
                zSQL += " AND CustomerPhone = @Phone";
            }
            if (Branch != string.Empty)
            {
                zSQL += " AND BranchKey = @Branch";
            }
            if (OwnerBy != string.Empty)
            {
                zSQL += " AND CAST(OwnerBy AS NVARCHAR(50)) = @OwnerBy";
            }
            if (Content != string.Empty)
            {
                zSQL += " AND TaskContent LIKE @Content";
            }
            if (FromDate != string.Empty &&
                ToDate != string.Empty)
            {

            }

            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                zCommand.Parameters.Add("@OwnerBy", SqlDbType.NVarChar).Value = OwnerBy;
                zCommand.Parameters.Add("@Branch", SqlDbType.NVarChar).Value = Branch;
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = Phone;
                zCommand.Parameters.Add("@Content", SqlDbType.NVarChar).Value = "%" + Content + "%";
                zCommand.Parameters.Add("@Option", SqlDbType.Int).Value = Option;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Task_Model> zList = new List<Task_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Task_Model()
                {
                    CompleteDate = (r["CompleteDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CompleteDate"]),

                    Note = r["Note"].ToString(),
                    ToDo = r["ToDo"].ToString(),
                    Quote = r["Quote"].ToString(),

                    TaskID = r["TaskID"].ToString(),
                    TaskKey = r["TaskKey"].ToString(),

                    Subject = r["Subject"].ToString(),
                    TaskContent = r["TaskContent"].ToString(),

                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    PriorityKey = r["PriorityKey"].ToInt(),

                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    GroupKey = r["GroupKey"].ToInt(),
                    GroupName = r["GroupName"].ToString(),

                    CustomerKey = r["CustomerKey"].ToString(),
                    CustomerID = r["CustomerID"].ToString(),
                    CustomerName = r["CustomerName"].ToString(),
                    CustomerPhone = r["CustomerPhone"].ToString(),
                    CustomerAddress = r["CustomerAddress"].ToString(),

                    ApproveBy = r["ApproveBy"].ToString(),
                    ApproveName = r["ApproveName"].ToString(),
                    OwnerBy = r["OwnerBy"].ToString(),
                    OwnerName = r["OwnerName"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    CodeLine = r["CodeLine"].ToString(),
                    Slug = r["Slug"].ToInt(),

                    Publish = r["Publish"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToInt(),
                    BranchKey = r["BranchKey"].ToInt(),
                    OrganizationID = r["OrganizationID"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static List<Task_Model> OnSite_Search(string PartnerNumber, out string Message, int Option, string Branch, int CategoryKey, string OwnerBy, string Name, string Phone)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM CRM_Task WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (Name != string.Empty)
            {
                zSQL += " AND CustomerName LIKE @Name";
            }

            if (Phone != string.Empty)
            {
                zSQL += " AND CustomerPhone = @Phone";
            }
            if (CategoryKey != 0)
            {
                zSQL += " AND CategoryKey = @CategoryKey";
            }
            if (Option != 0)
            {
                zSQL += " AND OptionTask = @Option";
            }
            if (Branch != string.Empty)
            {
                zSQL += " AND BranchKey = @Branch";
            }
            if (OwnerBy != string.Empty)
            {
                zSQL += " AND CAST(OwnerBy AS NVARCHAR(50)) = @OwnerBy";
            }
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                zCommand.Parameters.Add("@OwnerBy", SqlDbType.NVarChar).Value = OwnerBy;
                zCommand.Parameters.Add("@Branch", SqlDbType.NVarChar).Value = Branch;
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = Phone;
                zCommand.Parameters.Add("@Option", SqlDbType.Int).Value = Option;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Task_Model> zList = new List<Task_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Task_Model()
                {
                    CompleteDate = (r["CompleteDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CompleteDate"]),

                    Note = r["Note"].ToString(),
                    ToDo = r["ToDo"].ToString(),
                    Quote = r["Quote"].ToString(),

                    TaskID = r["TaskID"].ToString(),
                    TaskKey = r["TaskKey"].ToString(),

                    Subject = r["Subject"].ToString(),
                    TaskContent = r["TaskContent"].ToString(),

                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    PriorityKey = r["PriorityKey"].ToInt(),

                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    GroupKey = r["GroupKey"].ToInt(),
                    GroupName = r["GroupName"].ToString(),

                    CustomerKey = r["CustomerKey"].ToString(),
                    CustomerID = r["CustomerID"].ToString(),
                    CustomerName = r["CustomerName"].ToString(),
                    CustomerPhone = r["CustomerPhone"].ToString(),
                    CustomerAddress = r["CustomerAddress"].ToString(),

                    ApproveBy = r["ApproveBy"].ToString(),
                    ApproveName = r["ApproveName"].ToString(),
                    OwnerBy = r["OwnerBy"].ToString(),
                    OwnerName = r["OwnerName"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    CodeLine = r["CodeLine"].ToString(),
                    Slug = r["Slug"].ToInt(),

                    Publish = r["Publish"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToInt(),
                    BranchKey = r["BranchKey"].ToInt(),
                    OrganizationID = r["OrganizationID"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static List<Task_Model> OnSite_List(string PartnerNumber, out string Message, int Option, string Branch, int CategoryKey, string OwnerBy)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM CRM_Task WHERE RecordStatus != 99 AND Publish = 1 AND PartnerNumber = @PartnerNumber ";
            if (CategoryKey != 0)
            {
                zSQL += " AND CategoryKey = @CategoryKey";
            }
            if (Option != 0)
            {
                zSQL += " AND OptionTask = @Option";
            }
            if (Branch != string.Empty)
            {
                zSQL += " AND BranchKey = @Branch";
            }
            if (OwnerBy != string.Empty)
            {
                zSQL += " AND CAST(OwnerBy AS NVARCHAR(50)) = @OwnerBy";
            }
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                zCommand.Parameters.Add("@Branch", SqlDbType.NVarChar).Value = Branch;
                zCommand.Parameters.Add("@OwnerBy", SqlDbType.NVarChar).Value = OwnerBy;
                zCommand.Parameters.Add("@Option", SqlDbType.Int).Value = Option;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Task_Model> zList = new List<Task_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Task_Model()
                {
                    Note = r["Note"].ToString(),
                    ToDo = r["ToDo"].ToString(),
                    Quote = r["Quote"].ToString(),

                    TaskID = r["TaskID"].ToString(),
                    TaskKey = r["TaskKey"].ToString(),

                    Subject = r["Subject"].ToString(),
                    TaskContent = r["TaskContent"].ToString(),

                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    PriorityKey = r["PriorityKey"].ToInt(),

                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    GroupKey = r["GroupKey"].ToInt(),
                    GroupName = r["GroupName"].ToString(),

                    CustomerKey = r["CustomerKey"].ToString(),
                    CustomerID = r["CustomerID"].ToString(),
                    CustomerName = r["CustomerName"].ToString(),
                    CustomerPhone = r["CustomerPhone"].ToString(),
                    CustomerAddress = r["CustomerAddress"].ToString(),

                    ApproveBy = r["ApproveBy"].ToString(),
                    ApproveName = r["ApproveName"].ToString(),
                    OwnerBy = r["OwnerBy"].ToString(),
                    OwnerName = r["OwnerName"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    CodeLine = r["CodeLine"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    CompleteDate = (r["CompleteDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CompleteDate"]),
                    Publish = r["Publish"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToInt(),
                    BranchKey = r["BranchKey"].ToInt(),
                    OrganizationID = r["OrganizationID"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static string TaskCategoryName(string PartnerNumber, int CategoryKey)
        {
            string zResult = "";
            string zSQL = @"SELECT CategoryNameVN FROM CRM_Task_Category WHERE CategoryKey = @CategoryKey AND RecordStatus <> 99";

            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.NVarChar).Value = CategoryKey;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zResult;
        }
        public static string AutoID(string Prefix, string PartnerNumber)
        {
            string zResult = "";
            string zSQL = @"SELECT dbo.Auto_TaskID(@Prefix, @PartnerNumber)";

            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Prefix", SqlDbType.NVarChar).Value = Prefix;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zResult;
        }
        public static List<Task_Model> List(string PartnerNumber, int CategoryKey, string BranchKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM CRM_Task WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND CategoryKey = @CategoryKey";

            if (BranchKey != string.Empty)
            {
                zSQL += " AND (BranchKey IN (" + BranchKey + ") OR BranchKey = '' OR BranchKey IS NULL)";
            }

            //kiem tra nếu việc là ưu tiên và đã qua bước sửa xong thì không hiển thị tại vị trí này
            if (CategoryKey >= 23)
            {
                zSQL += " AND Class <> 'UT'";
            }


            zSQL += " ORDER BY CreatedOn ASC";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Task_Model> zList = new List<Task_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Task_Model()
                {
                    Note = r["Note"].ToString(),
                    ToDo = r["ToDo"].ToString(),
                    Quote = r["Quote"].ToString(),

                    TaskKey = r["TaskKey"].ToString(),
                    TaskID = r["TaskID"].ToString(),
                    TaskFile = r["TaskFile"].ToString(),
                    Subject = r["Subject"].ToString(),
                    TaskContent = r["TaskContent"].ToString(),
                    StartDate = (r["StartDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["StartDate"]),
                    DueDate = (r["DueDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DueDate"]),
                    Duration = r["Duration"].ToFloat(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    PriorityKey = r["PriorityKey"].ToInt(),
                    PriorityName = r["PriorityName"].ToString(),
                    CompleteRate = r["CompleteRate"].ToInt(),
                    CompleteDate = (r["CompleteDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CompleteDate"]),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    GroupKey = r["GroupKey"].ToInt(),
                    GroupName = r["GroupName"].ToString(),
                    ParentKey = r["ParentKey"].ToString(),
                    CustomerKey = r["CustomerKey"].ToString(),
                    CustomerID = r["CustomerID"].ToString(),
                    CustomerName = r["CustomerName"].ToString(),
                    CustomerPhone = r["CustomerPhone"].ToString(),
                    CustomerAddress = r["CustomerAddress"].ToString(),
                    ContractKey = r["ContractKey"].ToString(),
                    ContractName = r["ContractName"].ToString(),
                    Reminder = (r["Reminder"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["Reminder"]),
                    ApproveBy = r["ApproveBy"].ToString(),
                    ApproveName = r["ApproveName"].ToString(),
                    OwnerBy = r["OwnerBy"].ToString(),
                    OwnerName = r["OwnerName"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    CodeLine = r["CodeLine"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToInt(),
                    BranchKey = r["BranchKey"].ToInt(),
                    OrganizationID = r["OrganizationID"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static List<Task_Model> Personal(string PartnerNumber, int CategoryKey, string OwnerBy, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM CRM_Task WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND CategoryKey = @CategoryKey";

            if (OwnerBy != string.Empty)
            {
                zSQL += " AND CAST(OwnerBy AS NVARCHAR(50)) = @OwnerBy";
            }

            zSQL += " ORDER BY CreatedOn ASC";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@OwnerBy", SqlDbType.NVarChar).Value = OwnerBy;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Task_Model> zList = new List<Task_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Task_Model()
                {
                    Note = r["Note"].ToString(),
                    ToDo = r["ToDo"].ToString(),
                    Quote = r["Quote"].ToString(),

                    TaskKey = r["TaskKey"].ToString(),
                    TaskID = r["TaskID"].ToString(),
                    TaskFile = r["TaskFile"].ToString(),
                    Subject = r["Subject"].ToString(),
                    TaskContent = r["TaskContent"].ToString(),
                    StartDate = (r["StartDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["StartDate"]),
                    DueDate = (r["DueDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DueDate"]),
                    Duration = r["Duration"].ToFloat(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    PriorityKey = r["PriorityKey"].ToInt(),
                    PriorityName = r["PriorityName"].ToString(),
                    CompleteRate = r["CompleteRate"].ToInt(),
                    CompleteDate = (r["CompleteDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CompleteDate"]),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    GroupKey = r["GroupKey"].ToInt(),
                    GroupName = r["GroupName"].ToString(),
                    ParentKey = r["ParentKey"].ToString(),
                    CustomerKey = r["CustomerKey"].ToString(),
                    CustomerID = r["CustomerID"].ToString(),
                    CustomerName = r["CustomerName"].ToString(),
                    CustomerPhone = r["CustomerPhone"].ToString(),
                    CustomerAddress = r["CustomerAddress"].ToString(),
                    ContractKey = r["ContractKey"].ToString(),
                    ContractName = r["ContractName"].ToString(),
                    Reminder = (r["Reminder"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["Reminder"]),
                    ApproveBy = r["ApproveBy"].ToString(),
                    ApproveName = r["ApproveName"].ToString(),
                    OwnerBy = r["OwnerBy"].ToString(),
                    OwnerName = r["OwnerName"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    CodeLine = r["CodeLine"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToInt(),
                    BranchKey = r["BranchKey"].ToInt(),
                    OrganizationID = r["OrganizationID"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static List<Task_Model> Search(string PartnerNumber, int CategoryKey, string Name, string Phone, string BranchKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM CRM_Task WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND CategoryKey = @CategoryKey";

            if (BranchKey != string.Empty)
            {
                zSQL += " AND BranchKey IN (" + BranchKey + ")";
            }

            if (Name != string.Empty)
            {
                zSQL += " AND CustomerName LIKE @Name";
            }

            if (Phone != string.Empty)
            {
                zSQL += " AND CustomerPhone = @Phone";
            }

            zSQL += " ORDER BY CreatedOn ASC";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;

                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = Phone;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Task_Model> zList = new List<Task_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Task_Model()
                {
                    TaskKey = r["TaskKey"].ToString(),
                    TaskID = r["TaskID"].ToString(),
                    TaskFile = r["TaskFile"].ToString(),
                    Subject = r["Subject"].ToString(),
                    TaskContent = r["TaskContent"].ToString(),
                    StartDate = (r["StartDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["StartDate"]),
                    DueDate = (r["DueDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DueDate"]),
                    Duration = r["Duration"].ToFloat(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    PriorityKey = r["PriorityKey"].ToInt(),
                    PriorityName = r["PriorityName"].ToString(),
                    CompleteRate = r["CompleteRate"].ToInt(),
                    CompleteDate = (r["CompleteDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CompleteDate"]),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    GroupKey = r["GroupKey"].ToInt(),
                    GroupName = r["GroupName"].ToString(),
                    ParentKey = r["ParentKey"].ToString(),
                    CustomerKey = r["CustomerKey"].ToString(),
                    CustomerID = r["CustomerID"].ToString(),
                    CustomerName = r["CustomerName"].ToString(),
                    CustomerPhone = r["CustomerPhone"].ToString(),
                    CustomerAddress = r["CustomerAddress"].ToString(),
                    ContractKey = r["ContractKey"].ToString(),
                    ContractName = r["ContractName"].ToString(),
                    Reminder = (r["Reminder"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["Reminder"]),
                    ApproveBy = r["ApproveBy"].ToString(),
                    ApproveName = r["ApproveName"].ToString(),
                    OwnerBy = r["OwnerBy"].ToString(),
                    OwnerName = r["OwnerName"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    CodeLine = r["CodeLine"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToInt(),
                    BranchKey = r["BranchKey"].ToInt(),
                    OrganizationID = r["OrganizationID"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static List<Task_Model> Tracking(string PartnerNumber, int Group, string Employee, string GuestName, string GuestPhone, string Category, out string Message)
        {
            string zSQL = @"SELECT 
A.*, ISNULL(dbo.Mayhan_LayTenChiNhanh(A.BranchKey), '') AS BranchName
FROM CRM_Task A
WHERE 
A.RecordStatus <> 99 
AND A.PartnerNumber = @PartnerNumber";

            if (Group != 0)
            {
                zSQL += " AND A.GroupKey = @GroupKey";
            }
            if (Category != string.Empty)
            {
                zSQL += " AND A.CategoryKey IN (" + Category + ")";
            }
            if (Employee != string.Empty)
            {
                zSQL += " AND dbo.Mayhan_GetUserKey_ByEmployee(@Employee)";
            }
            if (GuestName != string.Empty)
            {
                zSQL += " AND A.CustomerName LIKE @GuestName";
            }
            if (GuestPhone != string.Empty)
            {
                zSQL += " AND A.CustomerPhone = @GuestPhone";
            }

            zSQL += " ORDER BY A.CreatedOn DESC";

            DataTable zTable = new DataTable();
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Employee", SqlDbType.NVarChar).Value = Employee;
                zCommand.Parameters.Add("@GuestName", SqlDbType.NVarChar).Value = "%" + GuestName + "%";
                zCommand.Parameters.Add("@GuestPhone", SqlDbType.NVarChar).Value = GuestPhone;
                zCommand.Parameters.Add("@GroupKey", SqlDbType.NVarChar).Value = Group;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<Task_Model> zList = new List<Task_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Task_Model()
                {
                    Note = r["Note"].ToString(),
                    ToDo = r["ToDo"].ToString(),
                    Quote = r["Quote"].ToString(),
                    OptionTask = r["OptionTask"].ToInt(),

                    TaskKey = r["TaskKey"].ToString(),
                    TaskID = r["TaskID"].ToString(),
                    TaskFile = r["TaskFile"].ToString(),
                    Subject = r["Subject"].ToString(),
                    TaskContent = r["TaskContent"].ToString(),
                    StartDate = (r["StartDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["StartDate"]),
                    DueDate = (r["DueDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DueDate"]),
                    Duration = r["Duration"].ToFloat(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    PriorityKey = r["PriorityKey"].ToInt(),
                    PriorityName = r["PriorityName"].ToString(),
                    CompleteRate = r["CompleteRate"].ToInt(),
                    CompleteDate = (r["CompleteDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CompleteDate"]),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    GroupKey = r["GroupKey"].ToInt(),
                    GroupName = r["GroupName"].ToString(),
                    ParentKey = r["ParentKey"].ToString(),
                    CustomerKey = r["CustomerKey"].ToString(),
                    CustomerID = r["CustomerID"].ToString(),
                    CustomerName = r["CustomerName"].ToString(),
                    CustomerPhone = r["CustomerPhone"].ToString(),
                    CustomerAddress = r["CustomerAddress"].ToString(),
                    ContractKey = r["ContractKey"].ToString(),
                    ContractName = r["ContractName"].ToString(),
                    Reminder = (r["Reminder"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["Reminder"]),
                    ApproveBy = r["ApproveBy"].ToString(),
                    ApproveName = r["ApproveName"].ToString(),
                    OwnerBy = r["OwnerBy"].ToString(),
                    OwnerName = r["OwnerName"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    CodeLine = r["CodeLine"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToInt(),
                    BranchKey = r["BranchKey"].ToInt(),
                    BranchName = r["BranchName"].ToString(),
                    OrganizationID = r["OrganizationID"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }

            return zList;
        }
        public static List<Task_Model> Tracking(string PartnerNumber, int Group, string Employee, string GuestName, string GuestPhone, string Category, DateTime FromDate, DateTime ToDate, out string Message)
        {
            string zSQL = @"SELECT 
A.*, ISNULL(dbo.Mayhan_LayTenChiNhanh(A.BranchKey), '') AS BranchName
FROM CRM_Task A
WHERE 
A.RecordStatus <> 99 
AND A.PartnerNumber = @PartnerNumber";

            if (Category != string.Empty)
            {
                zSQL += " AND A.CategoryKey IN (" + Category + ")";
            }
            if (Group != 0)
            {
                zSQL += " AND A.GroupKey = @GroupKey";
            }
            if (Employee != string.Empty)
            {
                zSQL += " AND A.OwnerBy = dbo.Mayhan_GetUserKey_ByEmployee(@Employee) ";
            }
            if (GuestName != string.Empty)
            {
                zSQL += " AND A.CustomerName LIKE @GuestName";
            }
            if (GuestPhone != string.Empty)
            {
                zSQL += " AND A.CustomerPhone = @GuestPhone";
            }
            if (FromDate != DateTime.MinValue &&
                ToDate != DateTime.MinValue)
            {
                zSQL += " AND A.CreatedOn BETWEEN @FromDate AND @ToDate";
            }

            zSQL += " ORDER BY A.CreatedOn DESC";

            DataTable zTable = new DataTable();
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Employee", SqlDbType.NVarChar).Value = Employee;
                zCommand.Parameters.Add("@GuestName", SqlDbType.NVarChar).Value = "%" + GuestName + "%";
                zCommand.Parameters.Add("@GuestPhone", SqlDbType.NVarChar).Value = GuestPhone;
                zCommand.Parameters.Add("@GroupKey", SqlDbType.NVarChar).Value = Group;

                if (FromDate != DateTime.MinValue &&
                    ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<Task_Model> zList = new List<Task_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Task_Model()
                {
                    TaskKey = r["TaskKey"].ToString(),
                    TaskID = r["TaskID"].ToString(),
                    TaskFile = r["TaskFile"].ToString(),
                    Subject = r["Subject"].ToString(),
                    TaskContent = r["TaskContent"].ToString(),
                    StartDate = (r["StartDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["StartDate"]),
                    DueDate = (r["DueDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DueDate"]),
                    Duration = r["Duration"].ToFloat(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    PriorityKey = r["PriorityKey"].ToInt(),
                    PriorityName = r["PriorityName"].ToString(),
                    CompleteRate = r["CompleteRate"].ToInt(),
                    CompleteDate = (r["CompleteDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CompleteDate"]),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    GroupKey = r["GroupKey"].ToInt(),
                    GroupName = r["GroupName"].ToString(),
                    ParentKey = r["ParentKey"].ToString(),
                    CustomerKey = r["CustomerKey"].ToString(),
                    CustomerID = r["CustomerID"].ToString(),
                    CustomerName = r["CustomerName"].ToString(),
                    CustomerPhone = r["CustomerPhone"].ToString(),
                    CustomerAddress = r["CustomerAddress"].ToString(),
                    ContractKey = r["ContractKey"].ToString(),
                    ContractName = r["ContractName"].ToString(),
                    Reminder = (r["Reminder"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["Reminder"]),
                    ApproveBy = r["ApproveBy"].ToString(),
                    ApproveName = r["ApproveName"].ToString(),
                    OwnerBy = r["OwnerBy"].ToString(),
                    OwnerName = r["OwnerName"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    CodeLine = r["CodeLine"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToInt(),
                    BranchKey = r["BranchKey"].ToInt(),
                    BranchName = r["BranchName"].ToString(),
                    OrganizationID = r["OrganizationID"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }

            return zList;
        }
        public static int Count(string PartnerNumber, string Category)
        {
            int zResult = 0;
            string zSQL = @"SELECT COUNT(TaskKey) FROM CRM_Task WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (Category != string.Empty)
            {
                zSQL += " AND CategoryKey IN (" + Category + ")";
            }
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zResult;
        }
        public static List<Task_Log_Model> Log(string TaskKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM CRM_Task_Log WHERE TaskKey = @TaskKey ";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TaskKey", SqlDbType.NVarChar).Value = TaskKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Task_Log_Model> zList = new List<Task_Log_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Task_Log_Model
                {
                    CategoryKey = r["CategoryKey"].ToString(),
                    CategoryName = r["CategoryName"].ToString(),
                    StatusKey = r["StatusKey"].ToString(),
                    StatusName = r["StatusName"].ToString(),
                    Description = r["Description"].ToString(),
                    CreatedOn = Convert.ToDateTime(r["CreatedOn"]),
                });
            }
            return zList;
        }
    }
}