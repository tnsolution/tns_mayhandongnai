﻿using System;
namespace Mobile
{
    public class Task_Model
    {
        #region [ Field Name ]
        private double _PaidAmount = 0;
        private string _TaskKey = "";
        private int _OptionTask = 0;
        private string _OptionName = "";
        private string _TaskID = "";
        private string _TaskFile = "";
        private string _TaskContent = "";
        private string _Subject = "";
        private DateTime _StartDate = DateTime.MinValue;
        private DateTime _DueDate = DateTime.MinValue;
        private float _Duration = 0;
        private int _StatusKey = 0;
        private string _StatusName = "";
        private int _PriorityKey = 0;
        private string _PriorityName = "";
        private int _CompleteRate = 0;
        private DateTime _CompleteDate = DateTime.MinValue;
        private int _CategoryKey = 0;
        private string _CategoryName = "";
        private int _GroupKey = 0;
        private string _GroupName = "";
        private string _ParentKey = "";
        private string _CustomerKey = "";
        private string _CustomerID = "";
        private string _CustomerName = "";
        private string _CustomerPhone = "";
        private string _CustomerAddress = "";
        private string _ContractKey = "";
        private string _ContractName = "";
        private DateTime _Reminder = DateTime.MinValue;
        private string _ApproveBy = "";
        private string _ApproveName = "";
        private string _OwnerBy = "";
        private string _OwnerName = "";
        private string _OwnerKey = "";
        private string _OwnerPhone = "";
        private string _Style = "";
        private string _Class = "";
        private string _CodeLine = "";
        private int _Slug = 0;
        private int _RecordStatus = 0;
        private bool _Publish = true;
        private double _TaskAmount = 0;
        private string _Note = "";
        private string _ToDo = "";
        private string _Quote = "";
        private bool _Paid;
        private string _PartnerNumber = "";
        private int _DepartmentKey = 0;
        private int _BranchKey = 0;
        private string _BranchName = "";
        private string _OrganizationID = "";
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public string TaskKey
        {
            get { return _TaskKey; }
            set { _TaskKey = value; }
        }
        public int OptionTask
        {
            get { return _OptionTask; }
            set { _OptionTask = value; }
        }
        public string OptionName
        {
            get { return _OptionName; }
            set { _OptionName = value; }
        }
        public string TaskID
        {
            get { return _TaskID; }
            set { _TaskID = value; }
        }
        public string TaskFile
        {
            get { return _TaskFile; }
            set { _TaskFile = value; }
        }
        public string TaskContent
        {
            get { return _TaskContent; }
            set { _TaskContent = value; }
        }
        public string Subject
        {
            get { return _Subject; }
            set { _Subject = value; }
        }
        public DateTime StartDate
        {
            get { return _StartDate; }
            set { _StartDate = value; }
        }
        public DateTime DueDate
        {
            get { return _DueDate; }
            set { _DueDate = value; }
        }
        public float Duration
        {
            get { return _Duration; }
            set { _Duration = value; }
        }
        public int StatusKey
        {
            get { return _StatusKey; }
            set { _StatusKey = value; }
        }
        public string StatusName
        {
            get { return _StatusName; }
            set { _StatusName = value; }
        }
        public int PriorityKey
        {
            get { return _PriorityKey; }
            set { _PriorityKey = value; }
        }
        public string PriorityName
        {
            get { return _PriorityName; }
            set { _PriorityName = value; }
        }
        public int CompleteRate
        {
            get { return _CompleteRate; }
            set { _CompleteRate = value; }
        }
        public DateTime CompleteDate
        {
            get { return _CompleteDate; }
            set { _CompleteDate = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string CategoryName
        {
            get { return _CategoryName; }
            set { _CategoryName = value; }
        }
        public int GroupKey
        {
            get { return _GroupKey; }
            set { _GroupKey = value; }
        }
        public string GroupName
        {
            get { return _GroupName; }
            set { _GroupName = value; }
        }
        public string ParentKey
        {
            get { return _ParentKey; }
            set { _ParentKey = value; }
        }
        public string CustomerKey
        {
            get { return _CustomerKey; }
            set { _CustomerKey = value; }
        }
        public string CustomerID
        {
            get { return _CustomerID; }
            set { _CustomerID = value; }
        }
        public string CustomerName
        {
            get { return _CustomerName; }
            set { _CustomerName = value; }
        }
        public string CustomerPhone
        {
            get { return _CustomerPhone; }
            set { _CustomerPhone = value; }
        }
        public string CustomerAddress
        {
            get { return _CustomerAddress; }
            set { _CustomerAddress = value; }
        }
        public string ContractKey
        {
            get { return _ContractKey; }
            set { _ContractKey = value; }
        }
        public string ContractName
        {
            get { return _ContractName; }
            set { _ContractName = value; }
        }
        public DateTime Reminder
        {
            get { return _Reminder; }
            set { _Reminder = value; }
        }
        public string ApproveBy
        {
            get { return _ApproveBy; }
            set { _ApproveBy = value; }
        }
        public string ApproveName
        {
            get { return _ApproveName; }
            set { _ApproveName = value; }
        }
        public string OwnerBy
        {
            get { return _OwnerBy; }
            set { _OwnerBy = value; }
        }
        public string OwnerName
        {
            get { return _OwnerName; }
            set { _OwnerName = value; }
        }
        public string OwnerKey
        {
            get { return _OwnerKey; }
            set { _OwnerKey = value; }
        }
        public string OwnerPhone
        {
            get { return _OwnerPhone; }
            set { _OwnerPhone = value; }
        }
        public string Style
        {
            get { return _Style; }
            set { _Style = value; }
        }
        public string Class
        {
            get { return _Class; }
            set { _Class = value; }
        }
        public string CodeLine
        {
            get { return _CodeLine; }
            set { _CodeLine = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public bool Publish
        {
            get { return _Publish; }
            set { _Publish = value; }
        }
        public double TaskAmount
        {
            get { return _TaskAmount; }
            set { _TaskAmount = value; }
        }
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }
        public string ToDo
        {
            get { return _ToDo; }
            set { _ToDo = value; }
        }
        public string Quote
        {
            get { return _Quote; }
            set { _Quote = value; }
        }
        public bool Paid
        {
            get { return _Paid; }
            set { _Paid = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public int BranchKey
        {
            get { return _BranchKey; }
            set { _BranchKey = value; }
        }
        public string OrganizationID
        {
            get { return _OrganizationID; }
            set { _OrganizationID = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }

        public string BranchName { get => _BranchName; set => _BranchName = value; }
        public double PaidAmount { get => _PaidAmount; set => _PaidAmount = value; }
        #endregion
    }
}
