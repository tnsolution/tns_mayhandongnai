#pragma checksum "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "64db37ba422dcc2e2c5fba0df91603bbdef70a25"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Guest_List), @"mvc.1.0.view", @"/Views/Guest/List.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Guest/List.cshtml", typeof(AspNetCore.Views_Guest_List))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Projects\_SLN2021_MayHan\Mobile\Views\_ViewImports.cshtml"
using Mobile;

#line default
#line hidden
#line 2 "D:\Projects\_SLN2021_MayHan\Mobile\Views\_ViewImports.cshtml"
using Mobile.Models;

#line default
#line hidden
#line 1 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#line 2 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
using Newtonsoft.Json;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"64db37ba422dcc2e2c5fba0df91603bbdef70a25", @"/Views/Guest/List.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"dfef6c2d78d7165afbb98ab2922cb7b9a487eb76", @"/Views/_ViewImports.cshtml")]
    public class Views_Guest_List : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Home", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("card"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Guest", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Search", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-primary btn-add"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_8 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 3 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
  
    ViewData["Title"] = "List";
    Layout = "~/Views/Guest/_Layout.cshtml";

    var _ListData = new List<Customer_Model>();
    if (ViewBag.ListData != null)
    {
        _ListData = ViewBag.ListData as List<Customer_Model>;
    }
    var _ListBranch = new List<Branch_Model>();
    if (ViewBag.ListBranch != null)
    {
        _ListBranch = ViewBag.ListBranch as List<Branch_Model>;
    }

    var session = Context.Session.GetString("UserLogged");
    var UserLog = JsonConvert.DeserializeObject<User_Model>(session);

#line default
#line hidden
            BeginContext(605, 343, true);
            WriteLiteral(@"
<header class=""header"" style=""height:60px; position:fixed"">
    <div class=""task-header"">
        <table class=""table table-sm mb-0""
               style=""
    border-bottom: 1px solid #2196f3;
    color: gray;"">
            <tr>
                <td class=""text-center"" style=""vertical-align: middle; width:15%"">
                    ");
            EndContext();
            BeginContext(948, 135, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d2a5ac29ed2a48268f4288514733eb8b", async() => {
                BeginContext(992, 87, true);
                WriteLiteral("\r\n                        <i class=\"fas fa-2x fa-angle-left\"></i>\r\n                    ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1083, 742, true);
            WriteLiteral(@"
                </td>
                <td class=""text-center"" style=""vertical-align: middle;"">
                    <h4>Danh sách khách hàng</h4>
                </td>
                <td class=""text-center"" style=""vertical-align: middle; width:15%"">
                    <a href=""#"" data-toggle-class=""sidebar-left-opened"" data-target=""html"" data-fire-event=""sidebar-left-opened"">
                        <i class=""fas fa-2x fa fa-search""></i>
                    </a>
                </td>
            </tr>
        </table>

    </div>
</header>
<div class=""inner-wrapper"" style=""padding-top:60px"">
    <section role=""main"" class=""container-fluid p-0"">
        <table class=""table table-sm bg-white"">
            <tbody>
");
            EndContext();
#line 51 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
                  
                    if (_ListData.Count > 0)
                    {
                        foreach (var item in _ListData)
                        {

#line default
#line hidden
            BeginContext(1998, 69, true);
            WriteLiteral("                            <tr>\r\n                                <td");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 2067, "\"", 2102, 3);
            WriteAttributeValue("", 2077, "Open(\'", 2077, 6, true);
#line 57 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
WriteAttributeValue("", 2083, item.CustomerKey, 2083, 17, false);

#line default
#line hidden
            WriteAttributeValue("", 2100, "\')", 2100, 2, true);
            EndWriteAttribute();
            BeginContext(2103, 87, true);
            WriteLiteral(">\r\n                                    <h4 class=\"m-0 font-weight-bolder text-primary\">");
            EndContext();
            BeginContext(2191, 23, false);
#line 58 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
                                                                               Write(item.FullName.ToUpper());

#line default
#line hidden
            EndContext();
            BeginContext(2214, 114, true);
            WriteLiteral("</h4>\r\n                                    <div class=\"text-muted\">\r\n                                        Sđt: ");
            EndContext();
            BeginContext(2329, 10, false);
#line 60 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
                                        Write(item.Phone);

#line default
#line hidden
            EndContext();
            BeginContext(2339, 56, true);
            WriteLiteral("\r\n                                        <a id=\"btnSms\"");
            EndContext();
            BeginWriteAttribute("phonenumber", " phonenumber=\"", 2395, "\"", 2420, 1);
#line 61 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
WriteAttributeValue("", 2409, item.Phone, 2409, 11, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginWriteAttribute("href", " href=\"", 2421, "\"", 2443, 2);
            WriteAttributeValue("", 2428, "sms:", 2428, 4, true);
#line 61 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
WriteAttributeValue("", 2432, item.Phone, 2432, 11, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2444, 124, true);
            WriteLiteral(" class=\"font-weight-bold text-info float-right d-none\">Nhắn tin</a>\r\n                                        <a id=\"btnCall\"");
            EndContext();
            BeginWriteAttribute("phonenumber", " phonenumber=\"", 2568, "\"", 2593, 1);
#line 62 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
WriteAttributeValue("", 2582, item.Phone, 2582, 11, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginWriteAttribute("href", " href=\"", 2594, "\"", 2616, 2);
            WriteAttributeValue("", 2601, "tel:", 2601, 4, true);
#line 62 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
WriteAttributeValue("", 2605, item.Phone, 2605, 11, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2617, 185, true);
            WriteLiteral(" class=\"font-weight-bold text-info float-right d-none mr-1\">Gọi | </a>\r\n                                    </div>\r\n                                    <div class=\"text-muted\">Địa chỉ: ");
            EndContext();
            BeginContext(2803, 12, false);
#line 64 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
                                                                Write(item.Address);

#line default
#line hidden
            EndContext();
            BeginContext(2815, 8, true);
            WriteLiteral("</div>\r\n");
            EndContext();
#line 65 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
                                     if (item.CategoryKey == 9)
                                    {

#line default
#line hidden
            BeginContext(2927, 86, true);
            WriteLiteral("                                        <span class=\"text-muted\">Đã giao dịch</span>\r\n");
            EndContext();
#line 68 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
                                    }
                                    else
                                    {

#line default
#line hidden
            BeginContext(3133, 88, true);
            WriteLiteral("                                        <span class=\"text-muted\">Khách vãng lai</span>\r\n");
            EndContext();
#line 72 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
                                    }

#line default
#line hidden
            BeginContext(3260, 36, true);
            WriteLiteral("                                    ");
            EndContext();
#line 73 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
                                     if (item.Referrer.Length > 0)
                                    {

#line default
#line hidden
            BeginContext(3367, 90, true);
            WriteLiteral("                                        <span class=\"text-warning\">Có sử dụng app</span>\r\n");
            EndContext();
#line 76 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
                                    }

#line default
#line hidden
            BeginContext(3496, 74, true);
            WriteLiteral("                                </td>\r\n                            </tr>\r\n");
            EndContext();
#line 79 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
                        }
                    }
                    else
                    {

#line default
#line hidden
            BeginContext(3669, 340, true);
            WriteLiteral(@"                        <tr>
                            <td>
                                <div class=""alert alert-info"">
                                    <strong>Chưa có phát sinh, hoặc không tìm thấy thông tin !.</strong>
                                </div>
                            </td>
                        </tr>
");
            EndContext();
#line 90 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
                    }

                

#line default
#line hidden
            BeginContext(4053, 116, true);
            WriteLiteral("            </tbody>\r\n        </table>\r\n    </section>\r\n</div>\r\n<aside id=\"sidebar-left\" class=\"sidebar-left\">\r\n    ");
            EndContext();
            BeginContext(4169, 1324, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "59cefa22a4e54fc4b7e8e8c782f7678c", async() => {
                BeginContext(4245, 702, true);
                WriteLiteral(@"
        <div class=""card-header"">
            <h4 class=""text-center m-0"">Tìm kiếm thông tin</h4>
        </div>
        <div class=""card-body"">
            <div class=""form-label-group"">
                <input type=""text"" class=""form-control"" id=""txt_SearchName"" name=""Name"" placeholder="""">
                <label for=""txt_SearchName"">Tên khách hàng</label>
            </div>
            <div class=""form-label-group"">
                <input type=""number"" class=""form-control"" id=""txt_SearchPhone"" name=""Phone"" placeholder="""">
                <label for=""txt_SearchPhone"">Số điện thoại</label>
            </div>
            <select class=""form-control"" name=""Branch"">
                ");
                EndContext();
                BeginContext(4947, 32, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "bd9a6731e5c944ed82a3323a376a59ab", async() => {
                    BeginContext(4964, 6, true);
                    WriteLiteral("--//--");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_2.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4979, 2, true);
                WriteLiteral("\r\n");
                EndContext();
#line 113 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
                 foreach (var item in _ListBranch)
                {

#line default
#line hidden
                BeginContext(5052, 20, true);
                WriteLiteral("                    ");
                EndContext();
                BeginContext(5072, 57, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "2ef03d7cd558479d897b556d9be0f3b4", async() => {
                    BeginContext(5105, 15, false);
#line 115 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
                                               Write(item.BranchName);

#line default
#line hidden
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                BeginWriteTagHelperAttribute();
#line 115 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
                       WriteLiteral(item.BranchKey);

#line default
#line hidden
                __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = __tagHelperStringValueBuffer;
                __tagHelperExecutionContext.AddTagHelperAttribute("value", __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(5129, 2, true);
                WriteLiteral("\r\n");
                EndContext();
#line 116 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
                }

#line default
#line hidden
                BeginContext(5150, 336, true);
                WriteLiteral(@"            </select>
            <input id=""txt_SearchCate"" type=""hidden"" name=""CategoryKey"" />
        </div>
        <div class=""card-footer text-center"">
            <button type=""submit"" class=""btn btn-primary"">
                <i class=""fas fa fa-search""></i>
                Tìm
            </button>
        </div>
    ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Controller = (string)__tagHelperAttribute_5.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_5);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_6.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_6);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(5493, 14, true);
            WriteLiteral("\r\n</aside>\r\n\r\n");
            EndContext();
#line 129 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
  

    var action = ViewContext.RouteData.Values["action"].ToString();
    var controller = ViewContext.RouteData.Values["controller"].ToString();
    var role = UserLog.ListRole.SingleOrDefault(s => s.ActionName == action && s.ControllerName == controller);
    if (role != null)
    {
        if (role.RoleAdd)
        {

#line default
#line hidden
            BeginContext(5840, 12, true);
            WriteLiteral("            ");
            EndContext();
            BeginContext(5852, 172, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "bbcf13261bf043a2a5a304931e339b6c", async() => {
                BeginContext(5928, 92, true);
                WriteLiteral("\r\n                <i class=\"fas fa-plus-circle\"></i>\r\n                Thêm mới\r\n            ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_7);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_5.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_5);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_8.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_8);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(6024, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 142 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
        }
    }

#line default
#line hidden
            BeginContext(6047, 18, true);
            WriteLiteral("\r\n<!--Scripts-->\r\n");
            EndContext();
            DefineSection("scripts", async() => {
                BeginContext(6082, 68, true);
                WriteLiteral("\r\n    <script>\r\n        function Open(id) {\r\n            var url = \"");
                EndContext();
                BeginContext(6151, 59, false);
#line 150 "D:\Projects\_SLN2021_MayHan\Mobile\Views\Guest\List.cshtml"
                  Write(Url.Action("Detail", "Guest", new { CustomerKey = "_id_" }));

#line default
#line hidden
                EndContext();
                BeginContext(6210, 110, true);
                WriteLiteral("\";\r\n            url = url.replace(\"_id_\", id);\r\n            window.location = url;\r\n        }\r\n    </script>\r\n");
                EndContext();
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
