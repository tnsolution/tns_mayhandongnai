﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Desktop.Controllers
{
    public class AuthController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult SignIn()
        {
            ViewBag.Message = "NotAuthen";
            return View("~/Views/Auth/SignIn.cshtml");
        }
        [HttpPost]
        public IActionResult SignIn(string UserName, string Password)
        {
            User_Info zInfo = new User_Info(UserName, Password);
            if (zInfo.Code == "200")
            {
                //check time
                // if (DateTime.Now.Hour < 8 ||
                //     DateTime.Now.Hour > 17)
                // {
                //     ViewBag.Message = "TimeOut";
                //     return View("~/Views/Auth/SignIn.cshtml");
                // }

                ViewBag.Message = "IsAuthen";

                //Get Partner
                var zUser = zInfo.User;
                zUser.Partner = new Partner_Info(zUser.PartnerNumber).Partner;

                //Get Employee
                var zEmployeeKey = zUser.Employee.EmployeeKey;
                if (zEmployeeKey != string.Empty)
                {
                    zUser.Employee = new Employee_Info(zEmployeeKey).Employee;
                }

                //get data access
                string temp = zInfo.User.DataAccess;
                if (temp != null && temp != string.Empty)
                {
                    var zList = JsonConvert.DeserializeObject<List<string>>(temp);
                    string zData = zList.ToStringComma();
                    zUser.DataAccess = zData;
                }

                //get role access
                zInfo.User.ListRole = User_Data.ReadUserRole(zUser.PartnerNumber, zInfo.User.UserKey, out string Message);

                // Save session
                var key = "UserLogged";
                var obj = JsonConvert.SerializeObject(zUser);
                HttpContext.Session.SetString(key, obj);

                //Update Logged
                zInfo.UpdateLogged();
             
            }
            else
            {
                ViewBag.Message = "405";
            }

            return View("~/Views/Auth/SignIn.cshtml");
        }

        public IActionResult SignOut()
        {
            //HttpContext.Session.Clear();
            return View("~/Views/Auth/SignIn.cshtml");
        }
    }
}
