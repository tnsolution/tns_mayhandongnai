﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Desktop.Controllers;
using Desktop;

namespace TN_HRM.Controllers
{
    public class EmployeeController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult List()
        {
            ViewBag.ListData = Employee_Data.List(UserLog.PartnerNumber, string.Empty);
            ViewBag.ListPosition = Position_Data.List(UserLog.PartnerNumber);
            ViewBag.ListDepartment = Department_Data.List(UserLog.PartnerNumber);
            ViewBag.ListBranch = Branch_Data.List(UserLog.PartnerNumber);
            return View();
        }

        [HttpPost]
        public JsonResult Save_Employee(
                string EmployeeKey, string EmployeeID, string LastName, string FirstName,
                int PositionKey, string PositionName, string MobiPhone = "",
                string ReportToKey = "", string ReportToName = "",
                string BranchKey = "0", string BranchName = "",
                string DepartmentKey = "0", string DepartmentName = "")
        {
            var zResult = new ServerResult();
            var zInfo = new Employee_Info();
            var zModel = new Employee_Model
            {
                PartnerNumber = UserLog.PartnerNumber,
                EmployeeID = EmployeeID.Trim(),
                LastName = LastName.Trim(),
                FirstName = FirstName.Trim(),
                PositionKey = PositionKey,
                PositionName = PositionName.Trim(),

                BranchKey = BranchKey.ToInt(),
                BranchName = BranchName,
                DepartmentKey = DepartmentKey.ToInt(),
                DepartmentName = DepartmentName,

                MobiPhone = MobiPhone.Trim(),
                ReportToKey = ReportToKey ?? "",
                ReportToName = ReportToName ?? "",
                CreatedBy = UserLog.UserKey,
                CreatedName = UserLog.Employee.FullName,
                ModifiedBy = UserLog.UserKey,
                ModifiedName = UserLog.Employee.FullName
            };

            zModel.ClearNullable();

            EmployeeKey = EmployeeKey ?? "";
            if (EmployeeKey == "")
            {
                zInfo.Employee = zModel;
                zInfo.Create_ServerKey();
            }
            else
            {
                zModel.EmployeeKey = EmployeeKey;
                zInfo.Employee = zModel;
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult);
            }
        }

        [HttpGet]
        public JsonResult Edit_Employee(string EmployeeKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Employee_Info(EmployeeKey);

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zInfo.Employee);
                return Json(zResult);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult);
            }
        }
        [HttpPost]
        public JsonResult Delete_Employee(string EmployeeKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Employee_Info();
            zInfo.Employee.EmployeeKey = EmployeeKey;
            zInfo.Delete();
            var zModel = zInfo.Employee;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult);
            }
        }

        public JsonResult GetID()
        {
            string ID = Employee_Data.AutoID("MH", UserLog.PartnerNumber);
            return Json(ID);
        }
    }
}