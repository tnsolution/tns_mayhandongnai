﻿using Desktop.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Desktop.Controllers
{
    public class HomeController : BaseController
    {
        private string _Color = "#383f4882,#0088cc82,#e361598f,#2baab194,#8000807a,#ffa50080";
        public IActionResult Index()
        {
            ViewBag.ListBranch = Branch_Data.List(UserLog.PartnerNumber);

            DateTime zFromDate;
            DateTime zToDate;
            zFromDate = TN_Utils.FirstDayOfWeek(DateTime.Now);
            zToDate = TN_Utils.LastDayOfWeek(DateTime.Now);

            zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            ViewBag.FromDate = zFromDate.ToString("dd/MM/yyyy");
            ViewBag.ToDate = zToDate.ToString("dd/MM/yyyy");

            ViewBag.Task = Report.Total_Task(UserLog.PartnerNumber, zFromDate, zToDate);
            ViewBag.Sales = Report.Total_Sale(UserLog.PartnerNumber, zFromDate, zToDate);
            ViewBag.Guest = Report.Total_Guest(UserLog.PartnerNumber, zFromDate, zToDate);
            ViewBag.DataChart = PrepairData(zFromDate, zToDate);

            ViewBag.TableStaff = Report.Sales_Staff(UserLog.PartnerNumber, zFromDate, zToDate, out string Message);
            ViewBag.TableGuest = Report.Sales_Guest(UserLog.PartnerNumber, zFromDate, zToDate, out Message);

            var zList = Task_Data.Tracking(UserLog.PartnerNumber, 0, string.Empty, string.Empty, string.Empty, string.Empty, zFromDate, zToDate, out Message);
            var zListStatus = new List<Rpt_ItemSale>();
            if (zList.Count > 0)
            {
                zListStatus.Add(new Rpt_ItemSale
                {
                    ItemName = "Việc mới",
                    ItemAmount = zList.Count(s => s.CategoryKey == 21 || s.CategoryKey == 26),
                });

                zListStatus.Add(new Rpt_ItemSale
                {
                    ItemName = "Đang làm",
                    ItemAmount = zList.Count(s => s.CategoryKey == 22 || s.CategoryKey == 23 || s.CategoryKey == 27 || s.CategoryKey == 28),
                });

                zListStatus.Add(new Rpt_ItemSale
                {
                    ItemName = "Đã hoàn thành",
                    ItemAmount = zList.Count(s => s.CategoryKey == 24 || s.CategoryKey == 29),
                });

                zListStatus.Add(new Rpt_ItemSale
                {
                    ItemName = "Đã giao/ trả",
                    ItemAmount = zList.Count(s => s.CategoryKey == 25 || s.CategoryKey == 30),
                });
            }

            ViewBag.TableStatus = zListStatus;

            return View();
        }
        private string PrepairData(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = Report.DB_ChartAIO(UserLog.PartnerNumber, "DONHANG,DOANHSO,CONGNO,CHIPHI,KHACHHANG,LOINHUAN", _Color, FromDate, ToDate, out string Message);

            string xlabel = ChartJS_XLabelName(zTable);
            string xvalue = ChartJS_Dataset(zTable);

            string DataChart = "{labels: [" + xlabel + "], datasets: [" + xvalue + "]}";
            return DataChart;
        }

        private string ChartJS_XLabelName(DataTable Table)
        {
            string zLabel = "";
            for (int i = 3; i < Table.Columns.Count; i++)
            {
                DataColumn c = Table.Columns[i];
                zLabel += "'" + c.ColumnName + "',";
            }
            zLabel = zLabel.Remove(zLabel.LastIndexOf(','), 1);
            zLabel += "";
            return zLabel;
        }
        private string ChartJS_Dataset(DataTable Table)
        {
            string zDataset = "";
            foreach (DataRow r in Table.Rows)
            {
                int Num = TN_Utils.RandomNumber(0, 299);

                zDataset += "{ label: '" + r[0].ToString() + "',";
                //zDataset += " backgroundColor: 'rgba(" + Num + ", 99, " + Num + ", 0.2)',";
                zDataset += " backgroundColor: '" + r[1].ToString() + "',";
                zDataset += " borderColor: 'rgba(" + Num + ", 99, " + Num + ", 1)',";
                zDataset += " borderWidth: 1,";
                zDataset += " data: [";
                for (int i = 3; i < Table.Columns.Count; i++)
                {
                    if (r[i] != DBNull.Value)
                    {
                        zDataset += Convert.ToDouble(r[i]) + ",";
                    }
                    else
                    {
                        zDataset += "0,";
                    }
                }
                zDataset = zDataset.Remove(zDataset.LastIndexOf(','), 1);
                zDataset += " ]},";
            }

            zDataset = zDataset.Remove(zDataset.LastIndexOf(','), 1);
            return zDataset;
        }

        //
        public IActionResult GetDataChart(string btnAction = "", string DateView = "", string FromDate = "", string ToDate = "", string Branch = "", string Option = "")
        {
            Option = Option ?? "DONHANG,DOANHSO,CONGNO,CHIPHI,KHACHHANG,LOINHUAN";
            Branch = Branch == null || Branch == "0" ? UserLog.DataAccess : Branch;

            ViewBag.ListBranch = Branch_Data.List(UserLog.PartnerNumber);
            ViewBag.Branch = Branch;

            DateTime zFromDate;
            DateTime zToDate;
            if (FromDate == string.Empty &&
                ToDate == string.Empty)
            {
                zFromDate = TN_Utils.FirstDayOfWeek(DateTime.Now);
                zToDate = TN_Utils.LastDayOfWeek(DateTime.Now);

                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            }
            else
            {
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);

                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

                switch (btnAction)
                {
                    case "btn_Prev":
                        zFromDate = zFromDate.AddDays(-6);
                        zToDate = zToDate.AddDays(-6);
                        break;
                    case "btn_Next":
                        zFromDate = zFromDate.AddDays(+6);
                        zToDate = zToDate.AddDays(+6);
                        break;
                }
            }

            ViewBag.FromDate = zFromDate.ToString("dd/MM/yyyy");
            ViewBag.ToDate = zToDate.ToString("dd/MM/yyyy");

            ViewBag.Task = Report.Total_Task(UserLog.PartnerNumber, Branch, zFromDate, zToDate);
            ViewBag.Sales = Report.Total_Sale(UserLog.PartnerNumber, Branch, zFromDate, zToDate);
            ViewBag.Guest = Report.Total_Guest(UserLog.PartnerNumber, Branch, zFromDate, zToDate);

            DataTable zTable = Report.DB_ChartAIO(UserLog.PartnerNumber, Branch, Option, _Color, zFromDate, zToDate, out string Message);
            string DataChart = "{}";
            if (Message == string.Empty)
            {
                if (zTable.Rows.Count > 0)
                {
                    string xlabel = ChartJS_XLabelName(zTable);
                    string xvalue = ChartJS_Dataset(zTable);
                    DataChart = "{labels: [" + xlabel + "], datasets: [" + xvalue + "]}";
                    ViewBag.DataChart = DataChart;
                }
                TempData["Error"] = "Không tìm thấy dữ liệu !.";
            }
            else
            {
                TempData["Error"] = Message.GetFirstLine();
            }
            ViewBag.DataChart = DataChart;
            ViewBag.Option = Option;

            ViewBag.TableStaff = Report.Sales_Staff(UserLog.PartnerNumber, Branch, zFromDate, zToDate, out Message);
            ViewBag.TableGuest = Report.Sales_Guest(UserLog.PartnerNumber, Branch, zFromDate, zToDate, out Message);

            var zList = Task_Data.Tracking(UserLog.PartnerNumber, Branch, 0, string.Empty, string.Empty, string.Empty, string.Empty, zFromDate, zToDate, out Message);
            var zListStatus = new List<Rpt_ItemSale>();
            if (zList.Count > 0)
            {
                zListStatus.Add(new Rpt_ItemSale
                {
                    ItemName = "Việc mới",
                    ItemAmount = zList.Count(s => s.CategoryKey == 21 || s.CategoryKey == 26),
                });

                zListStatus.Add(new Rpt_ItemSale
                {
                    ItemName = "Đang làm",
                    ItemAmount = zList.Count(s => s.CategoryKey == 22 || s.CategoryKey == 23 || s.CategoryKey == 27 || s.CategoryKey == 28),
                });

                zListStatus.Add(new Rpt_ItemSale
                {
                    ItemName = "Đã hoàn thành",
                    ItemAmount = zList.Count(s => s.CategoryKey == 24 || s.CategoryKey == 29),
                });

                zListStatus.Add(new Rpt_ItemSale
                {
                    ItemName = "Đã giao/ trả",
                    ItemAmount = zList.Count(s => s.CategoryKey == 25 || s.CategoryKey == 30),
                });
            }

            ViewBag.TableStatus = zListStatus;

            return View("~/Views/Home/Index.cshtml");
        }
    }
}
