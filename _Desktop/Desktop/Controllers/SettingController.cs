﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using Desktop;
using Desktop.Controllers;

namespace TN_Setting.Controllers
{
    public class SettingController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }

        #region [Branch_Setting]
        public IActionResult Branch()
        {
            ViewBag.ListBranch = Branch_Data.List(UserLog.PartnerNumber);
            return View();
        }
        public JsonResult Save_Branch(string BranchKey = "", string BranchID = "", string BranchName = "", string Address = "", string Description = "", int Rank = 0)
        {
            var zResult = new ServerResult();
            var zInfo = new Branch_Info();
            var zModel = new Branch_Model();

            BranchKey = BranchKey ?? "0";
            BranchID = BranchID ?? "";
            BranchName = BranchName ?? "";
            Address = Address ?? "";
            Description = Description ?? "";

            zModel.PartnerNumber = UserLog.PartnerNumber;
            zModel.BranchKey = BranchKey.ToInt();
            zModel.BranchID = BranchID.Trim();
            zModel.BranchName = BranchName.Trim();
            zModel.Address = Address.Trim();
            zModel.Rank = Rank;
            zModel.Description = Description.Trim();
            zModel.CreatedBy = UserLog.UserKey;
            zModel.CreatedName = UserLog.Employee.FullName;
            zModel.ModifiedBy = UserLog.UserKey;
            zModel.ModifiedName = UserLog.Employee.FullName;


            zModel.ClearNullable();
            if (BranchKey=="0")
            {
                zInfo.Branch = zModel;
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Branch = zModel;
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
                return Json(zResult);
            }
        }
        public JsonResult Edit_Branch(string BranchKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Branch_Info(BranchKey);
            var zModel = zInfo.Branch;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }
            return Json(zResult);
        }
        public JsonResult Delete_Branch(string BranchKey = "")
        {
            var zResult = new ServerResult();
            var zInfo = new Branch_Info();
            BranchKey = BranchKey ?? "0";
            zInfo.Branch.BranchKey = BranchKey.ToInt();
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
                return Json(zResult);
            }
        }
        #endregion

        #region [Depart_Setting]
        public IActionResult Department()
        {
            ViewBag.ListDepartment = Department_Data.List(UserLog.PartnerNumber);
            ViewBag.ListBranch = Branch_Data.List(UserLog.PartnerNumber);
            return View();
        }
        public JsonResult Save_Department(string DepartmentKey = "", string DepartmentID = "", string DepartmentName = "", string Description = "", int Rank = 0, string BranchKey = "0", string BranchName = "")
        {
            var zResult = new ServerResult();
            var zInfo = new Department_Info();
            var zModel = new Department_Model();

            DepartmentKey = DepartmentKey ?? "0";
            DepartmentID = DepartmentID ?? "";
            DepartmentName = DepartmentName ?? "";
            Description = Description ?? "";

            zModel.PartnerNumber = UserLog.PartnerNumber;
            zModel.DepartmentKey = DepartmentKey.ToInt();
            zModel.DepartmentID = DepartmentID.Trim();
            zModel.DepartmentName = DepartmentName.Trim();
            zModel.Description = Description.Trim();
            zModel.Rank = Rank;
            zModel.BranchKey = BranchKey.ToInt();
            zModel.BranchName = BranchName;
            zModel.CreatedBy = UserLog.UserKey;
            zModel.CreatedName = UserLog.Employee.FullName;
            zModel.ModifiedBy = UserLog.UserKey;
            zModel.ModifiedName = UserLog.Employee.FullName;

            zModel.ClearNullable();
            if (DepartmentKey == "0")
            {
                zInfo.Department = zModel;
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Department = zModel;
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
                return Json(zResult);
            }
        }
        public JsonResult Edit_Department(string DepartmentKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Department_Info(DepartmentKey);
            var zModel = zInfo.Department;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
                return Json(zResult);
            }
        }
        public JsonResult Delete_Department(string DepartmentKey = "")
        {
            var zResult = new ServerResult();
            var zInfo = new Department_Info();
            DepartmentKey = DepartmentKey ?? "0";
            zInfo.Department.DepartmentKey = DepartmentKey.ToInt();
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
                return Json(zResult);
            }
        }
        #endregion

        #region [Position_Setting]
        public IActionResult Position()
        {
            ViewBag.Position = Position_Data.List(UserLog.PartnerNumber);
            return View();
        }
        public JsonResult Save_Position(int PositionKey = 0, string PositionID = "", string PositionNameVN = "", string Description = "", int Rank = 0)
        {
            var zResult = new ServerResult();
            var zInfo = new Position_Info();
            var zModel = new Position_Model
            {
                PartnerNumber = UserLog.PartnerNumber,
                PositionKey = PositionKey,
                PositionID = PositionID,
                PositionNameVN = PositionNameVN,
                Rank = Rank,
                Description = Description,

                CreatedBy = UserLog.UserKey,
                CreatedName = UserLog.Employee.FullName,
                ModifiedBy = UserLog.UserKey,
                ModifiedName = UserLog.Employee.FullName
            };

            zModel.ClearNullable();
            if (PositionKey == 0)
            {
                zInfo.Position = zModel;
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Position = zModel;
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
                return Json(zResult);
            }
        }
        public JsonResult Edit_Position(int PositionKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Position_Info(PositionKey);

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zInfo.Position);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }

            return Json(zResult);
        }
        public JsonResult Delete_Position(int PositionKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Position_Info();
            zInfo.Position.PositionKey = PositionKey;
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
                return Json(zResult);
            }
        }
        #endregion

        #region [Role_Setting]
        /// <summary>
        /// Phân quyền thao tác
        /// </summary>
        /// <param name="UserKey"></param>
        /// <returns></returns>
        public IActionResult AuthRole(string UserKey)
        {
            var zList = User_Data.ListUserRole(UserLog.PartnerNumber, UserKey, out string Message);
            ViewBag.ListData = zList;
            return PartialView("~/Views/Setting/Partial/_Role.cshtml");
        }
        /// <summary>
        /// Phân quyền xem dữ liệu
        /// </summary>
        /// <param name="UserKey"></param>
        /// <returns></returns>
        public IActionResult AuthData(string UserKey)
        {
            string Message = "";
            var zList = User_Data.ListUserAccess(UserLog.PartnerNumber, UserKey, out Message);
            ViewBag.ListArea = Branch_Data.List(UserLog.PartnerNumber); //Product_Land_Data.List(UserLog.PartnerNumber);
            ViewBag.DataAccess = zList.ToStringComma();
            return PartialView("~/Views/Setting/Partial/_Data.cshtml");
        }
        public JsonResult AuthSave(string UserKey, string Role, string Data)
        {
            var zResult = new ServerResult();
            var zInfo = new User_Info();

            zInfo.User.PartnerNumber = UserLog.PartnerNumber;
            zInfo.User.CreatedBy = UserLog.UserKey;
            zInfo.User.CreatedName = UserLog.Employee.FullName;
            zInfo.User.ModifiedBy = UserLog.UserKey;
            zInfo.User.ModifiedName = UserLog.Employee.FullName;

            if (Role.Length > 0)
            {
                var zRole = JsonConvert.DeserializeObject<List<User_Role>>(Role);
                zInfo.SetRoleAccess(zRole, UserKey);
                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zResult.Success = true;
                    zResult.Message = "";
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message.GetFirstLine();
                    return Json(zResult);
                }
            }
            if (Data.Length > 0)
            {
                var zData = JsonConvert.DeserializeObject<List<string>>(Data);
                zInfo.SetDataAccess(zData, UserKey, "TableName");
                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zResult.Success = true;
                    zResult.Message = "";
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message.GetFirstLine();
                    return Json(zResult);
                }
            }

            return Json(zResult);
        }
        #endregion

        #region[Account_Setting]
        public IActionResult Users()
        {
            ViewBag.ListUser = User_Data.List(UserLog.PartnerNumber);
            ViewBag.ListEmployee = Desktop.Employee_Data.List(UserLog.PartnerNumber, string.Empty);
            return View("~/Views/Setting/User.cshtml");
        }
        public JsonResult Save_User(string UserKey, string UserName, string Password, string EmployeeKey, string Description, string Activate, string ExpireDate)
        {
            UserKey = UserKey ?? "";
            Description = Description ?? "";

            var zResult = new ServerResult();
            var zInfo = new User_Info(UserKey);
            zInfo.User.PartnerNumber = UserLog.PartnerNumber;
            zInfo.User.UserName = UserName.Trim();
            zInfo.User.Description = Description.Trim();

            if (UserKey != string.Empty)
            {
                //kiểm tra cập nhật mật khẩu
                if (zInfo.User.Password != Password.Trim())
                {
                    zInfo.User.Password = TN_Utils.HashPass(Password.Trim());
                }
                else
                {
                    zInfo.User.Password = Password.Trim();
                }
            }
            else
            {
                zInfo.User.Password = TN_Utils.HashPass(Password.Trim());
            }

            DateTime zExpireDate = DateTime.MinValue;
            if (ExpireDate != null && ExpireDate != string.Empty)
            {
                DateTime.TryParseExact(ExpireDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zExpireDate);
                zExpireDate = new DateTime(zExpireDate.Year, zExpireDate.Month, zExpireDate.Day, 0, 0, 0);
            }

            zInfo.User.ExpireDate = zExpireDate;
            if (Activate.ToInt() == 0)
            {
                zInfo.User.Activate = false;
            }
            else
            {
                zInfo.User.Activate = true;
            }

            zInfo.User.CreatedBy = UserLog.UserKey;
            zInfo.User.CreatedName = UserLog.Employee.FullName;
            zInfo.User.ModifiedBy = UserLog.UserKey;
            zInfo.User.ModifiedName = UserLog.Employee.FullName;

            //set employee use
            zInfo.User.Employee.EmployeeKey = EmployeeKey.Trim();

            if (UserKey.Length < 36)
            {
                zInfo.User.UserKey = Guid.NewGuid().ToString();
                zInfo.Create_ClientKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
                return Json(zResult);
            }
        }
        public JsonResult Edit_User(string UserKey)
        {
            var zResult = new ServerResult();
            var zInfo = new User_Info(UserKey);

            //return;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zInfo.User);
                return Json(zResult);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult);
            }
        }
        public JsonResult Delete_User(string UserKey)
        {
            var zResult = new ServerResult();
            var zInfo = new User_Info();
            zInfo.Delete(UserKey);
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
                return Json(zResult);
            }
        }
        public JsonResult Reset_Pass(string UserKey)
        {
            var zResult = new ServerResult();
            string Random = TN_Utils.RandomPassword();
            var zInfo = new User_Info();
            zInfo.User.ModifiedName = UserLog.Employee.FullName;
            zInfo.User.ModifiedBy = UserLog.UserKey;
            zInfo.ResetPass(UserKey, TN_Utils.HashPass(Random));

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = Random;
                zResult.Message = "Mật khẩu mới của " + zInfo.User.UserName + " là: " + Random;
            }
            else
            {
                zResult.Success = false;
                zResult.Data = string.Empty;
                zResult.Message = zInfo.Message.GetFirstLine();
            }

            return Json(zResult);
        }
        public JsonResult Activate(string UserKey)
        {
            var zResult = new ServerResult();
            var zInfo = new User_Info();
            zInfo.User.ModifiedName = UserLog.Employee.FullName;
            zInfo.User.ModifiedBy = UserLog.UserKey;
            zInfo.SetActivate(UserKey);

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = string.Empty;
                zResult.Message = string.Empty;
            }
            else
            {
                zResult.Success = false;
                zResult.Data = string.Empty;
                zResult.Message = zInfo.Message.GetFirstLine();
            }

            return Json(zResult);
        }
        public JsonResult Change_Pass(string OldPass, string NewPass)
        {
            var zResult = new ServerResult();

            var zInfo = new User_Info(UserLog.UserName, OldPass);
            if (zInfo.Code != "200")
            {
                zResult.Success = false;
                zResult.Message = "Tên mật khẩu không đúng vui lòng thử lại !.";
                return Json(zResult);
            }

            zInfo.User.ModifiedName = UserLog.Employee.FullName;
            zInfo.User.ModifiedBy = UserLog.UserKey;

            zInfo.ResetPass(zInfo.User.UserKey, TN_Utils.HashPass(NewPass));

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = NewPass;
                zResult.Message = "Mật khẩu mới của " + zInfo.User.UserName + " là: " + NewPass;
            }
            else
            {
                zResult.Success = false;
                zResult.Data = string.Empty;
                zResult.Message = zInfo.Message.GetFirstLine();
            }

            return Json(zResult);
        }
        #endregion

        #region [Group_Roles]
        public IActionResult Group()
        {
            ViewBag.ListGroup = Group_Data.List(UserLog.PartnerNumber, out string Message);
            return View("~/Views/Setting/Group/Group.cshtml");
        }
        public JsonResult Save_Group(string GroupKey = "", string GroupName = "", string Description = "")
        {
            var zResult = new ServerResult();
            var zInfo = new Group_Info();
            var zModel = new Group_Model
            {
                PartnerNumber = UserLog.PartnerNumber,
                GroupKey = GroupKey,
                GroupName = GroupName.Trim(),
                Description = Description.Trim(),
                CreatedBy = UserLog.UserKey,
                CreatedName = UserLog.Employee.FullName,
                ModifiedBy = UserLog.UserKey,
                ModifiedName = UserLog.Employee.FullName
            };

            zModel.ClearNullable();
            if (string.IsNullOrEmpty(GroupKey))
            {
                zInfo.Group = zModel;
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Group = zModel;
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
                return Json(zResult);
            }
        }
        public JsonResult Edit_Group(string GroupKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Group_Info(GroupKey);
            var zModel = zInfo.Group;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }
            return Json(zResult);
        }
        public JsonResult Delete_Group(string GroupKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Group_Info();
            zInfo.Group.GroupKey = GroupKey;
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
                return Json(zResult);
            }
        }

        //
        public IActionResult Detail(string GroupKey, string Title)
        {
            ViewBag.Title = Title;
            ViewBag.ListRoleGroup = Group_Roles_Data.List(UserLog.PartnerNumber, GroupKey, out string Message);
            ViewBag.ListRolePartner = Role_Partner_Data.List(UserLog.PartnerNumber, GroupKey, out Message);
            return View("~/Views/Setting/Group/_Detail.cshtml");
        }
        //
        public JsonResult Role_Group_Add_All(List<User_Role> ListRole, string GroupKey)
        {
            var zResult = new ServerResult();
            if (ListRole.Count > 0)
            {
                string SQL = "DELETE SYS_Group_Roles WHERE GroupKey = '" + Guid.Parse(GroupKey) + "'";
                foreach (var item in ListRole)
                {
                    SQL += @"INSERT INTO SYS_Group_Roles (RoleKey, GroupKey, CreatedBy, CreatedName) 
                                    VALUES ('" + Guid.Parse(item.RoleKey) + "' , '" + Guid.Parse(GroupKey) + "', N'" + UserLog.UserKey + "', N'" + UserLog.Employee.FullName + "')" + Environment.NewLine;
                }

                string Message = "";
                if (SQL != string.Empty)
                {
                    Misc.ApplySQL(SQL, out Message);
                }

                if (Message == string.Empty)
                {
                    zResult.Message = "Đã cập nhật thành công !.";
                    zResult.Success = true;
                }
                else
                {
                    zResult.Message = Message.GetFirstLine();
                    zResult.Success = false;
                }
            }
            else
            {
                zResult.Message = "Không có dữ liệu";
                zResult.Success = false;
            }
            return Json(zResult);
        }
        public JsonResult Role_Group_Remove_All(string GroupKey)
        {
            var zResult = new ServerResult();
            string SQL = "DELETE SYS_Group_Roles WHERE GroupKey = '" + Guid.Parse(GroupKey) + "'";
            string Message = "";
            if (SQL != string.Empty)
            {
                Misc.ApplySQL(SQL, out Message);
            }

            if (Message == string.Empty)
            {
                zResult.Message = "Đã cập nhật thành công !.";
                zResult.Success = true;
            }
            else
            {
                zResult.Message = Message.GetFirstLine();
                zResult.Success = false;
            }
            return Json(zResult);
        }
        public JsonResult Role_Group_Add(string RoleKey, string GroupKey)
        {
            var zResult = new ServerResult();
            string SQL = @"INSERT INTO SYS_Group_Roles (RoleKey, GroupKey, CreatedBy, CreatedName) 
                                    VALUES ('" + Guid.Parse(RoleKey) + "' , '" + Guid.Parse(GroupKey) + "', N'" + UserLog.UserKey + "', N'" + UserLog.Employee.FullName + "')" + Environment.NewLine;
            string Message = "";
            if (SQL != string.Empty)
            {
                Misc.ApplySQL(SQL, out Message);
            }

            if (Message == string.Empty)
            {
                zResult.Message = "Đã cập nhật thành công !.";
                zResult.Success = true;
            }
            else
            {
                zResult.Message = Message.GetFirstLine();
                zResult.Success = false;
            }
            return Json(zResult);
        }
        public JsonResult Role_Group_Remove(string RoleKey, string GroupKey)
        {
            var zResult = new ServerResult();
            string SQL = @"DELETE SYS_Group_Roles WHERE GroupKey = '" + Guid.Parse(GroupKey) + "' AND RoleKey = '" + Guid.Parse(RoleKey) + "'";
            string Message = "";
            if (SQL != string.Empty)
            {
                Misc.ApplySQL(SQL, out Message);
            }

            if (Message == string.Empty)
            {
                zResult.Message = "Đã cập nhật thành công !.";
                zResult.Success = true;
            }
            else
            {
                zResult.Message = Message.GetFirstLine();
                zResult.Success = false;
            }
            return Json(zResult);
        }
        public JsonResult Role_Save(string GroupKey, string Role)
        {
            var zResult = new ServerResult();
            if (Role.Length > 0)
            {
                var zRole = JsonConvert.DeserializeObject<List<Group_Roles_Model>>(Role);
                var zInfo = new Group_Info();
                zInfo.Group.ModifiedBy = UserLog.UserKey;
                zInfo.Group.CreatedBy = UserLog.UserKey;
                zInfo.Group.ModifiedName = UserLog.Employee.FullName;
                zInfo.Group.CreatedName = UserLog.Employee.FullName;
                zInfo.SetRoleAccess(zRole, GroupKey);
                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zResult.Success = true;
                    zResult.Message = "";
                    return Json(zResult);
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message.GetFirstLine();
                    return Json(zResult);
                }
            }
            zResult.Success = false;
            zResult.Message = "Chưa chọn dữ liệu !.";
            return Json(zResult);
        }
        //
        public IActionResult Role_Default(string GroupKey)
        {
            ViewBag.ListRolePartner = Role_Partner_Data.List(UserLog.PartnerNumber, GroupKey, out string Message);
            return View("~/Views/Setting/Group/_RoleDefault.cshtml");
        }
        public IActionResult Role_Group(string GroupKey)
        {
            ViewBag.ListRoleGroup = Group_Roles_Data.List(UserLog.PartnerNumber, GroupKey, out string Message);
            return View("~/Views/Setting/Group/_RoleGroup.cshtml");
        }
        #endregion
    }
}