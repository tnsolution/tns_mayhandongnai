﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Desktop.Controllers;
using Desktop;

namespace TN_PMS.Controllers
{
    public class TaskController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }

        //----------------Tracking function
        public IActionResult Tracking()
        {
            ViewBag.ListStaff = Employee_Data.List(UserLog.PartnerNumber, string.Empty);
            ViewBag.ListBranch = Branch_Data.List(UserLog.PartnerNumber);

            DateTime zFromDate = DateTime.Now;
            zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            var zList = Task_Data.Tracking(UserLog.PartnerNumber, 0, string.Empty, string.Empty, string.Empty, string.Empty, zFromDate, zToDate, out string Message);
            ViewBag.DataNew = zList.Where(s => s.CategoryKey == 21 || s.CategoryKey == 26).ToList();
            ViewBag.DataDoing = zList.Where(s => s.CategoryKey == 22 || s.CategoryKey == 23 || s.CategoryKey == 27 || s.CategoryKey == 28).ToList();
            ViewBag.DataDone = zList.Where(s => s.CategoryKey == 24 || s.CategoryKey == 29).ToList();
            ViewBag.DataDeliver = zList.Where(s => s.CategoryKey == 25 || s.CategoryKey == 30).ToList();

            return View("~/Views/Task/Track/List.cshtml");
        }
        public IActionResult Tracking_Filter(int Group, string GuestName, string GuestPhone, string Branch, string Employee, string FromDate, string ToDate)
        {
            ViewBag.ListStaff = Employee_Data.List(UserLog.PartnerNumber, string.Empty);
            ViewBag.ListBranch = Branch_Data.List(UserLog.PartnerNumber);

            //retain search option
            ViewBag.txt_FromDate = FromDate;
            ViewBag.txt_ToDate = ToDate;
            ViewBag.txt_SearchName = GuestName = GuestName ?? "";
            ViewBag.txt_SearchPhone = GuestPhone = GuestPhone ?? "";
            ViewBag.cbo_Branch = Branch = Branch ?? "";
            ViewBag.cbo_Employee = Employee = Employee ?? "";
            ViewBag.cbo_Group = Group;

            DateTime zFromDate = DateTime.MinValue;
            if (FromDate != null && FromDate != string.Empty)
            {
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
            }
            DateTime zToDate = DateTime.MinValue;
            if (ToDate != null && ToDate != string.Empty)
            {
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            }

            var zList = Task_Data.Tracking(UserLog.PartnerNumber, Branch, Group, Employee, GuestName, GuestPhone, string.Empty, zFromDate, zToDate, out string Message);
            ViewBag.DataNew = zList.Where(s => s.CategoryKey == 21 || s.CategoryKey == 26).ToList();
            ViewBag.DataDoing = zList.Where(s => s.CategoryKey == 22 || s.CategoryKey == 23 || s.CategoryKey == 27 || s.CategoryKey == 28).ToList();
            ViewBag.DataDone = zList.Where(s => s.CategoryKey == 24 || s.CategoryKey == 29).ToList();
            ViewBag.DataDeliver = zList.Where(s => s.CategoryKey == 25 || s.CategoryKey == 30).ToList();

            return View("~/Views/Task/Track/List.cshtml");
        }

        public IActionResult Tracking_Detail(string TaskKey)
        {
            TaskKey = TaskKey ?? "";
            var zTask = new Task_Model();
            if (TaskKey != string.Empty)
            {
                var zInfo = new Task_Info(TaskKey);
                zTask = zInfo.Task;
            }

            var Logs = Task_Data.Log(TaskKey, out string Message);
            ViewBag.History = Logs;
            return View("~/Views/Task/Track/_recView.cshtml", zTask);
        }
        private List<string> Count_Task()
        {
            var data = new List<string>();

            string Cate = "21,26";
            int Coung = Task_Data.Count(UserLog.PartnerNumber, Cate);
            data.Add(Coung.ToString());

            //chưa thực hiện
            Cate = "22,23,27,28";
            Coung = Task_Data.Count(UserLog.PartnerNumber, Cate);
            data.Add(Coung.ToString());

            //đang làm
            Cate = "24,29";
            Coung = Task_Data.Count(UserLog.PartnerNumber, Cate);
            data.Add(Coung.ToString());

            //đã hoàn thành
            Cate = "25,30";
            Coung = Task_Data.Count(UserLog.PartnerNumber, Cate);
            data.Add(Coung.ToString());

            return data;
        }

        public IActionResult Monitor(string btnAction = "", string DateView = "", string FromDate = "", string ToDate = "")
        {
            DateTime zFromDate;
            DateTime zToDate;

            if (btnAction == "btn_Search")
            {
                //ngày tự do

                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);

                zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

                ViewBag.DateView = FromDate + "-" + ToDate;
            }
            else
            {
                #region [Datetime string to fromdate - todate]
                int Month = DateTime.Now.Month;
                int Year = DateTime.Now.Year;
                if (DateView != "")
                {
                    Month = DateView.Split('/')[0].ToInt();
                    Year = DateView.Split('/')[1].ToInt();

                    if (Year.ToString().Length < 4)
                    {
                        Month = DateTime.Now.Month;
                        Year = DateTime.Now.Year;
                    }
                }

                switch (btnAction)
                {
                    case "btn_Prev":
                        Month = Month - 1;
                        break;
                    case "btn_Next":
                        Month = Month + 1;
                        break;
                }
                if (Month <= 0)
                {
                    Month = 12;
                    Year = Year - 1;
                }
                if (Month >= 13)
                {
                    Month = 1;
                    Year = Year + 1;
                }
                DateTime zDateView = DateTime.Now;
                zDateView = new DateTime(Year, Month, 1, 23, 59, 59);
                zFromDate = new DateTime(zDateView.Year, zDateView.Month, 1, 0, 0, 0);
                zToDate = zFromDate.AddMonths(1).AddDays(-1);
                zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
                #endregion

                ViewBag.DateView = zToDate.Month + "/" + zToDate.Year;
            }

            var zData = Task_Data.Monitor(UserLog.PartnerNumber, out _, zFromDate, zToDate);
            ViewBag.Data = zData;
            return View("~/Views/Task/Track/Monitor.cshtml");
        }

        public JsonResult Delete(string TaskKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Task_Info();
            zInfo.Task.TaskKey = TaskKey;
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }

            return Json(zResult);
        }
    }
}