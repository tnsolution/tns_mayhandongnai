﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;


namespace Desktop
{
    public class Role_Partner_Data
    {
        public static List<Role_Partner_Model> List(string PartnerNumber, string GroupKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT CONVERT(NVARCHAR(50), A.PartnerNumber) PartnerNumber, 
B.RoleID, B.RoleKey, B.RoleName, B.RoleURL, B.Module, 'True' AS Access, 
B.[Level], ISNULL(B.Parent,0) AS Parent, B.[Description], B.RouteName, B.Slug
FROM SYS_Role_Partner A
LEFT JOIN SYS_Role B ON A.RoleKey = B.RoleKey
WHERE 
PartnerNumber = @PartnerNumber 
AND B.RecordStatus != 99
AND A.RoleKey NOT IN (SELECT RoleKey FROM SYS_Group_Roles WHERE GroupKey = @GroupKey)";
            zSQL += " ORDER BY B.Slug, B.RoleID";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@GroupKey", SqlDbType.NVarChar).Value = GroupKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Role_Partner_Model> zList = new List<Role_Partner_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Role_Partner_Model()
                {
                    RoleName = r["RoleName"].ToString(),
                    RoleKey = r["RoleKey"].ToString(),
                    Parent = r["Parent"].ToString(),
                    Level = r["Level"].ToInt(),
                    Slug = r["Slug"].ToInt()
                });
            }
            return zList;
        }
    }
}
