﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;


namespace Desktop
{
    public class User_Data
    {
        public static List<User_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, B.LastName, B.FirstName, B.MobiPhone
FROM SYS_User A LEFT JOIN HRM_Employee B ON A.EmployeeKey = B.EmployeeKey
WHERE A.RecordStatus != 99  
AND A.PartnerNumber = @PartnerNumber ";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<User_Model> zList = new List<User_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    DateTime Exp = DateTime.MinValue;
                    if (r["ExpireDate"] != DBNull.Value)
                    {
                        Exp = Convert.ToDateTime(r["ExpireDate"]);
                    }

                    DateTime LstLog = DateTime.MinValue;
                    if (r["LastLoginDate"] != DBNull.Value)
                    {
                        LstLog = Convert.ToDateTime(r["LastLoginDate"]);
                    }
                    zList.Add(new User_Model()
                    {
                        UserKey = r["UserKey"].ToString(),
                        UserName = r["UserName"].ToString(),
                        Activate = r["Activate"].ToBool(),
                        LastLoginDate = LstLog ,
                        FailedPasswordCount = r["FailedPasswordCount"].ToInt(),
                        ExpireDate = Exp,
                        PartnerNumber = r["PartnerNumber"].ToString(),
                        Employee = new Employee_Model()
                        {
                            MobiPhone = r["MobiPhone"].ToString(),
                            FirstName = r["FirstName"].ToString(),
                            LastName = r["LastName"].ToString(),
                        }
                    });
                }
            }
            return zList;
        }
        public static int UserCount(string PartnerNumber)
        {
            int zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = @"SELECT COUNT(*) FROM SYS_User WHERE A.RecordStatus != 99 AND A.PartnerNumber = @PartnerNumber ";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zResult = zCommand.ExecuteScalar().ToInt();

                zCommand.Dispose();
            }
            catch (Exception)
            {

            }
            finally
            {
                zConnect.Close(); ;
            }
            return zResult;
        }
        public static int CheckUserName(string UserName)
        {
            int zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = @"SELECT COUNT(*) FROM SYS_User WHERE UserName = @UserName ";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = UserName;
                zResult = zCommand.ExecuteScalar().ToInt();

                zCommand.Dispose();
            }
            catch (Exception)
            {

            }
            finally
            {
                zConnect.Close(); ;
            }
            return zResult;
        }

        //
        public static List<User_Role> ReadUserRole(string PartnerNumber, string UserKey, out string Message)
        {
            List<User_Role> zList = new List<User_Role>();
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
A.UserKey, A.RoleKey, A.RoleRead, A.RoleAdd, A.RoleEdit, A.RoleDel, 
A.RoleApprove, B.RoleName, B.RoleID, B.RoleURL, B.Module, B.Parent, B.Slug,
B.[Level], B.RouteName, B.ActionName, B.ControllerName, B.ParamName, B.Icon
FROM SYS_User_Role A
LEFT JOIN SYS_Role B ON A.RoleKey = B.RoleKey
WHERE B.RecordStatus != 99 
AND A.UserKey = @UserKey
ORDER BY RoleID";

            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();

                if (zTable.Rows.Count > 0)
                {
                    foreach (DataRow r in zTable.Rows)
                    {
                        zList.Add(new User_Role()
                        {
                            UserKey = r["UserKey"].ToString(),
                            RoleKey = r["RoleKey"].ToString(),
                            RoleName = r["RoleName"].ToString(),
                            RoleID = r["RoleID"].ToString(),
                            RoleURL = r["RoleURL"].ToString(),
                            Module = r["Module"].ToString(),
                            RoleRead = r["RoleRead"].ToBool(),
                            RoleAdd = r["RoleAdd"].ToBool(),
                            RoleEdit = r["RoleEdit"].ToBool(),
                            RoleDel = r["RoleDel"].ToBool(),

                            Level = r["Level"].ToInt(),
                            Parent = r["Parent"].ToString(),
                            RouteName = r["RouteName"].ToString(),
                            ActionName = r["ActionName"].ToString(),
                            ControllerName = r["ControllerName"].ToString(),
                            ParamName = r["ParamName"].ToString(),
                            Icon = r["Icon"].ToString(),
                            Slug = r["Slug"].ToInt(),
                        });
                    }
                }

                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            return zList;
        }

        public static List<User_Role> ListUserRole(string PartnerNumber, string UserKey, out string Message)
        {
            List<User_Role> zList = new List<User_Role>();
            DataTable zTable = new DataTable();
            string zSQL = @"
WITH X AS
(
	SELECT N'' UserKey, B.RoleKey, C.PartnerNumber ,B.RoleName, B.RoleURL, B.Module, B.RoleID, B.[Level], ISNULL(B.Parent,0) AS Parent, B.[Description], 'FALSE' RoleRead, 'FALSE' RoleAdd, 'FALSE' RoleEdit, 'FALSE' RoleDel
	FROM SYS_Role_Partner C 
	LEFT JOIN SYS_Role B ON C.RoleKey = B.RoleKey
	WHERE
	C.PartnerNumber = @PartnerNumber
	AND C.RoleKey 
	NOT IN ( SELECT RoleKey FROM SYS_User_Role WHERE CONVERT(NVARCHAR(50), UserKey) = @UserKey )

UNION ALL

  SELECT CONVERT(NVARCHAR(50), A.UserKey) UserKey, B.RoleKey, C.PartnerNumber, B.RoleName, B.RoleURL, B.Module, B.RoleID, B.[Level], ISNULL(B.Parent, 0) AS Parent, B.Description, A.RoleRead, A.RoleAdd, A.RoleEdit, A.RoleDel
  FROM SYS_User_Role A 
  LEFT JOIN SYS_Role B ON A.RoleKey = B.RoleKey
  LEFT JOIN SYS_User C ON C.UserKey = A.UserKey
  WHERE B.RecordStatus != 99 
  AND CONVERT(NVARCHAR(50), A.UserKey)  = @UserKey
  AND CONVERT(NVARCHAR(50), C.PartnerNumber)  = @PartnerNumber
) 
SELECT * FROM X ORDER BY Module, RoleID";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();

                if (zTable.Rows.Count > 0)
                {
                    foreach (DataRow r in zTable.Rows)
                    {
                        zList.Add(new User_Role()
                        {
                            UserKey = r["UserKey"].ToString(),
                            RoleKey = r["RoleKey"].ToString(),
                            RoleName = r["RoleName"].ToString(),
                            RoleID = r["RoleID"].ToString(),
                            RoleURL = r["RoleURL"].ToString(),
                            Module = r["Module"].ToString(),
                            RoleRead = r["RoleRead"].ToBool(),
                            RoleAdd = r["RoleAdd"].ToBool(),
                            RoleEdit = r["RoleEdit"].ToBool(),
                            RoleDel = r["RoleDel"].ToBool(),
                            Level = r["Level"].ToInt(),
                            Parent = r["Parent"].ToString(),
                        });
                    }
                }

                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            return zList;
        }
        public static List<string> ListUserAccess(string PartnerNumber, string UserKey, out string Message)
        {
            List<string> zList = new List<string>();
            string zResult = "";
            string zSQL = @"SELECT JsonKey FROM SYS_User_Access WHERE RecordStatus != 99 AND UserKey = @UserKey";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();

                zList = JsonConvert.DeserializeObject<List<string>>(zResult);

                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            return zList;
        }

    }
}

