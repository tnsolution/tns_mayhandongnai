﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Desktop
{
    public class Task_Log_Data
    {
        public static List<Task_Log_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM CRM_Task_Log WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Task_Log_Model> zList = new List<Task_Log_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Task_Log_Model()
                {
                    AutoKey = r["AutoKey"].ToInt(),
                    TaskKey = r["TaskKey"].ToString(),
                    CategoryKey = r["CategoryKey"].ToString(),
                    JsonData = r["JsonData"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
