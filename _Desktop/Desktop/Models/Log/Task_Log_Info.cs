﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Desktop
{
    public class Task_Log_Info
    {

        public Task_Log_Model Task_Log = new Task_Log_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Task_Log_Info()
        {
        }
        public Task_Log_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM CRM_Task_Log WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                    {
                        Task_Log.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    }

                    Task_Log.TaskKey = zReader["TaskKey"].ToString();
                    Task_Log.CategoryKey = zReader["CategoryKey"].ToString();
                    Task_Log.JsonData = zReader["JsonData"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Task_Log.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Task_Log.CreatedBy = zReader["CreatedBy"].ToString();
                    Task_Log.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Task_Log.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Task_Log.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Task_Log.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO CRM_Task_Log ("
         + " TaskKey , CategoryKey, CategoryName, StatusKey, StatusName, Description, JsonData , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @TaskKey , @CategoryKey, @CategoryName, @StatusKey, @StatusName, @Description, @JsonData , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TaskKey", SqlDbType.NVarChar).Value = Task_Log.TaskKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.NVarChar).Value = Task_Log.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Task_Log.CategoryName;
                zCommand.Parameters.Add("@StatusKey", SqlDbType.NVarChar).Value = Task_Log.StatusKey;
                zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = Task_Log.StatusName;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Task_Log.Description;
                zCommand.Parameters.Add("@JsonData", SqlDbType.NVarChar).Value = Task_Log.JsonData;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Task_Log.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Task_Log.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Task_Log.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Task_Log.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }        
        #endregion
    }
}
