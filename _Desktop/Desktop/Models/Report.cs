﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;


namespace Desktop
{
    public class Report
    {

        public static List<Rpt_ItemSale> Data_BranchGuest(string PartnerNumber, DateTime FromDate, DateTime ToDate, out string Message)
        {
            string zSQL = @"
SELECT A.BranchKey, A.BranchName, ISNULL(dbo.Mayhan_KhachChiNhanh(A.BranchKey, @FromDate, @ToDate),0) AS INCOME
FROM HRM_Branch A
Where A.RecordStatus <> 99 AND A.PartnerNumber = @PartnerNumber";
            DataTable zTable = new DataTable();
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                if (FromDate != DateTime.MinValue &&
                    ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<Rpt_ItemSale> zList = new List<Rpt_ItemSale>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Rpt_ItemSale
                {
                    ItemKey = r["BranchKey"].ToString(),
                    ItemName = r["BranchName"].ToString(),
                    ItemAmount = Convert.ToDouble(r["INCOME"]),
                });
            }

            return zList;
        }
        public static List<Rpt_ItemSale> Data_BranchTask(string PartnerNumber, DateTime FromDate, DateTime ToDate, out string Message)
        {
            string zSQL = @"
SELECT A.BranchKey, A.BranchName, ISNULL(dbo.Mayhan_CongViecChiNhanh(A.BranchKey, @FromDate, @ToDate),0) AS INCOME
FROM HRM_Branch A
Where A.RecordStatus <> 99 AND A.PartnerNumber = @PartnerNumber";
            DataTable zTable = new DataTable();
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                if (FromDate != DateTime.MinValue &&
                    ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<Rpt_ItemSale> zList = new List<Rpt_ItemSale>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Rpt_ItemSale
                {
                    ItemKey = r["BranchKey"].ToString(),
                    ItemName = r["BranchName"].ToString(),
                    ItemAmount = Convert.ToDouble(r["INCOME"]),
                });
            }

            return zList;
        }
        public static List<Rpt_ItemSale> Data_BranchSales(string PartnerNumber, DateTime FromDate, DateTime ToDate, out string Message)
        {
            string zSQL = @"
SELECT A.BranchKey, A.BranchName, ISNULL(dbo.Mayhan_DoanhSoChiNhanh(A.BranchKey, @FromDate, @ToDate),0) AS INCOME
FROM HRM_Branch A
WHERE A.RecordStatus <> 99 
AND A.PartnerNumber = @PartnerNumber
ORDER BY BranchName";

            DataTable zTable = new DataTable();
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<Rpt_ItemSale> zList = new List<Rpt_ItemSale>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Rpt_ItemSale
                {
                    ItemKey = r["BranchKey"].ToString(),
                    ItemName = r["BranchName"].ToString(),
                    ItemAmount = Convert.ToDouble(r["INCOME"]),
                });
            }

            return zList;
        }

        public static List<Rpt_ItemSale> Sales_Branch(string PartnerNumber, DateTime FromDate, DateTime ToDate, out string Message)
        {
            string zSQL = @"
SELECT A.BranchKey, A.BranchName, ISNULL(dbo.Mayhan_DoanhSoChiNhanh(A.BranchKey, @FromDate, @ToDate),0) AS INCOME
FROM HRM_Branch A
WHERE A.RecordStatus <> 99 
AND A.PartnerNumber = @PartnerNumber
ORDER BY BranchName";

            DataTable zTable = new DataTable();
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<Rpt_ItemSale> zList = new List<Rpt_ItemSale>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Rpt_ItemSale
                {
                    ItemKey = r["BranchKey"].ToString(),
                    ItemName = r["BranchName"].ToString(),
                    ItemAmount = Convert.ToDouble(r["INCOME"]),
                });
            }

            return zList;
        }
        public static List<Rpt_ItemSale> Sales_Staff(string PartnerNumber, DateTime FromDate, DateTime ToDate, out string Message)
        {
            string zSQL = @"
SELECT A.LastName + ' ' + A.FirstName AS FullName, A.EmployeeKey, 
ISNULL(dbo.Mayhan_GetRepairedAmount(A.EmployeeKey, @FromDate, @ToDate),0) AS INCOME
FROM HRM_Employee A
WHERE 
A.RecordStatus <> 99 
AND A.PartnerNumber = @PartnerNumber
ORDER BY INCOME DESC";

            DataTable zTable = new DataTable();
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<Rpt_ItemSale> zList = new List<Rpt_ItemSale>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Rpt_ItemSale
                {
                    ItemKey = r["EmployeeKey"].ToString(),
                    ItemName = r["FullName"].ToString(),
                    ItemAmount = Convert.ToDouble(r["INCOME"]),
                });
            }

            return zList;
        }
        public static List<Rpt_ItemSale> Sales_Staff(string PartnerNumber, string Branch, DateTime FromDate, DateTime ToDate, out string Message)
        {
            string zSQL = @"
SELECT A.LastName + ' ' + A.FirstName AS FullName, A.EmployeeKey, 
ISNULL(dbo.Mayhan_GetRepairedAmount(A.EmployeeKey, @FromDate, @ToDate),0) AS INCOME
FROM HRM_Employee A
WHERE 
A.RecordStatus <> 99 
AND A.PartnerNumber = @PartnerNumber
";

            if (Branch != string.Empty && Branch != "0")
            {
                if (Branch.Contains(","))
                {
                    zSQL += "AND BranchKey IN(" + Branch + @")";
                }
                else
                {
                    zSQL += "AND BranchKey = '" + Branch + @"'";
                }
            }
            zSQL += " ORDER BY INCOME DESC";
            DataTable zTable = new DataTable();
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<Rpt_ItemSale> zList = new List<Rpt_ItemSale>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Rpt_ItemSale
                {
                    ItemKey = r["EmployeeKey"].ToString(),
                    ItemName = r["FullName"].ToString(),
                    ItemAmount = Convert.ToDouble(r["INCOME"]),
                });
            }

            return zList;
        }
        public static List<Rpt_ItemSale> Sales_Guest(string PartnerNumber, DateTime FromDate, DateTime ToDate, out string Message)
        {
            string zSQL = @"
SELECT A.CustomerKey, A.CustomerName, 
ISNULL(SUM(TaskAmount),0) AS INCOME
FROM CRM_Task A 
WHERE 
RecordStatus <> 99
AND A.CodeLine = 'Y'
AND A.PartnerNumber = @PartnerNumber
AND A.CreatedOn BETWEEN @FromDate AND @ToDate
AND CAST(A.TaskKey AS NVARCHAR(50)) IN (SELECT DocumentID FROM FNC_Receipt WHERE RecordStatus <> 99 AND PartnerNumber = @PartnerNumber)
GROUP BY A.CustomerKey, A.CustomerName";

            DataTable zTable = new DataTable();
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<Rpt_ItemSale> zList = new List<Rpt_ItemSale>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Rpt_ItemSale
                {
                    ItemKey = r["CustomerKey"].ToString(),
                    ItemName = r["CustomerName"].ToString(),
                    ItemAmount = Convert.ToDouble(r["INCOME"]),
                });
            }
            return zList;
        }

        public static List<Rpt_ItemSale> Sales_Guest(string PartnerNumber, string Branch, DateTime FromDate, DateTime ToDate, out string Message)
        {
            string zSQL = @"
SELECT A.CustomerKey, A.CustomerName, 
ISNULL(SUM(TaskAmount),0) AS INCOME
FROM CRM_Task A 
WHERE 
RecordStatus <> 99
AND A.PartnerNumber = @PartnerNumber
AND A.CreatedOn BETWEEN @FromDate AND @ToDate
AND CAST(A.TaskKey AS NVARCHAR(50)) IN (SELECT DocumentID FROM FNC_Receipt WHERE RecordStatus <> 99 AND PartnerNumber = @PartnerNumber)";
            if (Branch != string.Empty && Branch != "0")
            {
                if (Branch.Contains(","))
                {
                    zSQL += "AND BranchKey IN(" + Branch + @")";
                }
                else
                {
                    zSQL += "AND BranchKey = '" + Branch + @"'";
                }
            }
            zSQL += " GROUP BY A.CustomerKey, A.CustomerName";
            DataTable zTable = new DataTable();
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<Rpt_ItemSale> zList = new List<Rpt_ItemSale>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Rpt_ItemSale
                {
                    ItemKey = r["CustomerKey"].ToString(),
                    ItemName = r["CustomerName"].ToString(),
                    ItemAmount = Convert.ToDouble(r["INCOME"]),
                });
            }
            return zList;
        }

        public static int Total_Task(string PartnerNumber, DateTime FromDate, DateTime ToDate)
        {
            int zResult = 0;
            string zSQL = @"SELECT COUNT(TaskKey) FROM CRM_Task WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (FromDate != DateTime.MinValue &&
                ToDate != DateTime.MinValue)
            {
                zSQL += " AND CreatedOn BETWEEN @FromDate AND @ToDate ";
            }
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                if (FromDate != DateTime.MinValue &&
                    ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }

                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zResult;
        }
        public static int Total_Task(string PartnerNumber, string Branch, DateTime FromDate, DateTime ToDate)
        {
            int zResult = 0;
            string zSQL = @"SELECT COUNT(TaskKey) FROM CRM_Task WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (FromDate != DateTime.MinValue &&
                ToDate != DateTime.MinValue)
            {
                zSQL += " AND CreatedOn BETWEEN @FromDate AND @ToDate ";
            }
            if (Branch != string.Empty && Branch != "0")
            {
                if (Branch.Contains(","))
                {
                    zSQL += "AND BranchKey IN(" + Branch + @")";
                }
                else
                {
                    zSQL += "AND BranchKey = '" + Branch + @"'";
                }
            }
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                if (FromDate != DateTime.MinValue &&
                    ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }

                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zResult;
        }
        public static double Total_Sale(string PartnerNumber, DateTime FromDate, DateTime ToDate)
        {
            double zResult = 0;
            string zSQL = @"SELECT SUM(AmountCurrencyMain) FROM FNC_Receipt WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (FromDate != DateTime.MinValue &&
                ToDate != DateTime.MinValue)
            {
                zSQL += " AND ReceiptDate BETWEEN @FromDate AND @ToDate ";
            }
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                if (FromDate != DateTime.MinValue &&
                    ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }

                zResult = Convert.ToDouble(zCommand.ExecuteScalar());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zResult;
        }

        public static double Total_Sale(string PartnerNumber, string Branch, DateTime FromDate, DateTime ToDate)
        {
            double zResult = 0;
            string zSQL = @"SELECT SUM(AmountCurrencyMain) FROM FNC_Receipt WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (FromDate != DateTime.MinValue &&
                ToDate != DateTime.MinValue)
            {
                zSQL += " AND ReceiptDate BETWEEN @FromDate AND @ToDate ";
            }
            if (Branch != string.Empty && Branch != "0")
            {
                if (Branch.Contains(","))
                {
                    zSQL += "AND BranchKey IN(" + Branch + @")";
                }
                else
                {
                    zSQL += "AND BranchKey = '" + Branch + @"'";
                }
            }
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                if (FromDate != DateTime.MinValue &&
                    ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }

                zResult = Convert.ToDouble(zCommand.ExecuteScalar());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zResult;
        }

        public static int Total_Guest(string PartnerNumber, DateTime FromDate, DateTime ToDate)
        {
            int zResult = 0;
            string zSQL = @"SELECT COUNT(CustomerKey) FROM CRM_Customer WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (FromDate != DateTime.MinValue &&
                ToDate != DateTime.MinValue)
            {
                zSQL += " AND CreatedOn BETWEEN @FromDate AND @ToDate ";
            }
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                if (FromDate != DateTime.MinValue &&
                    ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }

                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zResult;
        }
        public static int Total_Guest(string PartnerNumber, string Branch, DateTime FromDate, DateTime ToDate)
        {
            int zResult = 0;
            string zSQL = @"SELECT COUNT(CustomerKey) FROM CRM_Customer WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (FromDate != DateTime.MinValue &&
                ToDate != DateTime.MinValue)
            {
                zSQL += " AND CreatedOn BETWEEN @FromDate AND @ToDate ";
            }
            if (Branch != string.Empty && Branch != "0")
            {
                if (Branch.Contains(","))
                {
                    zSQL += "AND BranchKey IN(" + Branch + @")";
                }
                else
                {
                    zSQL += "AND BranchKey = '" + Branch + @"'";
                }
            }
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                if (FromDate != DateTime.MinValue &&
                    ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }

                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zResult;
        }
        //
        public static DataTable DB_ChartAIO(string PartnerNumber, string Option, string Color, DateTime FromDate, DateTime ToDate, out string Message)
        {
            Message = string.Empty;
            DataTable zTable = new DataTable();
            if (Option.Length > 0)
            {
                string PivotDate = "";
                for (var i = FromDate.Date; i.Date <= ToDate.Date; i = i.AddDays(1))
                {
                    PivotDate += "[" + i.Day + "],";
                }
                if (PivotDate.Contains(","))
                {
                    PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
                }

                string zSQL = @"
CREATE TABLE #temp
(
	Category NVARCHAR(500) ,
	DateView INT,
	[Value] MONEY,
	[Rank] INT,
    Color NVARCHAR(50)
)";
                string[] temp = Option.Split(',');
                string[] color = Color.Split(',');
                foreach (string s in temp)
                {
                    switch (s)
                    {
                        case "DONHANG":
                            #region --Đơn hàng--
                            zSQL += @"
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Số công việc', 1, 0, 1, '" + color[0] + @"')
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color)
SELECT 
N'Số công việc',
DAY(CreatedOn),
COUNT(TaskKey), 
1, '" + color[0] + @"'
FROM CRM_Task
WHERE RecordStatus <> 99
AND PartnerNumber = @PartnerNumber 
AND (CreatedOn BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(CreatedOn)";
                            #endregion
                            break;

                        case "DOANHSO":
                            #region --Danh số--
                            zSQL += @"
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Doanh số', 1, 0, 2, '" + color[1] + @"')
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color)
SELECT 
N'Doanh số',
DAY(ReceiptDate),
SUM(AmountCurrencyMain), 
2, '" + color[1] + @"'
FROM FNC_Receipt
WHERE RecordStatus <> 99
AND PartnerNumber = @PartnerNumber 
AND (ReceiptDate BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(ReceiptDate)";
                            #endregion
                            break;

                        case "KHACHHANG":
                            zSQL += @"
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Khách hàng', 1, 0, 3, '" + color[2] + @"')
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color)
SELECT 
N'Khách hàng',
DAY(CreatedOn),
COUNT(CustomerKey), 
3, '" + color[2] + @"'
FROM CRM_Customer 
WHERE RecordStatus <> 99
AND PartnerNumber = @PartnerNumber 
AND (CreatedOn BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(CreatedOn)";
                            break;
                    }
                }

                zSQL += @"
SELECT * FROM 
(
	SELECT Category, DateView AS [Time], [Value] AS Total, Color, [Rank]
	FROM #temp
) X
PIVOT 
( SUM(Total) FOR [Time] IN (" + PivotDate + ")) P ORDER BY [Rank] DROP TABLE #temp";

                string zConnectionString = Helper.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();

                }
                catch (Exception ex)
                {
                    Message = ex.ToString();
                }

            }
            return zTable;
        }

        public static DataTable DB_ChartAIO(string PartnerNumber, string Branch, string Option, string Color, DateTime FromDate, DateTime ToDate, out string Message)
        {
            if (!Branch.Contains(","))
            {
                Branch = "'" + Branch + "'";
            }

            Message = string.Empty;
            DataTable zTable = new DataTable();
            if (Option.Length > 0)
            {
                string PivotDate = "";
                for (var i = FromDate.Date; i.Date <= ToDate.Date; i = i.AddDays(1))
                {
                    PivotDate += "[" + i.Day + "],";
                }
                if (PivotDate.Contains(","))
                {
                    PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
                }

                string zSQL = @"
CREATE TABLE #temp
(
	Category NVARCHAR(500) ,
	DateView INT,
	[Value] MONEY,
	[Rank] INT,
    Color NVARCHAR(50)
)";
                string[] temp = Option.Split(',');
                string[] color = Color.Split(',');
                foreach (string s in temp)
                {
                    switch (s)
                    {
                        case "DONHANG":
                            #region --Đơn hàng--
                            zSQL += @"
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Số công việc', 1, 0, 1, '" + color[0] + @"')
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color)
SELECT 
N'Số công việc',
DAY(CreatedOn),
COUNT(TaskKey), 
1, '" + color[0] + @"'
FROM CRM_Task
WHERE RecordStatus <> 99
AND BranchKey IN (" + Branch + @")
AND PartnerNumber = @PartnerNumber 
AND (CreatedOn BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(CreatedOn)";
                            #endregion
                            break;

                        case "DOANHSO":
                            #region --Danh số--
                            zSQL += @"
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Doanh số', 1, 0, 2, '" + color[1] + @"')
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color)
SELECT 
N'Doanh số',
DAY(ReceiptDate),
SUM(AmountCurrencyMain), 
2, '" + color[1] + @"'
FROM FNC_Receipt
WHERE RecordStatus <> 99
AND BranchKey IN (" + Branch + @")
AND PartnerNumber = @PartnerNumber 
AND (ReceiptDate BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(ReceiptDate)";
                            #endregion
                            break;

                        case "KHACHHANG":
                            zSQL += @"
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color) VALUES (N'Khách hàng', 1, 0, 3, '" + color[2] + @"')
INSERT INTO #temp (Category, DateView, [Value], [Rank], Color)
SELECT 
N'Khách hàng',
DAY(CreatedOn),
COUNT(CustomerKey), 
3, '" + color[2] + @"'
FROM CRM_Customer 
WHERE RecordStatus <> 99
AND BranchKey IN (" + Branch + @")
AND PartnerNumber = @PartnerNumber 
AND (CreatedOn BETWEEN @FromDate AND @ToDate)
GROUP BY DAY(CreatedOn)";
                            break;
                    }
                }

                zSQL += @"
SELECT * FROM 
(
	SELECT Category, DateView AS [Time], [Value] AS Total, Color, [Rank]
	FROM #temp
) X
PIVOT 
( SUM(Total) FOR [Time] IN (" + PivotDate + ")) P ORDER BY [Rank] DROP TABLE #temp";

                string zConnectionString = Helper.ConnectionString;
                try
                {
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                    SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                    zAdapter.Fill(zTable);
                    zCommand.Dispose();
                    zConnect.Close();

                }
                catch (Exception ex)
                {
                    Message = ex.ToString();
                }

            }
            return zTable;
        }
    }
}
