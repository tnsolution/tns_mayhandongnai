﻿using System;
namespace Desktop
{
    public class Group_Roles_Model
    {
        #region [ Field Name ]
        private int _Slug = 0;
        private int _AutoKey = 0;
        private string _GroupKey = "";
        private string _RoleKey = "";
        private string _RouteName = "";
        private string _ActionName = "";
        private string _ControllerName = "";
        private int _Level = 0;
        private bool _RoleRead;
        private bool _RoleAdd;
        private bool _RoleEdit;
        private bool _RoleDel;
        private bool _RoleApprove;
        private string _RoleName = "";
        private string _RoleID = "";
        private string _RoleURL = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string GroupKey
        {
            get { return _GroupKey; }
            set { _GroupKey = value; }
        }
        public string RoleKey
        {
            get { return _RoleKey; }
            set { _RoleKey = value; }
        }
        public string RouteName
        {
            get { return _RouteName; }
            set { _RouteName = value; }
        }
        public string ActionName
        {
            get { return _ActionName; }
            set { _ActionName = value; }
        }
        public string ControllerName
        {
            get { return _ControllerName; }
            set { _ControllerName = value; }
        }
        public int Level
        {
            get { return _Level; }
            set { _Level = value; }
        }
        public bool RoleRead
        {
            get { return _RoleRead; }
            set { _RoleRead = value; }
        }
        public bool RoleAdd
        {
            get { return _RoleAdd; }
            set { _RoleAdd = value; }
        }
        public bool RoleEdit
        {
            get { return _RoleEdit; }
            set { _RoleEdit = value; }
        }
        public bool RoleDel
        {
            get { return _RoleDel; }
            set { _RoleDel = value; }
        }
        public bool RoleApprove
        {
            get { return _RoleApprove; }
            set { _RoleApprove = value; }
        }
        public string RoleName
        {
            get { return _RoleName; }
            set { _RoleName = value; }
        }
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public string RoleURL
        {
            get { return _RoleURL; }
            set { _RoleURL = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }

        public int Slug
        {
            get
            {
                return _Slug;
            }

            set
            {
                _Slug = value;
            }
        }
        #endregion
    }
}
