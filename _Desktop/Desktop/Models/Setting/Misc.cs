﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;


namespace Desktop
{
    public class Misc
    {
        public static string ApplySQL(string SQL, out string Message)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------

            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(SQL, zConnect);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Message = string.Empty;
            }
            catch (Exception Err)
            {
                Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

    }
}
