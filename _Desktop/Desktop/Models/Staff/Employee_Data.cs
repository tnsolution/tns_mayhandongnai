﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;


namespace Desktop
{
    public class Employee_Data
    {
        public static List<Employee_Model> List(string PartnerNumber, string Employee)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, B.[Rank] 
FROM HRM_Employee A 
LEFT JOIN HRM_ListPosition B ON A.PositionKey = B.PositionKey 
LEFT JOIN HRM_Department C ON A.DepartmentKey = C.DepartmentKey
WHERE 
A.RecordStatus != 99 
AND A.PartnerNumber = @PartnerNumber";
            if (Employee.Length >= 36)
            {
                zSQL += " AND A.EmployeeKey = @Employee";
            }
            zSQL += " ORDER BY A.EmployeeID, C.[Rank] ASC, B.[Rank] ASC";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Employee", SqlDbType.NVarChar).Value = Employee;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            List<Employee_Model> zList = new List<Employee_Model>();

            foreach (DataRow r in zTable.Rows)
            {
                DateTime zBirthday = DateTime.MinValue;
                DateTime.TryParse(r["Birthday"].ToString(), out zBirthday);

                DateTime zStartingDate = DateTime.MinValue;
                DateTime.TryParse(r["StartingDate"].ToString(), out zStartingDate);

                zList.Add(new Employee_Model()
                {
                    EmployeeKey = r["EmployeeKey"].ToString(),
                    EmployeeID = r["EmployeeID"].ToString(),
                    PositionKey = r["PositionKey"].ToInt(),
                    StartingDate = zStartingDate,
                    PositionName = r["PositionName"].ToString(),
                    FirstName = r["FirstName"].ToString(),
                    LastName = r["LastName"].ToString(),
                    Email = r["Email"].ToString(),
                    MobiPhone = r["MobiPhone"].ToString(),
                    Birthday = zBirthday,
                    Gender = r["Gender"].ToInt(),
                    AddressRegister = r["AddressRegister"].ToString(),
                    PassportNumber = r["PassportNumber"].ToString(),
                    OrganizationName = r["OrganizationName"].ToString(),
                    DepartmentName = r["DepartmentName"].ToString(),
                });
            }

            return zList;
        }
        public static string AutoID(string Prefix, string PartnerNumber)
        {
            string zResult = "";
            string zSQL = @"SELECT dbo.Auto_EmployeeID(@Prefix, @PartnerNumber)";

            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Prefix", SqlDbType.NVarChar).Value = Prefix;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zResult;
        }
    }
}