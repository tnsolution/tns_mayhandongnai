﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Desktop.Models
{
    public class TN_Message
    {
        public string Content { get; set; } = "";
        public int Type { get; set; } = 0;
    }
}
