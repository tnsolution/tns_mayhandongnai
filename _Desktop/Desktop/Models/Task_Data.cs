﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;


namespace Desktop
{
    public class Task_Data
    {
        public static List<Task_Model> Monitor(string PartnerNumber, out string Message, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
A.TaskKey, A.BranchKey, A.OwnerKey,
A.CreatedOn, A.CustomerName, A.CustomerPhone, 
A.GroupName,
CASE A.OptionTask 
	WHEN 1 THEN N'Khách vãng lai' 
	WHEN 2 THEN N'Máy mang về'
	WHEN 3 THEN N'Máy chỉ định cửa hàng'
	ELSE A.Style
END AS [Option],
ISNULL(dbo.Mayhan_LayTenChiNhanh(A.BranchKey), '') AS BranchName,
A.TaskID, A.TaskContent, A.OwnerName,
A.TaskAmount,
A.CategoryName, 
CASE CodeLine 
	WHEN 'Y' THEN N'TRUE' 
	WHEN 'N' THEN N'FALSE' 
END AS [Status]
FROM CRM_Task A 
WHERE 
A.RecordStatus <> 99
AND A.Publish = 1
AND A.PartnerNumber = @PartnerNumber
AND A.CreatedOn BETWEEN @FromDate AND @ToDate
ORDER BY CreatedOn DESC, CustomerName";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            var zList = new List<Task_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Task_Model()
                {
                    TaskKey = r["TaskKey"].ToString(),
                    TaskID = r["TaskID"].ToString(),
                    TaskContent = r["TaskContent"].ToString(),
                    BranchKey = r["BranchKey"].ToString(),
                    OwnerKey = r["OwnerKey"].ToString(),
                    CustomerName = r["CustomerName"].ToString(),
                    CustomerPhone = r["CustomerPhone"].ToString(),
                    GroupName = r["GroupName"].ToString(),
                    OptionName = r["Option"].ToString(),
                    BranchName = r["BranchName"].ToString(),
                    OwnerName = r["OwnerName"].ToString(),
                    CategoryName = r["CategoryName"].ToString(),
                    StatusName = r["Status"].ToString(),
                    TaskAmount = Convert.ToDouble(r["TaskAmount"]),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                });
            }

            return zList;
        }

        public static List<Task_Model> Monitor(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
A.TaskKey, A.BranchKey, A.OwnerKey,
A.CreatedOn, A.CustomerName, A.CustomerPhone, 
A.GroupName,
CASE A.OptionTask 
	WHEN 1 THEN N'Khách vãng lai' 
	WHEN 2 THEN N'Máy mang về'
	WHEN 3 THEN N'Máy chỉ định cửa hàng'
	ELSE A.Style
END AS [Option],
ISNULL(dbo.Mayhan_LayTenChiNhanh(A.BranchKey), '') AS BranchName,
A.TaskID, A.TaskContent, A.OwnerName,
A.TaskAmount,
A.CategoryName, 
CASE CodeLine 
	WHEN 'Y' THEN N'TRUE' 
	WHEN 'N' THEN N'FALSE' 
END AS [Status]
FROM CRM_Task A 
WHERE 
A.RecordStatus <> 99
AND A.PartnerNumber = @PartnerNumber
ORDER BY CreatedOn DESC, CustomerName";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            var zList = new List<Task_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Task_Model()
                {
                    TaskKey = r["TaskKey"].ToString(),
                    TaskID = r["TaskID"].ToString(),
                    TaskContent = r["TaskContent"].ToString(),
                    BranchKey = r["BranchKey"].ToString(),
                    OwnerKey = r["OwnerKey"].ToString(),
                    CustomerName = r["CustomerName"].ToString(),
                    CustomerPhone = r["CustomerPhone"].ToString(),
                    GroupName = r["GroupName"].ToString(),
                    OptionName = r["Option"].ToString(),
                    BranchName = r["BranchName"].ToString(),
                    OwnerName = r["OwnerName"].ToString(),
                    CategoryName = r["CategoryName"].ToString(),
                    StatusName = r["Status"].ToString(),
                    TaskAmount = Convert.ToDouble(r["TaskAmount"]),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                });
            }

            return zList;
        }

        public static string TaskCategoryName(string PartnerNumber, int CategoryKey)
        {
            string zResult = "";
            string zSQL = @"SELECT CategoryNameVN FROM CRM_Task_Category WHERE CategoryKey = @CategoryKey AND RecordStatus <> 99";

            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.NVarChar).Value = CategoryKey;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zResult;
        }
        public static string TaskCategoryParent(string PartnerNumber, int CategoryKey)
        {
            string zResult = "";
            string zSQL = @"SELECT Parent FROM CRM_Task_Category WHERE CategoryKey = @CategoryKey AND RecordStatus <> 99";

            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.NVarChar).Value = CategoryKey;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zResult;
        }
        public static string AutoID(string Prefix, string PartnerNumber)
        {
            string zResult = "";
            string zSQL = @"SELECT dbo.Auto_TaskID(@Prefix, @PartnerNumber)";

            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Prefix", SqlDbType.NVarChar).Value = Prefix;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zResult;
        }
        public static List<Task_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM CRM_Task WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Task_Model> zList = new List<Task_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Task_Model()
                {
                    TaskKey = r["TaskKey"].ToString(),
                    TaskFile = r["TaskFile"].ToString(),
                    Subject = r["Subject"].ToString(),
                    TaskContent = r["TaskContent"].ToString(),
                    StartDate = (r["StartDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["StartDate"]),
                    DueDate = (r["DueDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DueDate"]),
                    Duration = r["Duration"].ToFloat(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    PriorityKey = r["PriorityKey"].ToInt(),
                    PriorityName = r["PriorityName"].ToString(),
                    CompleteRate = r["CompleteRate"].ToInt(),
                    CompleteDate = (r["CompleteDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CompleteDate"]),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    GroupKey = r["GroupKey"].ToInt(),
                    GroupName = r["GroupName"].ToString(),
                    ParentKey = r["ParentKey"].ToString(),
                    CustomerKey = r["CustomerKey"].ToString(),
                    CustomerID = r["CustomerID"].ToString(),
                    CustomerName = r["CustomerName"].ToString(),
                    CustomerPhone = r["CustomerPhone"].ToString(),
                    CustomerAddress = r["CustomerAddress"].ToString(),
                    ContractKey = r["ContractKey"].ToString(),
                    ContractName = r["ContractName"].ToString(),
                    Reminder = (r["Reminder"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["Reminder"]),
                    ApproveBy = r["ApproveBy"].ToString(),
                    ApproveName = r["ApproveName"].ToString(),
                    OwnerBy = r["OwnerBy"].ToString(),
                    OwnerName = r["OwnerName"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    CodeLine = r["CodeLine"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToString(),
                    BranchKey = r["BranchKey"].ToString(),
                    OrganizationID = r["OrganizationID"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static List<Task_Model> List(string PartnerNumber, int CategoryKey, string BranchKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM CRM_Task WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND CategoryKey = @CategoryKey";

            if (BranchKey != string.Empty)
            {
                zSQL += " AND BranchKey IN (" + BranchKey + ")";
            }

            zSQL += " ORDER BY CreatedOn ASC";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Task_Model> zList = new List<Task_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Task_Model()
                {
                    TaskKey = r["TaskKey"].ToString(),
                    TaskID = r["TaskID"].ToString(),
                    TaskFile = r["TaskFile"].ToString(),
                    Subject = r["Subject"].ToString(),
                    TaskContent = r["TaskContent"].ToString(),
                    StartDate = (r["StartDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["StartDate"]),
                    DueDate = (r["DueDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DueDate"]),
                    Duration = r["Duration"].ToFloat(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    PriorityKey = r["PriorityKey"].ToInt(),
                    PriorityName = r["PriorityName"].ToString(),
                    CompleteRate = r["CompleteRate"].ToInt(),
                    CompleteDate = (r["CompleteDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CompleteDate"]),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    GroupKey = r["GroupKey"].ToInt(),
                    GroupName = r["GroupName"].ToString(),
                    ParentKey = r["ParentKey"].ToString(),
                    CustomerKey = r["CustomerKey"].ToString(),
                    CustomerID = r["CustomerID"].ToString(),
                    CustomerName = r["CustomerName"].ToString(),
                    CustomerPhone = r["CustomerPhone"].ToString(),
                    CustomerAddress = r["CustomerAddress"].ToString(),
                    ContractKey = r["ContractKey"].ToString(),
                    ContractName = r["ContractName"].ToString(),
                    Reminder = (r["Reminder"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["Reminder"]),
                    ApproveBy = r["ApproveBy"].ToString(),
                    ApproveName = r["ApproveName"].ToString(),
                    OwnerBy = r["OwnerBy"].ToString(),
                    OwnerName = r["OwnerName"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    CodeLine = r["CodeLine"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToString(),
                    BranchKey = r["BranchKey"].ToString(),
                    OrganizationID = r["OrganizationID"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static List<Task_Model> Search(string PartnerNumber, int CategoryKey, string Name, string Phone, string BranchKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM CRM_Task WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND CategoryKey = @CategoryKey";

            if (BranchKey != string.Empty)
            {
                zSQL += " AND BranchKey IN (" + BranchKey + ")";
            }

            if (Name != string.Empty)
            {
                zSQL += " AND CustomerName LIKE @Name";
            }

            if (Phone != string.Empty)
            {
                zSQL += " AND CustomerPhone = @Phone";
            }

            zSQL += " ORDER BY CreatedOn ASC";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;

                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = Phone;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Task_Model> zList = new List<Task_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Task_Model()
                {
                    TaskKey = r["TaskKey"].ToString(),
                    TaskID = r["TaskID"].ToString(),
                    TaskFile = r["TaskFile"].ToString(),
                    Subject = r["Subject"].ToString(),
                    TaskContent = r["TaskContent"].ToString(),
                    StartDate = (r["StartDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["StartDate"]),
                    DueDate = (r["DueDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DueDate"]),
                    Duration = r["Duration"].ToFloat(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    PriorityKey = r["PriorityKey"].ToInt(),
                    PriorityName = r["PriorityName"].ToString(),
                    CompleteRate = r["CompleteRate"].ToInt(),
                    CompleteDate = (r["CompleteDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CompleteDate"]),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    GroupKey = r["GroupKey"].ToInt(),
                    GroupName = r["GroupName"].ToString(),
                    ParentKey = r["ParentKey"].ToString(),
                    CustomerKey = r["CustomerKey"].ToString(),
                    CustomerID = r["CustomerID"].ToString(),
                    CustomerName = r["CustomerName"].ToString(),
                    CustomerPhone = r["CustomerPhone"].ToString(),
                    CustomerAddress = r["CustomerAddress"].ToString(),
                    ContractKey = r["ContractKey"].ToString(),
                    ContractName = r["ContractName"].ToString(),
                    Reminder = (r["Reminder"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["Reminder"]),
                    ApproveBy = r["ApproveBy"].ToString(),
                    ApproveName = r["ApproveName"].ToString(),
                    OwnerBy = r["OwnerBy"].ToString(),
                    OwnerName = r["OwnerName"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    CodeLine = r["CodeLine"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToString(),
                    BranchKey = r["BranchKey"].ToString(),
                    OrganizationID = r["OrganizationID"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static List<Task_Model> Tracking(string PartnerNumber, int Group, string Employee, string GuestName, string GuestPhone, string Category, out string Message)
        {
            string zSQL = @"SELECT 
A.*, B.BranchName
FROM CRM_Task A
LEFT JOIN HRM_Branch B ON A.BranchKey = CAST(B.BranchKey AS NVARCHAR(50))
WHERE 
A.RecordStatus <> 99 
AND B.RecordStatus <> 99
AND A.PartnerNumber = @PartnerNumber
AND B.PartnerNumber = @PartnerNumber";

            if (Category != string.Empty)
            {
                zSQL += " AND A.CategoryKey IN (" + Category + ")";
            }
            if (Group != 0)
            {
                zSQL += " AND A.GroupKey = @GroupKey";
            }
            if (Employee != string.Empty)
            {
                zSQL += " AND dbo.Mayhan_GetUserKey_ByEmployee(@Employee)";
            }
            if (GuestName != string.Empty)
            {
                zSQL += " AND A.CustomerName LIKE @GuestName";
            }
            if (GuestPhone != string.Empty)
            {
                zSQL += " AND A.CustomerPhone = @GuestPhone";
            }

            zSQL += "ORDER BY A.CreatedOn DESC";

            DataTable zTable = new DataTable();
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Employee", SqlDbType.NVarChar).Value = Employee;
                zCommand.Parameters.Add("@GuestName", SqlDbType.NVarChar).Value = "%" + GuestName + "%";
                zCommand.Parameters.Add("@GuestPhone", SqlDbType.NVarChar).Value = GuestPhone;
                zCommand.Parameters.Add("@GroupKey", SqlDbType.NVarChar).Value = Group;

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<Task_Model> zList = new List<Task_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Task_Model()
                {
                    TaskKey = r["TaskKey"].ToString(),
                    TaskID = r["TaskID"].ToString(),
                    TaskFile = r["TaskFile"].ToString(),
                    Subject = r["Subject"].ToString(),
                    TaskContent = r["TaskContent"].ToString(),
                    StartDate = (r["StartDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["StartDate"]),
                    DueDate = (r["DueDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DueDate"]),
                    Duration = r["Duration"].ToFloat(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    PriorityKey = r["PriorityKey"].ToInt(),
                    PriorityName = r["PriorityName"].ToString(),
                    CompleteRate = r["CompleteRate"].ToInt(),
                    CompleteDate = (r["CompleteDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CompleteDate"]),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    GroupKey = r["GroupKey"].ToInt(),
                    GroupName = r["GroupName"].ToString(),
                    ParentKey = r["ParentKey"].ToString(),
                    CustomerKey = r["CustomerKey"].ToString(),
                    CustomerID = r["CustomerID"].ToString(),
                    CustomerName = r["CustomerName"].ToString(),
                    CustomerPhone = r["CustomerPhone"].ToString(),
                    CustomerAddress = r["CustomerAddress"].ToString(),
                    ContractKey = r["ContractKey"].ToString(),
                    ContractName = r["ContractName"].ToString(),
                    Reminder = (r["Reminder"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["Reminder"]),
                    ApproveBy = r["ApproveBy"].ToString(),
                    ApproveName = r["ApproveName"].ToString(),
                    OwnerBy = r["OwnerBy"].ToString(),
                    OwnerName = r["OwnerName"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    CodeLine = r["CodeLine"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToString(),
                    BranchKey = r["BranchKey"].ToString(),
                    BranchName = r["BranchName"].ToString(),
                    OrganizationID = r["OrganizationID"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }

            return zList;
        }
        public static List<Task_Model> Tracking(string PartnerNumber, string Branch, int Group, string Employee, string GuestName, string GuestPhone, string Category, DateTime FromDate, DateTime ToDate, out string Message)
        {
            string zSQL = @"SELECT 
A.*, B.BranchName
FROM CRM_Task A
LEFT JOIN HRM_Branch B ON A.BranchKey = CAST(B.BranchKey AS NVARCHAR(50))
WHERE 
A.RecordStatus <> 99 
AND B.RecordStatus <> 99
AND A.PartnerNumber = @PartnerNumber
AND B.PartnerNumber = @PartnerNumber";
            if (Branch != string.Empty && Branch != "0")
            {
                if (Branch.Contains(","))
                {
                    zSQL += "AND A.BranchKey IN(" + Branch + @")";
                }
                else
                {
                    zSQL += "AND A.BranchKey = '" + Branch + @"'";
                }
            }
            if (Category != string.Empty)
            {
                zSQL += " AND A.CategoryKey IN (" + Category + ")";
            }
            if (Group != 0)
            {
                zSQL += " AND A.GroupKey = @GroupKey";
            }
            if (Employee != string.Empty)
            {
                zSQL += " AND dbo.Mayhan_GetUserKey_ByEmployee(@Employee)";
            }
            if (GuestName != string.Empty)
            {
                zSQL += " AND A.CustomerName LIKE @GuestName";
            }
            if (GuestPhone != string.Empty)
            {
                zSQL += " AND A.CustomerPhone = @GuestPhone";
            }
            if (FromDate != DateTime.MinValue &&
               ToDate != DateTime.MinValue)
            {
                zSQL += " AND A.CreatedOn BETWEEN @FromDate AND @ToDate";
            }

            zSQL += "ORDER BY A.CreatedOn DESC";

            DataTable zTable = new DataTable();
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Employee", SqlDbType.NVarChar).Value = Employee;
                zCommand.Parameters.Add("@GuestName", SqlDbType.NVarChar).Value = "%" + GuestName + "%";
                zCommand.Parameters.Add("@GuestPhone", SqlDbType.NVarChar).Value = GuestPhone;
                zCommand.Parameters.Add("@GroupKey", SqlDbType.NVarChar).Value = Group;
                if (FromDate != DateTime.MinValue &&
                   ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<Task_Model> zList = new List<Task_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Task_Model()
                {
                    TaskKey = r["TaskKey"].ToString(),
                    TaskID = r["TaskID"].ToString(),
                    TaskFile = r["TaskFile"].ToString(),
                    Subject = r["Subject"].ToString(),
                    TaskContent = r["TaskContent"].ToString(),
                    StartDate = (r["StartDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["StartDate"]),
                    DueDate = (r["DueDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DueDate"]),
                    Duration = r["Duration"].ToFloat(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    PriorityKey = r["PriorityKey"].ToInt(),
                    PriorityName = r["PriorityName"].ToString(),
                    CompleteRate = r["CompleteRate"].ToInt(),
                    CompleteDate = (r["CompleteDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CompleteDate"]),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    GroupKey = r["GroupKey"].ToInt(),
                    GroupName = r["GroupName"].ToString(),
                    ParentKey = r["ParentKey"].ToString(),
                    CustomerKey = r["CustomerKey"].ToString(),
                    CustomerID = r["CustomerID"].ToString(),
                    CustomerName = r["CustomerName"].ToString(),
                    CustomerPhone = r["CustomerPhone"].ToString(),
                    CustomerAddress = r["CustomerAddress"].ToString(),
                    ContractKey = r["ContractKey"].ToString(),
                    ContractName = r["ContractName"].ToString(),
                    Reminder = (r["Reminder"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["Reminder"]),
                    ApproveBy = r["ApproveBy"].ToString(),
                    ApproveName = r["ApproveName"].ToString(),
                    OwnerBy = r["OwnerBy"].ToString(),
                    OwnerName = r["OwnerName"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    CodeLine = r["CodeLine"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToString(),
                    BranchKey = r["BranchKey"].ToString(),
                    BranchName = r["BranchName"].ToString(),
                    OrganizationID = r["OrganizationID"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }

            return zList;
        }
        public static List<Task_Model> Tracking(string PartnerNumber, int Group, string Employee, string GuestName, string GuestPhone, string Category, DateTime FromDate, DateTime ToDate, out string Message)
        {
            string zSQL = @"SELECT 
A.*, ISNULL(dbo.Mayhan_LayTenChiNhanh(A.BranchKey), '') AS BranchName
FROM CRM_Task A
WHERE 
A.RecordStatus <> 99 
AND A.PartnerNumber = @PartnerNumber";

            if (Category != string.Empty)
            {
                zSQL += " AND A.CategoryKey IN (" + Category + ")";
            }
            if (Group != 0)
            {
                zSQL += " AND A.GroupKey = @GroupKey";
            }
            if (Employee != string.Empty)
            {
                zSQL += " AND A.OwnerBy = dbo.Mayhan_GetUserKey_ByEmployee(@Employee) ";
            }
            if (GuestName != string.Empty)
            {
                zSQL += " AND A.CustomerName LIKE @GuestName";
            }
            if (GuestPhone != string.Empty)
            {
                zSQL += " AND A.CustomerPhone = @GuestPhone";
            }
            if (FromDate != DateTime.MinValue &&
                ToDate != DateTime.MinValue)
            {
                zSQL += " AND A.CreatedOn BETWEEN @FromDate AND @ToDate";
            }

            zSQL += " ORDER BY A.CreatedOn DESC";

            DataTable zTable = new DataTable();
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Employee", SqlDbType.NVarChar).Value = Employee;
                zCommand.Parameters.Add("@GuestName", SqlDbType.NVarChar).Value = "%" + GuestName + "%";
                zCommand.Parameters.Add("@GuestPhone", SqlDbType.NVarChar).Value = GuestPhone;
                zCommand.Parameters.Add("@GroupKey", SqlDbType.NVarChar).Value = Group;

                if (FromDate != DateTime.MinValue &&
                    ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<Task_Model> zList = new List<Task_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Task_Model()
                {
                    TaskKey = r["TaskKey"].ToString(),
                    TaskID = r["TaskID"].ToString(),
                    TaskFile = r["TaskFile"].ToString(),
                    Subject = r["Subject"].ToString(),
                    TaskContent = r["TaskContent"].ToString(),
                    StartDate = (r["StartDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["StartDate"]),
                    DueDate = (r["DueDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DueDate"]),
                    Duration = r["Duration"].ToFloat(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    PriorityKey = r["PriorityKey"].ToInt(),
                    PriorityName = r["PriorityName"].ToString(),
                    CompleteRate = r["CompleteRate"].ToInt(),
                    CompleteDate = (r["CompleteDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CompleteDate"]),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    GroupKey = r["GroupKey"].ToInt(),
                    GroupName = r["GroupName"].ToString(),
                    ParentKey = r["ParentKey"].ToString(),
                    CustomerKey = r["CustomerKey"].ToString(),
                    CustomerID = r["CustomerID"].ToString(),
                    CustomerName = r["CustomerName"].ToString(),
                    CustomerPhone = r["CustomerPhone"].ToString(),
                    CustomerAddress = r["CustomerAddress"].ToString(),
                    ContractKey = r["ContractKey"].ToString(),
                    ContractName = r["ContractName"].ToString(),
                    Reminder = (r["Reminder"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["Reminder"]),
                    ApproveBy = r["ApproveBy"].ToString(),
                    ApproveName = r["ApproveName"].ToString(),
                    OwnerBy = r["OwnerBy"].ToString(),
                    OwnerName = r["OwnerName"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    CodeLine = r["CodeLine"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToString(),
                    BranchKey = r["BranchKey"].ToString(),
                    BranchName = r["BranchName"].ToString(),
                    OrganizationID = r["OrganizationID"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }

            return zList;
        }
        public static int Count(string PartnerNumber, string Category)
        {
            int zResult = 0;
            string zSQL = @"SELECT COUNT(TaskKey) FROM CRM_Task WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (Category != string.Empty)
            {
                zSQL += " AND CategoryKey IN (" + Category + ")";
            }
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }

            return zResult;
        }
        public static List<Task_Log_Model> Log(string TaskKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM CRM_Task_Log WHERE TaskKey = @TaskKey ";
            string zConnectionString = Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TaskKey", SqlDbType.NVarChar).Value = TaskKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Task_Log_Model> zList = new List<Task_Log_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Task_Log_Model
                {
                    CategoryKey = r["CategoryKey"].ToString(),
                    CategoryName = r["CategoryName"].ToString(),
                    StatusKey = r["StatusKey"].ToString(),
                    StatusName = r["StatusName"].ToString(),
                    Description = r["Description"].ToString(),
                    CreatedOn = Convert.ToDateTime(r["CreatedOn"]),
                });
            }
            return zList;
        }
    }
}
