﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Desktop
{
    public class Task_Info
    {

        public Task_Model Task = new Task_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Task_Info()
        {
            Task.TaskKey = Guid.NewGuid().ToString();
        }
        public Task_Info(string TaskKey)
        {
            string zSQL = "SELECT * FROM CRM_Task WHERE TaskKey = @TaskKey AND RecordStatus != 99 ";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TaskKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(TaskKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["TaskAmount"] != DBNull.Value)
                    {
                        Task.TaskAmount = double.Parse(zReader["TaskAmount"].ToString());
                    }

                    Task.TaskKey = zReader["TaskKey"].ToString();
                    Task.TaskID = zReader["TaskID"].ToString();
                    Task.TaskFile = zReader["TaskFile"].ToString();
                    Task.Subject = zReader["Subject"].ToString();
                    Task.TaskContent = zReader["TaskContent"].ToString();
                    if (zReader["StartDate"] != DBNull.Value)
                    {
                        Task.StartDate = (DateTime)zReader["StartDate"];
                    }

                    if (zReader["DueDate"] != DBNull.Value)
                    {
                        Task.DueDate = (DateTime)zReader["DueDate"];
                    }

                    if (zReader["Duration"] != DBNull.Value)
                    {
                        Task.Duration = float.Parse(zReader["Duration"].ToString());
                    }

                    if (zReader["StatusKey"] != DBNull.Value)
                    {
                        Task.StatusKey = int.Parse(zReader["StatusKey"].ToString());
                    }

                    Task.StatusName = zReader["StatusName"].ToString();
                    if (zReader["PriorityKey"] != DBNull.Value)
                    {
                        Task.PriorityKey = int.Parse(zReader["PriorityKey"].ToString());
                    }

                    Task.PriorityName = zReader["PriorityName"].ToString();
                    if (zReader["CompleteRate"] != DBNull.Value)
                    {
                        Task.CompleteRate = int.Parse(zReader["CompleteRate"].ToString());
                    }

                    if (zReader["CompleteDate"] != DBNull.Value)
                    {
                        Task.CompleteDate = (DateTime)zReader["CompleteDate"];
                    }

                    if (zReader["CategoryKey"] != DBNull.Value)
                    {
                        Task.CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    }

                    Task.CategoryName = zReader["CategoryName"].ToString();
                    if (zReader["GroupKey"] != DBNull.Value)
                    {
                        Task.GroupKey = int.Parse(zReader["GroupKey"].ToString());
                    }

                    Task.GroupName = zReader["GroupName"].ToString();
                    Task.ParentKey = zReader["ParentKey"].ToString();
                    Task.CustomerKey = zReader["CustomerKey"].ToString();
                    Task.CustomerID = zReader["CustomerID"].ToString();
                    Task.CustomerName = zReader["CustomerName"].ToString();
                    Task.CustomerPhone = zReader["CustomerPhone"].ToString();
                    Task.CustomerAddress = zReader["CustomerAddress"].ToString();
                    Task.ContractKey = zReader["ContractKey"].ToString();
                    Task.ContractName = zReader["ContractName"].ToString();
                    if (zReader["Reminder"] != DBNull.Value)
                    {
                        Task.Reminder = (DateTime)zReader["Reminder"];
                    }

                    Task.ApproveBy = zReader["ApproveBy"].ToString();
                    Task.ApproveName = zReader["ApproveName"].ToString();
                    Task.OwnerBy = zReader["OwnerBy"].ToString();
                    Task.OwnerName = zReader["OwnerName"].ToString();
                    Task.Style = zReader["Style"].ToString();
                    Task.Class = zReader["Class"].ToString();
                    Task.CodeLine = zReader["CodeLine"].ToString();
                    if (zReader["Slug"] != DBNull.Value)
                    {
                        Task.Slug = int.Parse(zReader["Slug"].ToString());
                    }

                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Task.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["Publish"] != DBNull.Value)
                    {
                        Task.Publish = (bool)zReader["Publish"];
                    }

                    Task.PartnerNumber = zReader["PartnerNumber"].ToString();
                    Task.DepartmentKey = zReader["DepartmentKey"].ToString();
                    Task.BranchKey = zReader["BranchKey"].ToString();
                    Task.OrganizationID = zReader["OrganizationID"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Task.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Task.CreatedBy = zReader["CreatedBy"].ToString();
                    Task.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Task.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Task.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Task.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO CRM_Task ("
         + " TaskID , TaskFile , Subject , TaskContent , StartDate , DueDate , Duration , StatusKey , StatusName , PriorityKey , PriorityName , CompleteRate , CompleteDate , CategoryKey , CategoryName , GroupKey , GroupName , ParentKey , CustomerKey , CustomerID , CustomerName , CustomerPhone , CustomerAddress , ContractKey , ContractName , Reminder , ApproveBy , ApproveName , OwnerBy , OwnerName , Style , Class , CodeLine , Slug , RecordStatus , Publish , PartnerNumber , DepartmentKey , BranchKey , OrganizationID , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @TaskID , @TaskFile , @Subject , @TaskContent , @StartDate , @DueDate , @Duration , @StatusKey , @StatusName , @PriorityKey , @PriorityName , @CompleteRate , @CompleteDate , @CategoryKey , @CategoryName , @GroupKey , @GroupName , @ParentKey , @CustomerKey , @CustomerID , @CustomerName , @CustomerPhone , @CustomerAddress , @ContractKey , @ContractName , @Reminder , @ApproveBy , @ApproveName , @OwnerBy , @OwnerName , @Style , @Class , @CodeLine , @Slug , @RecordStatus , @Publish , @PartnerNumber , @DepartmentKey , @BranchKey , @OrganizationID , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TaskID", SqlDbType.NVarChar).Value = Task.TaskID;
                zCommand.Parameters.Add("@TaskFile", SqlDbType.NVarChar).Value = Task.TaskFile;
                zCommand.Parameters.Add("@Subject", SqlDbType.NVarChar).Value = Task.Subject;
                zCommand.Parameters.Add("@TaskContent", SqlDbType.NVarChar).Value = Task.TaskContent;
                if (Task.StartDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = Task.StartDate;
                }

                if (Task.DueDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = Task.DueDate;
                }

                zCommand.Parameters.Add("@Duration", SqlDbType.Float).Value = Task.Duration;
                zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = Task.StatusKey;
                zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = Task.StatusName;
                zCommand.Parameters.Add("@PriorityKey", SqlDbType.Int).Value = Task.PriorityKey;
                zCommand.Parameters.Add("@PriorityName", SqlDbType.NVarChar).Value = Task.PriorityName;
                zCommand.Parameters.Add("@CompleteRate", SqlDbType.Int).Value = Task.CompleteRate;
                if (Task.CompleteDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@CompleteDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@CompleteDate", SqlDbType.DateTime).Value = Task.CompleteDate;
                }

                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Task.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Task.CategoryName;
                zCommand.Parameters.Add("@GroupKey", SqlDbType.Int).Value = Task.GroupKey;
                zCommand.Parameters.Add("@GroupName", SqlDbType.NVarChar).Value = Task.GroupName;
                if (Task.ParentKey != "" && Task.ParentKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ParentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Task.ParentKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ParentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                if (Task.CustomerKey != "" && Task.CustomerKey.Length == 36)
                {
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Task.CustomerKey);
                }
                else
                {
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = Task.CustomerID;
                zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = Task.CustomerName;
                zCommand.Parameters.Add("@CustomerPhone", SqlDbType.NVarChar).Value = Task.CustomerPhone;
                zCommand.Parameters.Add("@CustomerAddress", SqlDbType.NVarChar).Value = Task.CustomerAddress;
                if (Task.ContractKey != "" && Task.ContractKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Task.ContractKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ContractName", SqlDbType.NVarChar).Value = Task.ContractName;
                if (Task.Reminder == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@Reminder", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@Reminder", SqlDbType.DateTime).Value = Task.Reminder;
                }

                if (Task.ApproveBy != "" && Task.ApproveBy.Length == 36)
                {
                    zCommand.Parameters.Add("@ApproveBy", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Task.ApproveBy);
                }
                else
                {
                    zCommand.Parameters.Add("@ApproveBy", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ApproveName", SqlDbType.NVarChar).Value = Task.ApproveName;
                if (Task.OwnerBy != "" && Task.OwnerBy.Length == 36)
                {
                    zCommand.Parameters.Add("@OwnerBy", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Task.OwnerBy);
                }
                else
                {
                    zCommand.Parameters.Add("@OwnerBy", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@OwnerName", SqlDbType.NVarChar).Value = Task.OwnerName;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Task.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Task.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Task.CodeLine;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Task.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Task.RecordStatus;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Task.Publish;
                if (Task.PartnerNumber != "" && Task.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Task.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Task.DepartmentKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Task.BranchKey;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Task.OrganizationID;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Task.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Task.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Task.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Task.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO CRM_Task("
         + " TaskKey , TaskID , TaskFile , Subject , TaskContent , StartDate , DueDate , Duration , StatusKey , StatusName , PriorityKey , PriorityName , CompleteRate , CompleteDate , CategoryKey , CategoryName , GroupKey , GroupName , ParentKey , CustomerKey , CustomerID , CustomerName , CustomerPhone , CustomerAddress , ContractKey , ContractName , Reminder , ApproveBy , ApproveName , OwnerBy , OwnerName , Style , Class , CodeLine , Slug , RecordStatus , Publish , PartnerNumber , DepartmentKey , BranchKey , OrganizationID , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @TaskKey , @TaskID , @TaskFile , @Subject , @TaskContent , @StartDate , @DueDate , @Duration , @StatusKey , @StatusName , @PriorityKey , @PriorityName , @CompleteRate , @CompleteDate , @CategoryKey , @CategoryName , @GroupKey , @GroupName , @ParentKey , @CustomerKey , @CustomerID , @CustomerName , @CustomerPhone , @CustomerAddress , @ContractKey , @ContractName , @Reminder , @ApproveBy , @ApproveName , @OwnerBy , @OwnerName , @Style , @Class , @CodeLine , @Slug , @RecordStatus , @Publish , @PartnerNumber , @DepartmentKey , @BranchKey , @OrganizationID , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Task.TaskKey != "" && Task.TaskKey.Length == 36)
                {
                    zCommand.Parameters.Add("@TaskKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Task.TaskKey);
                }
                else
                {
                    zCommand.Parameters.Add("@TaskKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@TaskID", SqlDbType.NVarChar).Value = Task.TaskID;
                zCommand.Parameters.Add("@TaskFile", SqlDbType.NVarChar).Value = Task.TaskFile;
                zCommand.Parameters.Add("@Subject", SqlDbType.NVarChar).Value = Task.Subject;
                zCommand.Parameters.Add("@TaskContent", SqlDbType.NVarChar).Value = Task.TaskContent;
                if (Task.StartDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = Task.StartDate;
                }

                if (Task.DueDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = Task.DueDate;
                }

                zCommand.Parameters.Add("@Duration", SqlDbType.Float).Value = Task.Duration;
                zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = Task.StatusKey;
                zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = Task.StatusName;
                zCommand.Parameters.Add("@PriorityKey", SqlDbType.Int).Value = Task.PriorityKey;
                zCommand.Parameters.Add("@PriorityName", SqlDbType.NVarChar).Value = Task.PriorityName;
                zCommand.Parameters.Add("@CompleteRate", SqlDbType.Int).Value = Task.CompleteRate;
                if (Task.CompleteDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@CompleteDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@CompleteDate", SqlDbType.DateTime).Value = Task.CompleteDate;
                }

                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Task.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Task.CategoryName;
                zCommand.Parameters.Add("@GroupKey", SqlDbType.Int).Value = Task.GroupKey;
                zCommand.Parameters.Add("@GroupName", SqlDbType.NVarChar).Value = Task.GroupName;
                if (Task.ParentKey != "" && Task.ParentKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ParentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Task.ParentKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ParentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                if (Task.CustomerKey != "" && Task.CustomerKey.Length == 36)
                {
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Task.CustomerKey);
                }
                else
                {
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = Task.CustomerID;
                zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = Task.CustomerName;
                zCommand.Parameters.Add("@CustomerPhone", SqlDbType.NVarChar).Value = Task.CustomerPhone;
                zCommand.Parameters.Add("@CustomerAddress", SqlDbType.NVarChar).Value = Task.CustomerAddress;
                if (Task.ContractKey != "" && Task.ContractKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Task.ContractKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ContractName", SqlDbType.NVarChar).Value = Task.ContractName;
                if (Task.Reminder == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@Reminder", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@Reminder", SqlDbType.DateTime).Value = Task.Reminder;
                }

                if (Task.ApproveBy != "" && Task.ApproveBy.Length == 36)
                {
                    zCommand.Parameters.Add("@ApproveBy", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Task.ApproveBy);
                }
                else
                {
                    zCommand.Parameters.Add("@ApproveBy", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ApproveName", SqlDbType.NVarChar).Value = Task.ApproveName;
                if (Task.OwnerBy != "" && Task.OwnerBy.Length == 36)
                {
                    zCommand.Parameters.Add("@OwnerBy", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Task.OwnerBy);
                }
                else
                {
                    zCommand.Parameters.Add("@OwnerBy", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@OwnerName", SqlDbType.NVarChar).Value = Task.OwnerName;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Task.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Task.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Task.CodeLine;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Task.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Task.RecordStatus;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Task.Publish;
                if (Task.PartnerNumber != "" && Task.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Task.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Task.DepartmentKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Task.BranchKey;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Task.OrganizationID;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Task.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Task.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Task.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Task.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE CRM_Task SET "

                        + " TaskContent = @TaskContent,"
                        + " PriorityKey = @PriorityKey,"
                        + " PriorityName = @PriorityName,"
                        + " CategoryKey = @CategoryKey,"
                        + " CategoryName = @CategoryName,"
                        + " GroupKey = @GroupKey,"
                        + " GroupName = @GroupName,"
                        + " CustomerKey = @CustomerKey,"
                        + " CustomerID = @CustomerID,"
                        + " CustomerName = @CustomerName,"
                        + " CustomerPhone = @CustomerPhone,"
                        + " CustomerAddress = @CustomerAddress,"
                        + " Slug = @Slug,"
                        + " RecordStatus = @RecordStatus,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " BranchKey = @BranchKey,"
                        + " OrganizationID = @OrganizationID,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE TaskKey = @TaskKey";
            string zResult = "";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Task.TaskKey != "" && Task.TaskKey.Length == 36)
                {
                    zCommand.Parameters.Add("@TaskKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Task.TaskKey);
                }
                else
                {
                    zCommand.Parameters.Add("@TaskKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@TaskID", SqlDbType.NVarChar).Value = Task.TaskID;
                zCommand.Parameters.Add("@TaskFile", SqlDbType.NVarChar).Value = Task.TaskFile;
                zCommand.Parameters.Add("@Subject", SqlDbType.NVarChar).Value = Task.Subject;
                zCommand.Parameters.Add("@TaskContent", SqlDbType.NVarChar).Value = Task.TaskContent;
                if (Task.StartDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = Task.StartDate;
                }

                if (Task.DueDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = Task.DueDate;
                }

                zCommand.Parameters.Add("@Duration", SqlDbType.Float).Value = Task.Duration;
                zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = Task.StatusKey;
                zCommand.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = Task.StatusName;
                zCommand.Parameters.Add("@PriorityKey", SqlDbType.Int).Value = Task.PriorityKey;
                zCommand.Parameters.Add("@PriorityName", SqlDbType.NVarChar).Value = Task.PriorityName;
                zCommand.Parameters.Add("@CompleteRate", SqlDbType.Int).Value = Task.CompleteRate;
                if (Task.CompleteDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@CompleteDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@CompleteDate", SqlDbType.DateTime).Value = Task.CompleteDate;
                }

                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Task.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Task.CategoryName;
                zCommand.Parameters.Add("@GroupKey", SqlDbType.Int).Value = Task.GroupKey;
                zCommand.Parameters.Add("@GroupName", SqlDbType.NVarChar).Value = Task.GroupName;
                if (Task.ParentKey != "" && Task.ParentKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ParentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Task.ParentKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ParentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                if (Task.CustomerKey != "" && Task.CustomerKey.Length == 36)
                {
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Task.CustomerKey);
                }
                else
                {
                    zCommand.Parameters.Add("@CustomerKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = Task.CustomerID;
                zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = Task.CustomerName;
                zCommand.Parameters.Add("@CustomerPhone", SqlDbType.NVarChar).Value = Task.CustomerPhone;
                zCommand.Parameters.Add("@CustomerAddress", SqlDbType.NVarChar).Value = Task.CustomerAddress;
                if (Task.ContractKey != "" && Task.ContractKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Task.ContractKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ContractKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ContractName", SqlDbType.NVarChar).Value = Task.ContractName;
                if (Task.Reminder == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@Reminder", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@Reminder", SqlDbType.DateTime).Value = Task.Reminder;
                }

                if (Task.ApproveBy != "" && Task.ApproveBy.Length == 36)
                {
                    zCommand.Parameters.Add("@ApproveBy", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Task.ApproveBy);
                }
                else
                {
                    zCommand.Parameters.Add("@ApproveBy", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ApproveName", SqlDbType.NVarChar).Value = Task.ApproveName;
                if (Task.OwnerBy != "" && Task.OwnerBy.Length == 36)
                {
                    zCommand.Parameters.Add("@OwnerBy", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Task.OwnerBy);
                }
                else
                {
                    zCommand.Parameters.Add("@OwnerBy", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@OwnerName", SqlDbType.NVarChar).Value = Task.OwnerName;
                zCommand.Parameters.Add("@Style", SqlDbType.NChar).Value = Task.Style;
                zCommand.Parameters.Add("@Class", SqlDbType.NChar).Value = Task.Class;
                zCommand.Parameters.Add("@CodeLine", SqlDbType.NChar).Value = Task.CodeLine;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Task.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Task.RecordStatus;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Task.Publish;
                if (Task.PartnerNumber != "" && Task.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Task.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = Task.DepartmentKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = Task.BranchKey;
                zCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar).Value = Task.OrganizationID;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Task.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Task.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
UPDATE CRM_Task SET RecordStatus = 99 WHERE TaskKey = @TaskKey
UPDATE FNC_Receipt SET RecordStatus = 99 WHERE DocumentID = @TaskKey";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TaskKey", SqlDbType.NVarChar).Value = Task.TaskKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM CRM_Task WHERE TaskKey = @TaskKey";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TaskKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Task.TaskKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Process(string SQL)
        {
            string zResult = "";
            string zConnectionString = Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(SQL, zConnect);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        #endregion
    }
}
