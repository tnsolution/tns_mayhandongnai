﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Desktop
{
    public class Rpt_ItemSale
    {
        public string ItemKey { get; set; } = "";
        public string ItemName { get; set; } = "";
        public string ItemTime { get; set; } = "";
        public double ItemAmount { get; set; } = 0;
    }
}
